<?
ob_start("ob_gzhandler");
include("db.php");
$getSystem = mysqli_query($GLOBALS['db'],"SELECT * FROM system WHERE id=2");
$getSystemRow = mysqli_fetch_array($getSystem);
?>
<!DOCTYPE html>
<html lang="en">
<head>

<!-- /Yandex.Metrika counter -->
        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge" />
        <title>Contact US | <? echo $getSystemRow[title_eng]; ?></title>
        <meta content="Auto4Rental - Georgian one of the best car rental service!!! - <? echo $getSystemRow[keywords]; ?>" name="description" />
        <meta content="<? echo $getSystemRow[keywords]; ?>" name="keywords" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta property="og:title" content="Auto4Rental - car rental in tbilisi, rent a car from 19$ per day" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="http://rentcartbilisi.com" />
		<meta property="og:image" content="http://rentcartbilisi.com/black.png" />
        <meta content="telephone=no" name="format-detection" />
        <meta name="HandheldFriendly" content="true" />
		<link rel="stylesheet" href="http://rentcartbilisi.com/mega.css?v=453">
        <link rel="icon" type="image/x-icon" href="http://rentcartbilisi.com/favicon.ico" />
        <!--[if lt IE 9 ]>
<script src="http://rentcartbilisi.com/assets/js/separate-js/html5shiv-3.7.2.min.js" type="text/javascript"></script><meta content="no" http-equiv="imagetoolbar">
<![endif]-->
    </head>

    <body>
        <!-- Loader-->
        <div id="page-preloader"><span class="spinner border-t_second_b border-t_prim_a"></span>
        </div>
        <!-- Loader end-->
        <!-- ==========================-->
        <!-- MOBILE MENU-->
        <!-- ==========================-->
        <? include("assets/blocks/header_m.php"); ?>
        <div class="l-theme animated-css" data-header="sticky" data-header-top="200" data-canvas="container">
            
            <!-- ==========================-->
            <!-- SEARCH MODAL-->
            <!-- ==========================-->
            <div class="header-search open-search">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                            <div class="navbar-search">
                                <form class="search-global">
                                    <input class="search-global__input" type="text" placeholder="Type to search" autocomplete="off" name="s" value="" />
                                    <button class="search-global__btn"><i class="icon stroke icon-Search"></i>
                                    </button>
                                    <div class="search-global__note">Begin typing your search above and press return to search.</div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="search-close close" type="button"><i class="fa fa-times"></i>
                </button>
            </div>
            <? include("assets/blocks/header.php"); ?>
            <!-- end .header-->
					
                    
            <!-- end .b-title-page-->
            <div class="bg-grey">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="icon fa fa-home"></i></a>
                                </li>
                                
                                <li class="active">Contact</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end breadcrumb-->
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <main class="l-main-content" style="padding-top:120px;">
                            
                            <!-- end .post-->

                            
                            
                            <!-- end .section-comment-->
							
							
                            <section class="section-reply-form" id="section-reply-form">
                                <h2 class="ui-title-inner">Have a question? Ask</h2>
								<h4>CALL US: <? echo $getSystemRow[mobile]; ?></h4>
								<h4><i class="icon icon-xs fa fa-volume-control-phone"></i><a style="color:orange;" href="viber://chat?number=+995558515191"> Viber</a></h4>
								<h4><i class="icon icon-xs fa fa-whatsapp"></i><a style="color:orange;" href="whatsapp://send?number=+995558515191"> WhatsApp</a></h4>
								<h4><i class="icon icon-xs fa fa-facebook-official"></i><a style="color:orange;" target="_blank" href="https://www.facebook.com/Auto4rental"> Facebook</a></h4>
								<h4><i class="icon icon-xs fa fa-twitter-square"></i><a style="color:orange;" target="_blank" href="https://twitter.com/RentCarTbilisi"> Twitter</a></h4>
                                <form class="form-reply ui-form-2" action="#" method="post">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <div class="ui-label">Your name</div>
                                                <input id="name" class="form-control" type="text" required="required" />
                                            </div>
                                            <div class="form-group">
                                                <div class="ui-label">Email</div>
                                                <input id="email" class="form-control" type="email" required="required" />
                                            </div>
                                            <div class="form-group">
                                                <div class="ui-label">Mobile</div>
                                                <input id="mobile" class="form-control" type="tel" required="required" />
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <div class="ui-label">Your question</div>
                                                <textarea id="question" class="form-control" rows="9" required="required"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="text-right">
                                                <span id="resp"><button onclick='sss()' class="ui-form__btn btn btn-primary">send</button></span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </section>
                            <!-- end .section-reply-form-->
							
                        </main>
						
                        <!-- end .l-main-content-->
                    </div>
                    <div class="col-md-6" style="padding-top:140px;">
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11910.951651895293!2d44.749586!3d41.726174!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbb18c9d40b31a4e6!2sCar+Rental+Company!5e0!3m2!1sru!2sge!4v1529242193999" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
                </div>
            </div>
            <? include("assets/blocks/footer.php"); ?>
            <!-- .footer-->
        </div>
        <!-- end layout-theme-->

<script Language="JavaScript">
function sss()
{
document.getElementById("resp").innerHTML = '<img src="http://rentcartbilisi.com/lll2.gif" style="width:6%;">';
var name = document.getElementById("name").value;
var email = document.getElementById("email").value;
var mobile = document.getElementById("mobile").value;
var question = document.getElementById("question").value;
var xhr;
 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
}
var data = "name=" + name + "&email=" + email + "&mobile=" + mobile + "&question=" + question;
     xhr.open("POST", "http://rentcartbilisi.com/send.php", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
	 xhr.onreadystatechange = display_data;
	function display_data() {
	 if (xhr.readyState == 4) {
      if (xhr.status == 200) {
       //alert(xhr.responseText);	   
	  document.getElementById("resp").innerHTML = xhr.responseText;
      } else {
        alert('There was a problem with the request.');
      }
     }
	}
}
</script>
        <script src="../bro.js"></script>
    </body>



</html>