<?
session_start();

ob_start("ob_gzhandler");
include("db.php");

if(isset($_POST['date1']) and isset($_POST['date2']) and ($_POST['place'] !='none' or $_POST['drop'] != 'none'))
{
	$pick = $_POST['date1'];
	$end = $_POST['date2'];
	$_SESSION['pick'] = $pick;
	$_SESSION['end'] = $end;
	$_SESSION['place'] = $_POST[place];
	$_SESSION['drop'] = $_POST[drop];
	$getPlaceId = mysqli_query($GLOBALS['db'],"SELECT * FROM places WHERE name_eng='$_POST[place]' LIMIT 1");
	$getPlaceIdR = mysqli_fetch_array($getPlaceId);
	$_SESSION['place_id'] = $getPlaceIdR[id];
	$short_text = mb_substr($end,0,10,'UTF-8');
	$short_text2 = mb_substr($pick,0,10,'UTF-8');
	$pick = explode("/", $short_text2); //$pick[0] Day, $pick[1] Month, $pick[2] Year
	$end = explode("/", $short_text); //$end[0] Year, $end[1] Month, $end[2] Day
	$dayPick = $pick[2].'-'.$pick[1].'-'.$pick[0];
	$dayEnd = $end[2].'-'.$end[1].'-'.$end[0];
	$difference = intval(abs(strtotime($dayPick) - strtotime($dayEnd)));
	$promo = $_POST['promo'];
	$promoAct = 0;
	if(!empty($promo))
	{
		$promo = mysqli_real_escape_string($GLOBALS['db'],$promo);
		$data = array();
		
		$getPromo = mysqli_query($GLOBALS['db'],"SELECT * FROM promo WHERE promo='$promo' LIMIT 1");
		$promoIS = mysqli_num_rows($getPromo);
		
		if($promoIS >= 1)
		{
			$row = mysqli_fetch_array($getPromo);
			
			$promo_amount = $row['p'];
			$promoAct = 1;
			
			$_SESSION['promo'] = $promo;
			$_SESSION['promo_amount'] = $row['p'];
		}
		else
		{
			$promo_amount = 100;
		}
	}
	
	
	$DaysCount = $difference / (3600 * 24);
	
	
	$_SESSION['time1'] = $_POST['time1'];
	$_SESSION['time2'] = $_POST['time2'];
	
	$timePick = $_SESSION['pick'].' '.$_POST['time1'];
	$timePick = strtotime($timePick);
	
	$timeDrop = $_SESSION['end'].' '.$_POST['time2'];
	$timeDrop = strtotime($timeDrop);
	
	$diff = round(($timeDrop-$timePick)/3600);
	
	
	
	$_SESSION['diff'] = $diff;
	$_SESSION['days'] = $DaysCount;
	$getCars4Price = mysqli_query($GLOBALS['db'],"SELECT * FROM catalog");
	$row = mysqli_fetch_array($GLOBALS['db'],$getCars4Price);
	$prices = array();
	while($row = mysqli_fetch_array($getCars4Price))
	{
		if($_SESSION['place'] == 1)
		{
			$getPickupPrice = $row[pick_tbs];
		}
		if($_SESSION['place'] == 2)
		{
			$getPickupPrice = $row[pick_tbs_air];
		}
		if($_SESSION['place'] == 3)
		{
			$getPickupPrice = $row[pick_btm];
		}
		if($_SESSION['place'] == 4)
		{
			$getPickupPrice = $row[pick_btm_air];
		}
		if($_SESSION['place'] == 5)
		{
			$getPickupPrice = $row[pick_kut];
		}
		if($_SESSION['place'] == 6)
		{
			$getPickupPrice = $row[pick_kut_air];
		}
		if($_SESSION['place'] == 7)
		{
			$getPickupPrice = $row[pick_step];
		}



		if($_SESSION['drop'] == 1)
		{
			$getDropoffPrice = $row[drop_tbs];
		}
		if($_SESSION['drop'] == 2)
		{
			$getDropoffPrice = $row[drop_tbs_air];
		}
		if($_SESSION['drop'] == 3)
		{
			$getDropoffPrice = $row[drop_btm];
		}
		if($_SESSION['drop'] == 4)
		{
			$getDropoffPrice = $row[drop_btm_air];
		}
		if($_SESSION['drop'] == 5)
		{
			$getDropoffPrice = $row[drop_kut];
		}
		if($_SESSION['drop'] == 6)
		{
			$getDropoffPrice = $row[drop_kut_air];
		}
		if($_SESSION['drop'] == 7)
		{
			$getDropoffPrice = $row[drop_step];
		}


		$pickoff = $getDropoffPrice + $getPickupPrice;
		if(!isset($_SESSION[pick]))
		{
			$price = $row['30-31'];
		}
		if(isset($_SESSION[pick]))
		{
			if($diff <= 0)
			{
				$price = $row['30-31'] + $pickoff;
			}
			if($diff <= 24 and $diff > 0 and $DaysCount != 1)
			{
				$price = $row['price'] + $pickoff;
			}
			if($diff <= 24 and $diff > 0 and $DaysCount == 1)
			{
				$price = $row['price'] + $pickoff;
			}
			if(($DaysCount >= 1 and $DaysCount <=2) and ($diff > 24 and $diff <= 48))
			{
				$pricePerHour = ($row['price']*2);
				$price = $pricePerHour+$pickoff;
			}
			if($DaysCount >= 1 and $DaysCount <=2 and $diff > 48)
			{
				$pricePerHour = $row['price']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
			}
			if($DaysCount >= 3 and $DaysCount <= 4)
			{
				$pricePerHour = $row['3-4']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			if($DaysCount >= 5 and $DaysCount <= 7)
			{
				$pricePerHour = $row['5-7']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			if($DaysCount >= 8 and $DaysCount <= 10)
			{
				$pricePerHour = $row['8-10']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			if($DaysCount >= 11 and $DaysCount <= 14)
			{
				$pricePerHour = $row['11-15']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			if($DaysCount >= 15 and $DaysCount <= 17)
			{
				$pricePerHour = $row['16-21']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			if($DaysCount >= 18 and $DaysCount <= 20)
			{
				$pricePerHour = $row['22-25']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			if($DaysCount >= 21 and $DaysCount <= 23)
			{
				$pricePerHour = $row['26-27']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			if($DaysCount >= 24 and $DaysCount <= 26)
			{
				$pricePerHour = $row['28-29']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			if($DaysCount >= 27 and $DaysCount <= 365)
			{
				$pricePerHour = $row['30-31']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			
		}
		$prices[] = $price;
	}
	
	$maxp = max($prices)+1;
	$minp = min($prices)-1;
	//echo $pickoff;
	//echo $timePick .' and '.$timeDrop;
}

?>
<!DOCTYPE html>
<html lang="en">

    

<head>
    <title>Cheap Car rental Tbilisi airport Georgia, best car rental in Tbilisi Kutaisi Batumi. Car booking in Tbilisi, Car Hire in Tbilisi</title>
<meta name="description" content="Cheap Car rental in Tbilisi airport Georgia, car rental Tbilisi, Best car booking in Tbilisi kutaisi batumi, economy cars (4x4,jeep,sedan,suv) rentals, Cheap car hire in Tbilisi">
<meta name="keywords" content="best,cheap,economy,car,cars,jeep,suv,sedan,4x4,rent,rental,book,booking,tbilisi,batumi,kutaisi,georgia,airport,hire,car hire,Car rental Tbilisi,Car rental Georgia,Rent car Tbilisi,4x4 booking in georgia,4x4 booking in tbilisi,4x4 booking in tbilisi airport,4x4 hire in georgia,4x4 hire in tbilisi,4x4 hire in tbilisi airport,4x4 rental in georgia,4x4 rental in tbilisi,4x4 rental in tbilisi airport,best car booking in georgia,best car booking in tbilisi,best car booking in tbilisi airport,best car hire in georgia,best car hire in tbilisi,best car hire in tbilisi airport,best car rental in georgia,best car rental in tbilisi,best car rental in tbilisi airport,car booking in georgia,car booking in tbilisi,car booking in tbilisi airport,car hire in georgia,car hire in tbilisi,car hire in tbilisi airport,car rental in georgia,car rental in tbilisi,car rental in tbilisi airport,cheap car booking in georgia,cheap car booking in tbilisi,cheap car booking in tbilisi airport,cheap car hire in georgia,cheap car hire in tbilisi,cheap car hire in tbilisi airport,cheap car rental in georgia,cheap car rental in tbilisi,cheap car rental in tbilisi airport,cheap car rentals in georgia,cheap car rentals in tbilisi,cheap car rentals in tbilisi airport,economy car booking in georgia,economy car booking in tbilisi,economy car booking in tbilisi airport,economy car hire in georgia,economy car hire in tbilisi,economy car hire in tbilisi airport,economy car rental in georgia,economy car rental in tbilisi,economy car rental in tbilisi airport,jeep booking in georgia,jeep booking in tbilisi,jeep booking in tbilisi airport,jeep hire in georgia,jeep hire in tbilisi,jeep hire in tbilisi airport,jeep rental in georgia,jeep rental in tbilisi,jeep rental in tbilisi airport,sedan booking in georgia,sedan booking in tbilisi,sedan booking in tbilisi airport,sedan hire in georgia,sedan hire in tbilisi,sedan hire in tbilisi airport,sedan rental in georgia,sedan rental in tbilisi,sedan rental in tbilisi airport,suv booking in georgia,suv booking in tbilisi,suv booking in tbilisi airport,suv hire in georgia,suv hire in tbilisi,suv hire in tbilisi airport,suv rental in georgia,suv rental in tbilisi,suv rental in tbilisi airport,rent a 4x4 in georgia,rent a 4x4 in tbilisi,rent a 4x4 in tbilisi airport,rent a best car in georgia,rent a best car in tbilisi,rent a best car in tbilisi airport,rent a car in georgia,rent a car in tbilisi,rent a car in tbilisi airport,rent a cheap car in georgia,rent a cheap car in tbilisi,rent a cheap car in tbilisi airport,rent a economy car in georgia,rent a economy car in tbilisi,rent a economy car in tbilisi airport,rent a jeep in georgia,rent a jeep in tbilisi,rent a jeep in tbilisi airport,rent a sedan in georgia,rent a sedan in tbilisi,rent a sedan in tbilisi airport,rent a suv in georgia,rent a suv in tbilisi,rent a suv in tbilisi airport,car booking airport batumi,car booking airport kutaisi,car booking airport tbilisi,car booking batumi,car booking kutaisi,car hire airport batumi,car hire airport kutaisi,car hire airport tbilisi,car hire batumi,car hire kutaisi,car rental airport batumi,car rental airport kutaisi,car rental airport tbilisi,car rental batumi,car rental kutaisi,cheap car booking airport batumi,cheap car booking airport kutaisi,cheap car booking airport tbilisi,cheap car booking batumi,cheap car booking kutaisi,cheap car hire airport batumi,cheap car hire airport kutaisi,cheap car hire airport tbilisi,cheap car hire batumi,cheap car hire kutaisi,cheap car rental airport batumi,cheap car rental airport kutaisi,cheap car rental airport tbilisi,cheap car rental batumi,cheap car rental kutaisi,car booking in airport batumi,car booking in airport kutaisi,car booking in airport tbilisi,car booking in batumi,car booking in kutaisi,car hire in airport batumi,car hire in airport kutaisi,car hire in airport tbilisi,car hire in batumi,car hire in kutaisi,car rental in airport batumi,car rental in airport kutaisi,car rental in airport tbilisi,car rental in batumi,car rental in kutaisi,cheap car booking in airport batumi,cheap car booking in airport kutaisi,cheap car booking in airport tbilisi,cheap car booking in batumi,cheap car booking in kutaisi,cheap car hire in airport batumi,cheap car hire in airport kutaisi,cheap car hire in airport tbilisi,cheap car hire in batumi,cheap car hire in kutaisi,cheap car rental in airport batumi,cheap car rental in airport kutaisi,cheap car rental in airport tbilisi,cheap car rental in batumi,cheap car rental in kutaisi,rent a car airport batumi,rent a car airport kutaisi,rent a car airport tbilisi,rent a car batumi,rent a car kutaisi,rent a cheap car airport batumi,rent a cheap car airport kutaisi,rent a cheap car airport tbilisi,rent a cheap car batumi,rent a cheap car kutaisi,rent a car in airport batumi,rent a car in airport kutaisi,rent a car in airport tbilisi,rent a car in batumi,rent a car in kutaisi,rent a cheap car in airport batumi,rent a cheap car in airport kutaisi,rent a cheap car in airport tbilisi,rent a cheap car in batumi,rent a cheap car in kutaisi">
    
<meta name="google-site-verification" content="yZ3si8gNk0r_qWQGY9ahsm8krBI3CuxNIhdKUIViA5Q" />
    
<meta name="yandex-verification" content="44786e581c8d566f" />
<!-- Global site tag (gtag.js) - Google Analytics -->

        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge" />
        <title>
		<?
			if(isset($_GET[id]) and !empty($_GET[id]))
			{
				$id = mysqli_real_escape_string(trim($_GET[id]));
				$url = explode('-',$id);
				$id_counter = count($url);
				$car_id = $url[$id_counter-1];
				for($i=0;$i<$id_counter-1;$i++)
				{
					$car_name .= $url[$i].' ';
				}
				$car_name = trim($car_name);
				$getTitle = mysqli_query($GLOBALS['db'],"SELECT * FROM catalog WHERE id='$car_id' and name='$car_name'") or die(mysqli_error());
				$getTitleR = mysqli_fetch_array($getTitle);
				//$title = $getTitleR[name].' From '.$getTitleR[price].' GEL | '.$getSystemRow[title_eng];
				//echo $title;
				echo $getTitleR[title_eng];
			}
			else
			{
				echo 'Cheap price Suv and Sedan  cars rental in Georgia | Batumi | Kutaisi';
			}
		?></title>
        <meta content="<? 
		if(isset($_GET[id]) and !empty($_GET[id]))
		{
			echo strip_tags($getTitleR[desc_eng]).' | Auto4Rental'; 
		}
		else
		{
			echo 'Get best cheap car rental Tbilisi service from company Auto4rental. Rent a car in Batumi airport and Kutaisi airport with cheap price deals';
		}
		?>" name="description" />
        <meta content="<? echo $getSystemRow[keywords]; ?>" name="keywords" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta content="telephone=no" name="format-detection" />
        <meta name="HandheldFriendly" content="true" />
       
		<meta property="og:title" content="<?
		if(isset($_GET[id]) and !empty($_GET[id]))
			{
				echo $getTitleR[title_eng];
			}
			else
			{
				echo 'Cars rental Georgia | Rent a cars Georgia | Cheap cars rentals Georgia';
			}
		
		?>" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="http://rentcartbilisi.com" />
		<meta property="og:image" content="http://rentcartbilisi.com/black.png" />
        <link rel="icon" type="image/x-icon" href="http://rentcartbilisi.com/favicon.ico" />
		<link rel="stylesheet" type="text/css" href="http://rentcartbilisi.com/assets/fonts/flaticon/font/flaticon.css">
		
		<link rel="stylesheet" href="http://rentcartbilisi.com/assets/plugins/slider-pro/slider-pro.css">
		<link rel="stylesheet" href="http://rentcartbilisi.com/mega2.css?v=32">
		
        <!--[if lt IE 9 ]>
<script src="http://rentcartbilisi.com/assets/js/separate-js/html5shiv-3.7.2.min.js" type="text/javascript"></script><meta content="no" http-equiv="imagetoolbar">
<![endif]-->

    </head>

    <body onload='GetCars();'>
        <!-- Loader-->
        <div id="page-preloader"><span class="spinner border-t_second_b border-t_prim_a"></span>
        </div>
        <!-- Loader end-->
        <!-- ==========================-->
        <!-- MOBILE MENU-->
        <!-- ==========================-->
        <? include("assets/blocks/header_m.php"); ?>
        <div class="l-theme animated-css" data-header="sticky" data-header-top="200" data-canvas="container">
            
            <!-- ==========================-->
            <!-- SEARCH MODAL-->
            <!-- ==========================-->
            <div class="header-search open-search">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                            <div class="navbar-search">
                                <form class="search-global">
                                    <input class="search-global__input" type="text" placeholder="Type to search" autocomplete="off" name="s" value="" />
                                    <button class="search-global__btn"><i class="icon stroke icon-Search"></i>
                                    </button>
                                    <div class="search-global__note">Begin typing your search above and press return to search.</div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="search-close close" type="button"><i class="fa fa-times"></i>
                </button>
            </div>
            
            <? include("assets/blocks/header.php"); ?>
            <!-- end .header-->
			
			<?
				function isMobile() { 
				return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
				}
				if(isset($_GET['id']) and !empty($_GET['id']))
				{
					include("assets/blocks/car_d.php");
				}
				else
				{	
					if(isMobile()){
						include("assets/blocks/cars_m.php");
					}
					else
					{
						include("assets/blocks/cars.php");
					}
					
				}
			
			?>
            <!-- end .l-main-content-->
            <? include("assets/blocks/footer.php"); ?>
            <!-- .footer-->
        </div>
        <!-- end layout-theme-->


        <!-- ++++++++++++-->
        <!-- MAIN SCRIPTS-->
        <!-- ++++++++++++-->
        <script src="http://rentcartbilisi.com/bro2.js?v=12324"></script>
		
<script>
if ($('#slider-price').length > 0) {
    var slider1 = document.getElementById('slider-price');
	
    noUiSlider.create(slider1, {
		<?
		if(isset($_POST['date1']) and isset($_POST['date2']) and ($_POST[place] !='none' or $_POST[drop] != 'none'))
		{
			echo '
			start: [ '.$minp.', '.$maxp.' ],
			range: {
			\'min\': ['.$minp.'],
			\'max\': ['.$maxp.']
			
			';
		}
		else
		{
			$getPmin = mysqli_query($GLOBALS['db'],"SELECT min(`30-31`) as m,max(`30-31`) as p FROM catalog") or die(mysqli_error());
			$getPminR = mysqli_fetch_array($getPmin);
			echo '
			
			
			start: [ '.$getPminR[m].', 50000 ],
      range: {
        \'min\': ['.$getPminR[m].'],
        \'max\': [50000]
			';
		}
		?>
      
      },
      step: 1,
      connect: true,
      format: wNumb({
        decimals: 0
      }),
    });
    var snapValues = [
      document.getElementById('slider-snap-value-lower'),
      document.getElementById('slider-snap-value-upper')
    ];

    slider1.noUiSlider.on('update', function( values, handle ) {
      snapValues[handle].innerHTML = values[handle];
    });
  }
  
  if ($('#slider-price2').length > 0) {
    var slider2 = document.getElementById('slider-price2');

    noUiSlider.create(slider2, {
      start: [ 2004, 2018 ],
      range: {
        'min': [2004],
        'max': [2018]
      },
      step: 1,
      connect: true,
      format: wNumb({
        decimals: 0
      }),
    });
    var snapValues2 = [
      document.getElementById('slider-snap-value-lower2'),
      document.getElementById('slider-snap-value-upper2')
    ];

    slider2.noUiSlider.on('update', function( values, handle ) {
      snapValues2[handle].innerHTML = values[handle];
    });
  }
</script>
<div id="add">
		</div>
		<script Language="JavaScript">



function calculate()
{
var pick = document.getElementById("datetimepicker3").value;
var end = document.getElementById("datetimepicker2").value;
var pickup = document.getElementById("pickup").value;
var dropoff = document.getElementById("dropoff").value;
var id = document.getElementById("carID").value;
var time1 = document.getElementById("time1").value;
var time2 = document.getElementById("time2").value;
<?
$getServices = mysqli_query($GLOBALS['db'],"SELECT * FROM services");
$getServicesR = mysqli_fetch_array($getServices);
do
{
	echo 'var ser'.$getServicesR[id].' = document.getElementById("chec'.$getServicesR[id].'").checked;';
}
while($getServicesR = mysqli_fetch_array($getServices));
?>
var xhr;
 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
}
var data = "pick=" + pick + "&end=" + end + "&id=" + id + "&pickup=" + pickup + "&dropoff=" + dropoff + "&time1=" + time1 + "&time2=" + time2

<?
$getServices2 = mysqli_query($GLOBALS['db'],"SELECT * FROM services");
$getServicesR2 = mysqli_fetch_array($getServices2);
do
{
	echo ' + "&ser'.$getServicesR2[id].'=" + ser'.$getServicesR2[id].'';
}
while($getServicesR2 = mysqli_fetch_array($getServices2));
?>
;
     xhr.open("POST", "http://rentcartbilisi.com/booking.php", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
	 xhr.onreadystatechange = display_data;
	function display_data() {
	 if (xhr.readyState == 4) {
      if (xhr.status == 200) {
       //alert(xhr.responseText);	   
	  document.getElementById("amount").innerHTML = xhr.responseText;
      } else {
        alert('There was a problem with the request.');
      }
     }
	}
}
try {
// код, вызывающий ошибку

function zak()
{
document.getElementById("answer").innerHTML = '<img src="http://rentcartbilisi.com/lll.gif" style="width:36%;">';
var pick = document.getElementById("datetimepicker3").value;
var end = document.getElementById("datetimepicker2").value;
var id = document.getElementById("carID").value;
var b = document.getElementById("b").value;
var first = document.getElementById("firstname").value;
var last = document.getElementById("lastname").value;
var mob = document.getElementById("mob").value;
var email = document.getElementById("Email").value;

var time1 = document.getElementById("time1").value;
var time2 = document.getElementById("time2").value;
<?
$getServices = mysqli_query($GLOBALS['db'],"SELECT * FROM services");
$getServicesR = mysqli_fetch_array($getServices);
do
{
	echo 'var ser'.$getServicesR[id].' = document.getElementById("chec'.$getServicesR[id].'").checked;';
}
while($getServicesR = mysqli_fetch_array($getServices));
?>
var pickup = document.getElementById("pickup").value;
var dropoff = document.getElementById("dropoff").value;

var xhr;
 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
}
var data = "pick=" + pick + "&end=" + end + "&id=" + id + "&book=" + b + "&first=" + first + "&last=" + last + "&mob=" + mob + "&email=" + email + "&pickup=" + pickup + "&dropoff=" + dropoff + "&time1=" + time1 + "&time2=" + time2
<?
$getServices2 = mysqli_query($GLOBALS['db'],"SELECT * FROM services");
$getServicesR2 = mysqli_fetch_array($getServices2);
do
{
	echo ' + "&ser'.$getServicesR2[id].'=" + ser'.$getServicesR2[id].'';
}
while($getServicesR2 = mysqli_fetch_array($getServices2));
?>
;
     xhr.open("POST", "http://rentcartbilisi.com/booking.php", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
	 xhr.onreadystatechange = display_data;
	function display_data() {
	 if (xhr.readyState == 4) {
      if (xhr.status == 200) {
       //alert(xhr.responseText);	   
	  document.getElementById("answer").innerHTML = xhr.responseText;
      } else {
        alert('There was a problem with the request.');
      }
     }
	}
}
}
catch(e) {
    console.log(e)
}
function sss()
{
document.getElementById("resp").innerHTML = '<img src="http://rentcartbilisi.com/lll2.gif" style="width:6%;">';
var name = document.getElementById("name").value;
		var email = document.getElementById("email").value;
		var mobile = document.getElementById("mobile").value;
		var question = document.getElementById("question").value;
var xhr;
 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
}
var data = "name=" + name + "&email=" + email + "&mobile=" + mobile + "&question=" + question;
     xhr.open("POST", "http://rentcartbilisi.com/send.php", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
	 xhr.onreadystatechange = display_data;
	function display_data() {
	 if (xhr.readyState == 4) {
      if (xhr.status == 200) {
       //alert(xhr.responseText);	   
	  document.getElementById("resp").innerHTML = xhr.responseText;
      } else {
        alert('There was a problem with the request.');
      }
     }
	}
}
function filt()
{
var mark = document.getElementById("mark").value;
var model = document.getElementById("model").value;
var priceN = document.getElementById("slider-snap-value-lower").innerHTML;
var priceM = document.getElementById("slider-snap-value-upper").innerHTML;

var minYear = document.getElementById("slider-snap-value-lower2").innerHTML;
var maxYear = document.getElementById("slider-snap-value-upper2").innerHTML;
var fuel = document.getElementById("fuel").value;
var transm = document.getElementById("transm").value;

var suv = document.getElementById("typeSuv").checked;

var sedan = document.getElementById("typeSedan").checked;


var order = document.getElementById("orders").value;
var xhr;
 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
}
var data = "mark=" + mark + "&model=" + model + "&priceN=" + priceN + "&priceM=" + priceM + "&suv=" + suv + "&sedan=" + sedan + "&fuel=" + fuel + "&transm=" + transm + "&minyear=" + minYear + "&maxyear=" + maxYear + "&order=" + order;
     xhr.open("POST", "http://rentcartbilisi.com/searchV2.php", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
	 xhr.onreadystatechange = display_data;
	function display_data() {
	 if (xhr.readyState == 4) {
      if (xhr.status == 200) {
       //alert(xhr.responseText);	   
	  document.getElementById("result").innerHTML = xhr.responseText;
      } else {
        alert('There was a problem with the request.');
      }
     }
	}
}
$("#promo").on('keyup', function(){
	var promo = $("#promo").val();
	
	
		param 				 = new Object();
		param.promo			 = promo;
		$.ajax({
			url: 'http://rentcartbilisi.com/promo.php',
			data: param,
			success: function(datas) {       
				var d = JSON.parse(datas);
				
				if(d.status == 'ok'){

					$("#promo_answer").html('<p style="text-align:center; color:#51fd51; font-size:17px; font-weight:900; display:block" >Correct promotional code: '+d.promo+'  <b>-'+d.p+'% discount</b></p>');
				}
				else{
					$("#promo_answer").html('<p style="text-align:center; color:red; font-size:17px; font-weight:900; display:block" >Promo code not real!!!</p>');
				}
			}
		});
		
		if(promo == ''){
			$("#promo_answer").empty();
			calculate();
		}
	
});
function age()
{
var birth = document.getElementById("datetimepicker").value;
var xhr;
 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
}
var data = "age=" + birth;
     xhr.open("POST", "http://rentcartbilisi.com/booking.php", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
	 xhr.onreadystatechange = display_data;
	function display_data() {
	 if (xhr.readyState == 4) {
      if (xhr.status == 200) {
       //alert(xhr.responseText);	   
	  document.getElementById("age").innerHTML = xhr.responseText;
      } else {
        alert('There was a problem with the request.');
      }
     }
	}
}

function Getadd() {
    document.getElementById("result").innerHTML = '<center><img src="http://rentcartbilisi.com/loading.gif"></center>';
    var mark = document.getElementById("mark").value,
        model = document.getElementById("model").value,
        fuel = document.getElementById("fuel").value,
        transm = document.getElementById("transm").value,
        suv = document.getElementById("typeSuv").checked,
        sedan = document.getElementById("typeSedan").checked,
        order = document.getElementById("orders").value,
        xhr;
	<?
		$getServices = mysqli_query($GLOBALS['db'],"SELECT * FROM services");
		$getServicesR = mysqli_fetch_array($getServices);
		do
		{
			echo 'var ser'.$getServicesR[id].' = document.getElementById("chec'.$getServicesR[id].'").checked;';
		}
		while($getServicesR = mysqli_fetch_array($getServices));
	?>
    window.XMLHttpRequest ? xhr = new XMLHttpRequest : window.ActiveXObject && (xhr = new ActiveXObject("Microsoft.XMLHTTP"));
    var data = "mark=" + mark + "&model=" + model + "&suv=" + suv + "&sedan=" + sedan + "&fuel=" + fuel + "&transm=" + transm + "&order=" + order
	<?
		$getServices2 = mysqli_query($GLOBALS['db'],"SELECT * FROM services");
		$getServicesR2 = mysqli_fetch_array($getServices2);
		do
		{
			echo ' + "&ser'.$getServicesR2[id].'=" + ser'.$getServicesR2[id].'';
		}
		while($getServicesR2 = mysqli_fetch_array($getServices2));
	?>
	
	;

    function display_data() {
        if (4 == xhr.readyState)
            if (200 == xhr.status) {
                document.getElementById("result").innerHTML = xhr.responseText;
                try {
                    eval(document.getElementById("run").innerHTML)
                } catch (e) {}
            } else alert("There was a problem with the request.")
    }
    xhr.open("POST", "http://rentcartbilisi.com/searchV2.php", !0), xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"), xhr.send(data), xhr.onreadystatechange = display_data, setTimeout(function() {
        
    }, 1e3)
}

<?
if(isset($_GET[id]))
{
	echo '
	window.onload = function() {
  calculate();
	}
	';
}
?>


</script>
<script src="http://rentcartbilisi.com/assets/plugins/slider-pro/jquery.sliderPro.min.js"></script>
    </body>
        
		


</html>