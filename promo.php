<?
session_start();
include("db.php");
$promo = $_REQUEST['promo'];

if(!empty($promo))
{
	$promo = mysqli_real_escape_string($GLOBALS['db'],$promo);
	$data = array();
	
	$getPromo = mysqli_query($GLOBALS['db'],"SELECT * FROM promo WHERE promo='$promo' LIMIT 1");
	$promoIS = mysqli_num_rows($getPromo);
	
	if($promoIS >= 1)
	{
		$row = mysqli_fetch_array($getPromo);
		$data['status'] = 'ok';
		$data['promo'] = $row['promo'];
		$data['p'] = $row['p'];
		
		$_SESSION['promo'] = $row['promo'];
		$_SESSION['promo_amount'] = $row['p'];
		$data['erty'] = $_SESSION['promo_amount'];
	}
	else
	{
		$data['status'] = 'off';
		unset($_SESSION['promo']);
		unset($_SESSION['promo_amount']);
	}	
}
else
{
	unset($_SESSION['promo']);
	unset($_SESSION['promo_amount']);
}
echo json_encode($data);
?>