<?
include("../db.php");

class sitemap
{
	private $CarsEng = [];
	private $CarsGeo = [];
	private $CarsRus = [];
	
	private $NewsEng = [];
	private $NewsGeo = [];
	private $NewsRus = [];
	
	public function createSitemap()
	{
		$this->fetchCars();
		$this->fetchNews();

		$datetime = date('Y-m-d');
		$dom = new domDocument("1.0", "utf-8"); // Создаём XML-документ версии 1.0 с кодировкой utf-8
		$urlset = $dom->createElement("urlset");
		$urlset->setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
		$dom->appendChild($urlset);
		
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/ru/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
		
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/pricing/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
		
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/ru/pricing/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
		
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/ru/terms");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
		
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/terms/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
		
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/ru/car/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
			
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/ru/news/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
		
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/ru/about-us/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
		
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/ru/contact/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
		
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/ge/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
		
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/ge/car/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
			
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/ge/news/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
		
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/ge/about-us/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
		
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/ge/contact/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
		
		
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
		
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/car/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
			
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/news/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
		
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/about-us/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
		
		$url = $dom->createElement("url");
		$dom->appendChild($url);
		$loc = $dom->createElement("loc", "http://rentcartbilisi.com/contact/");
		$lastmod = $dom->createElement("lastmod", $datetime);
		$urlset->appendChild($url);
		$url->appendChild($loc);
		$url->appendChild($lastmod);
		for($i=0;$i<count($this->CarsEng);$i++)
		{
			$url = $dom->createElement("url");
			$dom->appendChild($url);
			$loc = $dom->createElement("loc", $this->CarsEng[$i]);
			$lastmod = $dom->createElement("lastmod", $datetime);
			$urlset->appendChild($url);
			$url->appendChild($loc);
			$url->appendChild($lastmod);
			
		}
		for($i=0;$i<count($this->CarsRus);$i++)
		{
			$url = $dom->createElement("url");
			$dom->appendChild($url);
			$loc = $dom->createElement("loc", $this->CarsRus[$i]);
			$lastmod = $dom->createElement("lastmod", $datetime);
			$urlset->appendChild($url);
			$url->appendChild($loc);
			$url->appendChild($lastmod);
			
		}
		for($i=0;$i<count($this->CarsGeo);$i++)
		{
			$url = $dom->createElement("url");
			$dom->appendChild($url);
			$loc = $dom->createElement("loc", $this->CarsGeo[$i]);
			$lastmod = $dom->createElement("lastmod", $datetime);
			$urlset->appendChild($url);
			$url->appendChild($loc);
			$url->appendChild($lastmod);
			
		}
		for($i=0;$i<count($this->NewsGeo);$i++)
		{
			$url = $dom->createElement("url");
			$dom->appendChild($url);
			$loc = $dom->createElement("loc", $this->NewsGeo[$i]);
			$lastmod = $dom->createElement("lastmod", $datetime);
			$urlset->appendChild($url);
			$url->appendChild($loc);
			$url->appendChild($lastmod);
			
		}
		for($i=0;$i<count($this->NewsEng);$i++)
		{
			$url = $dom->createElement("url");
			$dom->appendChild($url);
			$loc = $dom->createElement("loc", $this->NewsEng[$i]);
			$lastmod = $dom->createElement("lastmod", $datetime);
			$urlset->appendChild($url);
			$url->appendChild($loc);
			$url->appendChild($lastmod);
			
		}
		for($i=0;$i<count($this->NewsRus);$i++)
		{
			$url = $dom->createElement("url");
			$dom->appendChild($url);
			$loc = $dom->createElement("loc", $this->NewsRus[$i]);
			$lastmod = $dom->createElement("lastmod", $datetime);
			$urlset->appendChild($url);
			$url->appendChild($loc);
			$url->appendChild($lastmod);
			
		}
		$dom->save("../sitemap.xml"); // Сохраняем полученный XML-документ в файл
	}
	
	private function fetchCars()
	{
		$getCars = mysql_query("SELECT * FROM catalog");
		$getCarsR = mysql_fetch_array($getCars);
		
		do
		{
			$this->CarsEng[] = "http://rentcartbilisi.com/car/".$getCarsR[id]."/";
			$this->CarsRus[] = "http://rentcartbilisi.com/ru/car/".$getCarsR[id]."/";
			$this->CarsGeo[] = "http://rentcartbilisi.com/ge/car/".$getCarsR[id]."/";
		}
		while($getCarsR = mysql_fetch_array($getCars));
	}
	
	private function fetchNews()
	{
		$getNews = mysql_query("SELECT * FROM news");
		$getNewsR = mysql_fetch_array($getNews);
		
		do
		{
			$this->NewsEng[] = "http://rentcartbilisi.com/news/".$getNewsR[id]."/";
			$this->NewsRus[] = "http://rentcartbilisi.com/ru/news/".$getNewsR[id]."/";
			$this->NewsGeo[] = "http://rentcartbilisi.com/ge/news/".$getNewsR[id]."/";
		}
		while($getNewsR = mysql_fetch_array($getNews));
	}
}
?>