<?
$getSystem = mysqli_query($GLOBALS['db'],"SELECT * FROM system WHERE id=2");
$getSystemRow = mysqli_fetch_array($getSystem);
?>
<header class="header header-boxed-width navbar-fixed-top header-background-white header-color-black header-topbar-dark header-logo-black header-topbarbox-1-left header-topbarbox-2-right header-navibox-1-left header-navibox-2-right header-navibox-3-right header-navibox-4-right">
                <div class="container container-boxed-width">
                    <div class="top-bar">
                        <div class="container" style="height:50px;">
                            <div class="header-topbarbox-1">
                                <ul>
                                    <li><i class="icon fa fa-clock-o"></i>24/7</li>
                                    <li><i class="icon fa fa-phone"></i><a href="#"><? echo $getSystemRow[mobile];?></a>
                                    </li>
                                    <li><i class="icon fa fa-envelope-o"></i><a href="mailto:<? echo $getSystemRow[email];?>"><? echo $getSystemRow[email];?></a>
                                    </li>
									
                                </ul>
                            </div>
                            <div class="header-topbarbox-2">
                                <ul class="social-links">
									<li style="width:100px;"><a href="#"><img src="http://rentcartbilisi.com/assets/img/uk.png" alt="car rental tbilisi, car rental georgia, rent car tbilisi, rent car in tbilisi" style="width:25%; height:25%;"></a>
                                    </li>
                                    <li style="width:100px;"><a href="/ru"><img src="http://rentcartbilisi.com/assets/img/ru.png" alt="rent car tbilisi, car rental tbilisi, car rental georgia, rent car in tbilisi" style="width:25%; height:25%;"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <nav class="navbar" id="nav">
                        <div class="container">
                            <div class="header-navibox-1">
                                <!-- Mobile Trigger Start-->
                                <button class="menu-mobile-button visible-xs-block js-toggle-mobile-slidebar toggle-menu-button"><i class="toggle-menu-button-icon"><span></span><span></span><span></span><span></span><span></span><span></span></i>
                                </button>
                                <!-- Mobile Trigger End-->
                                <a class="navbar-brand scroll" style="width: 100%;" href="/">
                                    <img class="normal-logo img-responsive" style="width: 63%; height: 50%;" src="<? echo $getSystemRow[logo]; ?>" alt="logo" />
                                    <img class="scroll-logo hidden-xs img-responsive" style="width: 200px;" src="<? echo $getSystemRow[logo]; ?>" alt="logo" />
                                </a>
                            </div>
                            <div class="header-navibox-3" style="padding-top:15px;">
                                <a class="btn btn-primary" href="/contact/">Contact</a>
                            </div>
                            <div class="header-navibox-2" style="padding-top:15px;">
                                <ul class="yamm main-menu nav navbar-nav">
                                    <li><a href="/">Home</a>
                                    </li>
									
                                    <li><a href="/car/">Car Fleet</a>
                                    </li>
                                    <li><a href="/pricing/">Pricing</a>
									</li>
                                    <li><a href="/news/">News</a>
                </li>
				<li><a href="/terms/">Terms Of Rental</a>
                </li>
                                     <li><a href="/about-us/">About US</a>
                                    </li>
									<li>|
                                    </li>
									<li><a><? echo '1 USD = '.$getSystemRow[dollar].' GEL'; ?></a>
                                    </li>
									
									
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>