<?
session_start();
if(!isset($_SESSION[a_id]))
{
	die("Only authorized users can enter here");
}
//////////
include("../db.php");
//////////
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
 <script src="ckeditor/ckeditor.js"></script>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<script type="text/javascript" src="../js/jquery.meerkat.1.0.js"></script>
<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){	
	meerkat({
		close: '.close',
		dontShow: '.dont-show',
		animation: 'slide',
		animationSpeed: 500,
		dontShowExpire: 0,
		removeCookie: '.remove-cookie',
		meerkatPosition: 'bottom',
		background: '#2e2a22 url(../../img/meerkat-bg.png) repeat-x 0 0',
		height: '110px'
	});
	
});
</script>
	<meta charset="utf-8" />
    <title>სასტუმროები</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="Admin/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="Admin/assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="Admin/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    
	<link rel="stylesheet" type="text/css" href="Admin/css/style.css" />
	<link rel="stylesheet" type="text/css" href="Admin/css/style_responsive.css" />
	<link rel="stylesheet" type="text/css" href="Admin/css/style_gray.css" />

	<link href="Admin/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link href="Admin/assets/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
	<link href="Admin/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="Admin/assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="Admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
    <link href="Admin/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" rel="stylesheet" type="text/css" />

    
	<script type="text/javascript" src="Admin/js/jquery-1.8.3.min.js"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="administration.php">
                    <img src="http://rentcartbilisi.com/logo.png" width="100px" height="100px" alt="">
                    
                </a>
				<!-- END LOGO -->

				<div id="top_menu" class="nav notify-row">
                    <!-- BEGIN NOTIFICATION -->
                                    </div>
                <!-- END  NOTIFICATION -->

                <div class="top-nav ">
                    <ul class="nav pull-right top-menu">
                        						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="https://demo.betscheme.com/theme/Admin/img/user-avatar.png" alt="" />                                <span class="username">
                                    <? echo $_SESSION['a_login'] ?>                                </span>
							</a>
						</li>
							<li class="">

							
								
                                    <a href="?exit"><img src="https://demo.betscheme.com/theme/Admin/img/door-exit.png" alt="" />                                        
                                     
                                    </a>
                               
							
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->

    <div id="container" class="row-fluid">

        <!-- BEGIN SIDEBAR -->
        
<div id="sidebar" class="nav-collapse collapse">
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->

    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
    <div class="navbar-inverse">
        <form class="navbar-search visible-phone">
            <input type="text" class="search-query" placeholder="Search" />
        </form>
    </div>
    <!-- END RESPONSIVE QUICK SEARCH FORM -->
    <!-- BEGIN SIDEBAR MENU -->
	
    <ul class="sidebar-menu loading">
                                    <li class="has-sub hidden" >
                    <a href="javascript:;" class="">
                        <span class="icon-box"><i class="icon-book"></i></span>
                        საიტის მართვა                        <span class="arrow"></span>
                    </a>
                                            <ul class="sub">
                                                                                                                                        <li><a href="jewelry.php">მანქანების დამატება</a></li><li><a href="jewelry2.php">სასტუმროს დამატება</a></li><li><a href="n.php">სიახლეების დამატება</a></li><li><a href="jewelry3.php">შეკვეთები</a></li><li><a href="jewelry5.php">სერვისები</a></li><li><a href="jewelry4.php">კონტაქტი</a></li>
                                                                                                                                                                                                          
                                                                                                                        </ul>
                                    </li>
									
									
									
									<li class="has-sub hidden" >
                    <a href="javascript:;" class="">
                        <span class="icon-box"><i class="icon-book"></i></span>
                        სისტემის მართვა                       <span class="arrow"></span>
                    </a>
                                            <ul class="sub">
                                                                                                                                        <li><a href="header.php">სლაიდერი</a></li><li><a href="other.php">სხვა პარამეტრები</a></li><li><a href="title.php">Title-ების შეცვლა</a></li>
                                                                                                                                                                                                          
                                                                                                                        </ul>
                                    </li>
                           
                            
	</ul>
    <script type="text/javascript">
        $(function() {
            var container = $('.sidebar-menu');
            container.find('a[href="/users.php"]').parent().parent().toggle('open');
            $('.sidebar-menu ul').each(function() {
                if($(this).find('ul li').context.childElementCount == 0) {
                    $(this).parent().css({'display' : 'none'});
                }else{

                }
            });
            container.removeClass('loading');
            container.find('li').removeClass('hidden');
        }());
    </script>
    <!-- END SIDEBAR MENU -->
</div>		<!-- END SIDEBAR -->

		<!-- BEGIN PAGE -->
		<div id="main-content">
			<!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <ul class="breadcrumb">
                                    <li>
                    <a href="/eng"><i class="icon-home"></i></a>
                    <span class="divider">&nbsp;</span>
                </li>
                                                <li>
                    სასტუმროები                   <span class="divider">&nbsp;</span>
                </li>
                                                <li>
                    სასტუმროების სია                  <span class="divider-last">&nbsp;</span>
                </li>
                        </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div id="page" class="dashboard">		
<?
if(isset($_GET['add']))
{
	//$id = mysqli_real_escape_string($GLOBALS['db'],$_GET['add']);
	//$Collection4 = mysqli_query($GLOBALS['db'],"SELECT * FROM collections WHERE id='".$id."'");
	//$CollectionRow4 = mysqli_fetch_array($Collection4);	
	echo '<div class="row-fluid ">
            <div class="span12">
                <!-- BEGIN INLINE TABS PORTLET-->
                <div class="widget">
				<div class="widget-body">
                        <div class="row-fluid">
                            <div class="span12">
                                <!--BEGIN TABS-->
                                <div class="table table-custom">
								 <h3>სასტუმროს დამატება</h3> 
								 <br />';
								  echo '<form action="jewelry2.php?add='.$ProductRow3[id].'" id="UserAdminEditForm" enctype="multipart/form-data" method="POST" accept-charset="utf-8"><div style="display:none;"><input type="hidden" name="_method" value="PUT"/></div><fieldset>
								 
<div class="input text"><label for="UserUsername">მთავარი სურათი</label><input name="m_pic" type="file" accept="image/*" id="UserUsername"/></div>
								 
								 <div class="input text"><label for="UserEmail">სახელი(GEO)</label><input name="name" type="text" value="" id="UserEmail"/></div>
								  <div class="input text"><label for="UserEmail">სახელი(RUS)</label><input name="name2" type="text" value="" id="UserEmail"/></div>
								   <div class="input text"><label for="UserEmail">სახელი(ENG)</label><input name="name3" type="text" value="" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">მიმართულება</label><select name="compas">';

									 echo '
									 <option value="0">თბილისი</option>
									 <option value="1">დასავლეთი</option>
									 <option value="2">აღმოსავლეთი</option>
									 ';

								 
								 echo '</select></div>
								 <div class="input text"><label for="UserEmail">მდებარეობა(geo)</label><input name="loc" type="text" value="" id="UserEmail"/></div>
								 
								 <div class="input text"><label for="UserEmail">მდებარეობა(rus)</label><input name="locrus" type="text" value="" id="UserEmail"/></div>
								 
								 <div class="input text"><label for="UserEmail">მდებარეობა(eng)</label><input name="loceng" type="text" value="" id="UserEmail"/></div>
								 <div class="input text"><label for="UserFirstName">სრული აღწერა(eng)</label><textarea name="ldescr" id="editor2" rows="10" cols="80" type="text"></textarea>
								<script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( "editor2" );
            </script>
								</div>
								
								<div class="input text"><label for="UserFirstName">სრული აღწერა(rus)</label><textarea name="ldescrrus" id="editor20" rows="10" cols="80" type="text"></textarea>
								<script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( "editor20" );
            </script>
								</div>
								
								<div class="input text"><label for="UserFirstName">სრული აღწერა(geo)</label><textarea name="ldescrgeo" id="editor21" rows="10" cols="80" type="text"></textarea>
								<script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( "editor21" );
            </script>
								</div>
								 <div class="input text"><label for="UserEmail">ტიპი</label><select name="type">';

									 echo '
									 <option value="0">სასტუმრო</option>
									 <option value="1">ტური</option>
									';
								 echo '</select></div>
								 
								 
								 
								 <div class="input text"><label for="UserDateOfBirth">pdf</label><input name="pdf" type="file" accept="image/*" id="UserUsername"/></div>
								 
								 <div class="input text"><label for="UserDateOfBirth">სურ 1</label><input name="f1" type="file" accept="image/*" id="UserUsername"/></div>
								 <div class="input text"><label for="UserDateOfBirth">სურ 2</label><input name="f2" type="file" accept="image/*" id="UserUsername"/></div>
								 <div class="input text"><label for="UserDateOfBirth">სურ 3</label><input name="f3" type="file" accept="image/*" id="UserUsername"/></div>
								 <div class="input text"><label for="UserDateOfBirth">სურ 4</label><input name="f4" type="file" accept="image/*" id="UserUsername"/></div>
								 
<div class="submit"><input  class="btn" style="margin-top: 15px;" type="submit" value="Submit"/></div></form>';
                                 echo '                                 </div>
								</div></div></div></div></div></div>';
								if(isset($_POST['name']) and isset($_POST['name2']) and isset($_POST['name3']) and isset($_POST['loc']) and isset($_POST['locrus']) and isset($_POST['loceng']) and isset($_POST['ldescr']) and isset($_POST['ldescrrus']) and isset($_POST['ldescrgeo']))
								{
									$name = mysqli_real_escape_string($GLOBALS['db'],$_POST['name']);
									$name22 = mysqli_real_escape_string($GLOBALS['db'],$_POST['name2']);
									$name33 = mysqli_real_escape_string($GLOBALS['db'],$_POST['name3']);
									$loc = $_POST[loc];
									$locrus = $_POST[locrus];
									$loceng = $_POST[loceng];
									
									$ldescr = mysqli_real_escape_string($GLOBALS['db'],$_POST['ldescr']);
									$ldescrrus = mysqli_real_escape_string($GLOBALS['db'],$_POST['ldescrrus']);
									$ldescrgeo = mysqli_real_escape_string($GLOBALS['db'],$_POST['ldescrgeo']);
									$type = $_POST['type'];
									$compas = $_POST['compas'];
									
									$filep = $_FILES['m_pic']['tmp_name']; 
									$filep1 = $_FILES['f1']['tmp_name']; 
									$filep2 = $_FILES['f2']['tmp_name']; 
									$filep3 = $_FILES['f3']['tmp_name']; 
									$filep4 = $_FILES['f4']['tmp_name']; 
									
									if(empty($name) or empty($name2) or empty($name3) or empty($loc) or empty($locrus) or empty($loceng) or empty($ldescr) or empty($ldescrrus) or empty($ldescrgeo))
									{
										echo '<h2 style="color:red;"><b>თქვენ გამოტოვეთ საჭირო ველები!!!</b></h2>';
									}
									else
									{
										$ftp_server = 'rentcartbilisi.com';
															$ftp_user_name = 'admin4';
															$ftp_user_pass = 'N12345678';
										$name23 = uniqid();
										
										$name1 = uniqid();
										$name2 = uniqid();
										$name3 = uniqid();
										$name4 = uniqid();
										$name5 = uniqid();
										
										$name23 = $name23.'.png';
										
										$name1 = $name1.'.png';
										$name2 = $name2.'.png';
										$name3 = $name3.'.png';
										$name4 = $name4.'.png';
										$name5 = $name5.'.pdf';
										$conn_id = ftp_connect($ftp_server);
										$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
										// загружаем файл
										$upload = ftp_put($conn_id, 'www/rentcartbilisi.com/hotels/'.$name23, $filep, FTP_BINARY);
										
										$upload1 = ftp_put($conn_id, 'www/rentcartbilisi.com/hotels/'.$name1, $filep1, FTP_BINARY);
										$upload2 = ftp_put($conn_id, 'www/rentcartbilisi.com/hotels/'.$name2, $filep2, FTP_BINARY);
										$upload3 = ftp_put($conn_id, 'www/rentcartbilisi.com/hotels/'.$name3, $filep3, FTP_BINARY);
										$upload4 = ftp_put($conn_id, 'www/rentcartbilisi.com/hotels/'.$name4, $filep4, FTP_BINARY);
										$upload5 = ftp_put($conn_id, 'www/rentcartbilisi.com/docs/'.$name5, $filep5, FTP_BINARY);

										// проверяем статус загрузки
										$AddTour = mysqli_query($GLOBALS['db'],"INSERT INTO hoto(`name_geo`,`name_rus`,`name_eng`,`main_pic`,`detailed_geo`,`detailed_rus`,`detailed_eng`,`desc_geo`,`desc_rus`,`desc_eng`,`type`,`compas_geo`,`pdflink`,`pic1`,`pic2`,`pic3`,`pic4`) VALUES('$name','$name23','$name33','http://geosky.ge/tour_pic/$name23','$loc','$locrus','$loceng','$ldescrgeo','$ldescr','$ldescrrus','$type','$compas','http://rentcartbilisi.com/docs/$name5','http://rentcartbilisi.com/hotels/$name1','http://rentcartbilisi.com/hotels/$name2','http://rentcartbilisi.com/hotels/$name3','http://rentcartbilisi.com/hotels/$name4')");
										if($AddTour == true)
										{
											echo '<h2 style="color:green;"><b>ტური '.$ProductRow3[name].' დამატებულია!!!</b></h2>';
										}
										else
										{
											echo 'შეცდომაა';
										}
											
									}
								}

								



                                 echo '                                 </div>
								</div></div></div></div></div></div>';
}
if(isset($_GET['edit']) and !empty($_GET['edit']))
{
$id = mysqli_real_escape_string($GLOBALS['db'],$_GET['edit']);
$Product3 = mysqli_query($GLOBALS['db'],"SELECT * FROM hoto WHERE id='".$id."'");
$ProductRow3 = mysqli_fetch_array($Product3);	
	echo '<div class="row-fluid ">
            <div class="span12">
                <!-- BEGIN INLINE TABS PORTLET-->
                <div class="widget">
				<div class="widget-body">
                        <div class="row-fluid">
                            <div class="span12">
                                <!--BEGIN TABS-->
                                <div class="table table-custom">
								 <h3>სასტუმროს რედაქტირება <b>'.$ProductRow3[name].'</b></h3> 
								 <br />';
								 echo '<form action="jewelry2.php?edit='.$ProductRow3[id].'" id="UserAdminEditForm" enctype="multipart/form-data" method="POST" accept-charset="utf-8"><div style="display:none;"><input type="hidden" name="_method" value="PUT"/></div><fieldset>
								 
<div class="input text"><label for="UserUsername">მთავარი სურათი</label><input name="m_pic" type="file" accept="image/*" id="UserUsername"/></div>
								 
								 <div class="input text"><label for="UserEmail">სახელი(GEO)</label><input name="name" type="text" value="'.$ProductRow3[name_geo].'" id="UserEmail"/></div>
								  <div class="input text"><label for="UserEmail">სახელი(RUS)</label><input name="name2" type="text" value="'.$ProductRow3[name_rus].'" id="UserEmail"/></div>
								   <div class="input text"><label for="UserEmail">სახელი(ENG)</label><input name="name3" type="text" value="'.$ProductRow3[name_eng].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">მიმართულება</label><select name="compas">';
								 if($ProductRow3['compas_geo'] == 0)
								 {
									 echo '
									 <option value="0">თბილისი</option>
									 <option value="1">დასავლეთი</option>
									 <option value="2">აღმოსავლეთი</option>
									 ';
								 }
								 if($ProductRow3['compas_geo'] == 1)
								 {
									 echo '
									 <option value="1">დასავლეთი</option>
									 <option value="0">თბილისი</option>
									 
									 <option value="2">აღმოსავლეთი</option>
									 ';
								 }
								 if($ProductRow3['compas_geo'] == 2)
								 {
									 echo '
									 <option value="2">აღმოსავლეთი</option>
									 <option value="0">თბილისი</option>
									 <option value="1">დასავლეთი</option>
									 
									 ';
								 }
								 echo '</select></div>
								 <div class="input text"><label for="UserEmail">მდებარეობა(geo)</label><input name="loc" type="text" value="'.$ProductRow3[detailed_geo].'" id="UserEmail"/></div>
								 
								 <div class="input text"><label for="UserEmail">მდებარეობა(rus)</label><input name="locrus" type="text" value="'.$ProductRow3[detailed_rus].'" id="UserEmail"/></div>
								 
								 <div class="input text"><label for="UserEmail">მდებარეობა(eng)</label><input name="loceng" type="text" value="'.$ProductRow3[detailed_eng].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserFirstName">სრული აღწერა(eng)</label><textarea name="ldescr" id="editor2" rows="10" cols="80" type="text">'.$ProductRow3[desc_eng].'</textarea>
								<script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( "editor2" );
            </script>
								</div>
								
								<div class="input text"><label for="UserFirstName">სრული აღწერა(rus)</label><textarea name="ldescrrus" id="editor20" rows="10" cols="80" type="text">'.$ProductRow3[desc_rus].'</textarea>
								<script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( "editor20" );
            </script>
								</div>
								
								<div class="input text"><label for="UserFirstName">სრული აღწერა(geo)</label><textarea name="ldescrgeo" id="editor21" rows="10" cols="80" type="text">'.$ProductRow3[desc_geo].'</textarea>
								<script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( "editor21" );
            </script>
								</div>
								 <div class="input text"><label for="UserEmail">ტიპი</label><select name="type">';
								 if($ProductRow3['type'] == 0)
								 {
									 echo '
									 <option value="0">სასტუმრო</option>
									 <option value="1">ტური</option>
									 ';
								 }
								 if($ProductRow3['type'] == 1)
								 {
									 echo '
									 <option value="1">ტური</option>
									 <option value="0">სასტუმრო</option>
									 ';
								 }
								 echo '</select></div>
								 
								 
								 
								 <div class="input text"><label for="UserDateOfBirth">pdf</label><input name="pdf" type="file" accept="image/*" id="UserUsername"/></div>
								 
								 <div class="input text"><label for="UserDateOfBirth">სურ 1</label><input name="f1" type="file" accept="image/*" id="UserUsername"/></div>
								 <div class="input text"><label for="UserDateOfBirth">სურ 2</label><input name="f2" type="file" accept="image/*" id="UserUsername"/></div>
								 <div class="input text"><label for="UserDateOfBirth">სურ 3</label><input name="f3" type="file" accept="image/*" id="UserUsername"/></div>
								 <div class="input text"><label for="UserDateOfBirth">სურ 4</label><input name="f4" type="file" accept="image/*" id="UserUsername"/></div>
								 
<div class="submit"><input  class="btn" style="margin-top: 15px;" type="submit" value="Submit"/></div></form>';
                                 echo '                                 </div>
								</div></div></div></div></div></div>';
								if(isset($_POST['name']) and isset($_POST['name2']) and isset($_POST['name3']) and isset($_POST['loc']) and isset($_POST['locrus']) and isset($_POST['loceng']) and isset($_POST['ldescr']) and isset($_POST['ldescrrus']) and isset($_POST['ldescrgeo']))
								{
									$name = mysqli_real_escape_string($GLOBALS['db'],$_POST['name']);
									$name2 = mysqli_real_escape_string($GLOBALS['db'],$_POST['name2']);
									$name3 = mysqli_real_escape_string($GLOBALS['db'],$_POST['name3']);
									$loc = $_POST[loc];
									$locrus = $_POST[locrus];
									$loceng = $_POST[loceng];
									
									$ldescr = mysqli_real_escape_string($GLOBALS['db'],$_POST['ldescr']);
									$ldescrrus = mysqli_real_escape_string($GLOBALS['db'],$_POST['ldescrrus']);
									$ldescrgeo = mysqli_real_escape_string($GLOBALS['db'],$_POST['ldescrgeo']);
									$type = $_POST['type'];
									$compas = $_POST['compas'];
									
									$filep = $_FILES['m_pic']['tmp_name']; 
									$filep1 = $_FILES['f1']['tmp_name']; 
									$filep2 = $_FILES['f2']['tmp_name']; 
									$filep3 = $_FILES['f3']['tmp_name']; 
									$filep4 = $_FILES['f4']['tmp_name']; 
									$filep5 = $_FILES['pdf']['tmp_name']; 
									
									if(empty($name) or empty($name2) or empty($name3) or empty($loc) or empty($locrus) or empty($loceng) or empty($ldescr) or empty($ldescrrus) or empty($ldescrgeo))
									{
										echo '<h2 style="color:red;"><b>თქვენ გამოტოვეთ საჭირო ველები!!!</b></h2>';
									}
									else
									{
										$UpdateProduct = mysqli_query($GLOBALS['db'],"UPDATE hoto SET name_geo='$name', name_rus='$name2', name_eng='$name3', main_pic='$main_pic', detailed_geo='$loc', detailed_rus='$locrus', detailed_eng='$loceng', desc_eng='$ldescr', desc_rus='$ldescrrus', desc_geo='$ldescrgeo', type='$type', compas_geo='$compas' WHERE id='".$id."'") or die(mysqli_error());
										if($UpdateProduct == true)
										{
											echo '<h2 style="color:green;"><b>სასტუმრო '.$ProductRow3[name].' შესწორებულია!!!</b></h2>';
										}
										else
										{
											echo 'შეცდომაა';
										}
										$ftp_server = 'rentcartbilisi.com';
															$ftp_user_name = 'admin4';
															$ftp_user_pass = 'N12345678';
											$name23 = uniqid();
											
											$name1 = uniqid();
											$name2 = uniqid();
											$name3 = uniqid();
											$name4 = uniqid();
											$name5 = uniqid();
											
											$name23 = $name23.'.png';
											
											$name1 = $name1.'.png';
											$name2 = $name2.'.png';
											$name3 = $name3.'.png';
											$name4 = $name4.'.png';
											$name5 = $name5.'.pdf';
											$conn_id = ftp_connect($ftp_server);
											$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
											// загружаем файл
											
											
											
											if(!empty($filep))
											{
												$upload = ftp_put($conn_id, 'www/rentcartbilisi.com/hotels/'.$name23, $filep, FTP_BINARY);
												$UpdateProduct = mysqli_query($GLOBALS['db'],"UPDATE hoto SET main_pic='http://rentcartbilisi.com/hotels/$name23' WHERE id='".$id."'") or die(mysqli_error());
											}
											if(!empty($filep1))
											{
												$upload1 = ftp_put($conn_id, 'www/rentcartbilisi.com/hotels/'.$name1, $filep1, FTP_BINARY);
												$UpdateProduct = mysqli_query($GLOBALS['db'],"UPDATE hoto SET pic1='http://rentcartbilisi.com/hotels/$name1' WHERE id='".$id."'") or die(mysqli_error());
											}
											if(!empty($filep2))
											{
												$upload1 = ftp_put($conn_id, 'www/rentcartbilisi.com/hotels/'.$name2, $filep2, FTP_BINARY);
												$UpdateProduct = mysqli_query($GLOBALS['db'],"UPDATE hoto SET pic2='http://rentcartbilisi.com/hotels/$name2' WHERE id='".$id."'") or die(mysqli_error());
											}
											if(!empty($filep3))
											{
												$upload1 = ftp_put($conn_id, 'www/rentcartbilisi.com/hotels/'.$name3, $filep3, FTP_BINARY);
												$UpdateProduct = mysqli_query($GLOBALS['db'],"UPDATE hoto SET pic3='http://rentcartbilisi.com/hotels/$name3' WHERE id='".$id."'") or die(mysqli_error());
											}
											if(!empty($filep4))
											{
												$upload1 = ftp_put($conn_id, 'www/rentcartbilisi.com/hotels/'.$name4, $filep4, FTP_BINARY);
												$UpdateProduct = mysqli_query($GLOBALS['db'],"UPDATE hoto SET pic4='http://rentcartbilisi.com/hotels/$name4' WHERE id='".$id."'") or die(mysqli_error());
											}
											if(!empty($filep5))
											{
												$upload1 = ftp_put($conn_id, 'www/rentcartbilisi.com/docs/'.$name5, $filep5, FTP_BINARY);
												$UpdateProduct = mysqli_query($GLOBALS['db'],"UPDATE hoto SET pdflink='http://rentcartbilisi.com/docs/$name5' WHERE id='".$id."'") or die(mysqli_error());
											}
											
									}
								}
}
if(isset($_GET['del']) and !empty($_GET['del']))
{
$id = mysqli_real_escape_string($GLOBALS['db'],$_GET['del']);
$Product2 = mysqli_query($GLOBALS['db'],"SELECT * FROM Hoto WHERE id='".$id."'");
$ProductRow2 = mysqli_fetch_array($Product2);
echo '<div class="row-fluid ">
            <div class="span12">
                <!-- BEGIN INLINE TABS PORTLET-->
                <div class="widget">
				<div class="widget-body">
                        <div class="row-fluid">
                            <div class="span12">
                                <!--BEGIN TABS-->
                                <div class="table table-custom">
								 <h3>სასტუმროს/ტურის <b>'.$ProductRow2[name].' წაშლა</b></h3> 
								 <br />
                                    <div class="tab-content">
                                        Are you sure?<br> <a href="jewelry2.php?del='.$_GET[del].'&yes">Yes, of course</a><br><a href="jewelry2.php" style="color:red;">No</a>                                    </div>
                                </div>
								</div></div></div></div></div></div>';
								if(isset($_GET[yes]))
								{
									$DelProduct = mysqli_query($GLOBALS['db'],"DELETE FROM hoto WHERE id='".$id."'");
									if($DelProduct == true)
									{
										echo '<h2 style="color:green;"><b>Hotel '.$ProductRow2[name].' has been deleted</b></h2>';
									}
								}
								
}
if(!isset($_GET['edit']) and !isset($_GET['del']) and !isset($_GET['add']))
{
echo '<div class="row-fluid ">
            <div class="span12">
                <!-- BEGIN INLINE TABS PORTLET-->
                <div class="widget">
				<div class="widget-body">
                        <div class="row-fluid">
                            <div class="span12">
                                <!--BEGIN TABS-->
                                <div class="table table-custom">
								 <ul class="nav nav-tabs">
                                    <li class="active">
                    <a href="#">სია</a>                </li>
                            <li >
                    <a href="jewelry2.php?add">დამატება</a>                </li>
                        </ul>
								 <br />
								 <div class="tab-content">
									
									<div class="row-fluid">
        <div class="span12">
            <!-- BEGIN TABLE widget-->
            <div class="widget">
                <div class="widget-title">
                    <h4>
                        <i class="icon-reorder"></i>
                                                    Data list                                            </h4>

                </div>
                <div class="widget-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                                                                <th>
                                        <a href="/users.php/index/sort:id/direction:asc" class="desc">Id</a>                                    </th>
                                                                    <th>
                                        <a href="/users.php/index/sort:username/direction:asc">Image</a>                                    </th>
                                                                    <th>
                                        <a href="/users.php/index/sort:email/direction:asc">Name(geo)</a>                                    </th>
                                                                    <th>
                                        <a href="/users.php/index/sort:registration_date/direction:asc">location(GEO)</a>                                    </th>
										
										<th>
                                        <a href="/users.php/index/sort:last_visit/direction:asc">Type</a>                                    </th>
										<th>
                                        <a href="/users.php/index/sort:last_visit/direction:asc">PDF</a>                                    </th>
										
										
										
								
										
                                
                                                            <th>
                                    Actions                                </th>
                            
                                                    </tr>
                        </thead>
                        <tbody>';

                       
					   $Product = mysqli_query($GLOBALS['db'],"SELECT * FROM hoto ORDER BY id DESC") or die(mysqli_error());
					   $ProductRow = mysqli_fetch_array($Product);
					   do
					   {
						   echo "<tr><td class=''>".$ProductRow[id]."</td><td class=''><img src='".$ProductRow[main_pic]."' width='90' height='90'></td><td class=''>".$ProductRow[name_geo]."</td>";					   
						  echo "<td class=''>".$ProductRow[detailed_geo]."</td><td class=''>";
						  if($ProductRow[type] == '0')
						  {
							  echo 'სასტუმრო';
						  }
						  if($ProductRow[type] == '1')
						  {
							  echo 'ტური';
						  }
						  echo "</td><td class=''>".$ProductRow[pdflink]."</td>   <td style='min-width: 190px;' class='actions'>
	<a href='?edit=$ProductRow[id]' class='btn btn-mini btn-primary'>Edit</a> <a href='?del=$ProductRow[id]' class='btn btn-mini btn-success'>Delete</a>  </td>                       </tr>";
					   }
					   while($ProductRow = mysqli_fetch_array($Product));
					   

						
						
                                               echo ' <tr>
                                                            <td>
                                                           </td>
                                                            <td>
                                                           </td>
                                                            <td>
                                                           </td>
                                                            <td>
                                                           </td>
                                                            <td>
                                                           </td>
                                                                                    <td></td>
                                                    </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END TABLE widget-->
        </div>
    </div>

								 
                                                                  </div>
								</div></div></div></div></div></div>';	
}
		?>
    </div>
    <!-- END PAGE CONTENT-->
</div>			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="Admin/assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
	<script src="Admin/assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="Admin/assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
	<script src="Admin/assets/bootstrap/js/bootstrap.min.js"></script>
    
	<script type="text/javascript" src="Admin/js/jquery.blockui.js"></script>
    
	<script type="text/javascript" src="Admin/js/jquery.cookie.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
    
	<script type="text/javascript" src="https://demo.betscheme.com/theme/Admin/js/excanvas.js"></script>
    
	<script type="text/javascript" src="https://demo.betscheme.com/theme/Admin/js/respond.js"></script>
	<![endif]-->


    <!-- ie8 fixes -->
    <!--[if lt IE 9]>
    
	
    
	
    <![endif]-->
    <script src="Admin/assets/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
    <script type="text/javascript" src="Admin/assets/jquery-knob/js/jquery.knob.js"></script>
	<script type="text/javascript" src="Admin/js/jquery.peity.min.js"></script>
    <script type="text/javascript" src="Admin/assets/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    
	<script type="text/javascript" src="Admin/js/scripts.js"></script>

    <script type="text/javascript" src="Admin/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="Admin/assets/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="Admin/assets/clockface/js/clockface.js"></script>
    <script type="text/javascript" src="Admin/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="Admin/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>

    <script type="text/javascript" src="Admin/assets/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="Admin/assets/fancybox/source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="Admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <script type="text/javascript" src="Admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

    <!--[if lte IE 8]>
    <script language="javascript" type="text/javascript" src="https://demo.betscheme.com/theme/Admin/assets/flot/excanvas.min.js"></script>
    <![endif]-->

    <script type="text/javascript" src="Admin/assets/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="Admin/assets/flot/jquery.flot.pie.js"></script>
<!--Begin Comm100 Live Chat Code-->
<script>
		jQuery(document).ready(function() {
			// initiate layout and plugins
			App.setMainPage(true);
			App.init();
		});
	</script>
<?
if(isset($_GET['exit']))
{
	session_destroy();
	echo '<meta http-equiv="refresh" content="0; url=http://geosky.ge/admin" />';
}
?>
</body>
<!-- END BODY -->
</html>
