<?
session_start();
if(!isset($_SESSION[a_id]))
{
	die("Only authorized users can enter here");
}
//////////
include("../db.php");
//////////
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
 <script src="ckeditor/ckeditor.js"></script>
<link rel="stylesheet" type="text/css" href="../css/main.css">
<script type="text/javascript" src="../js/jquery.meerkat.1.0.js"></script>
<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	meerkat({
		close: '.close',
		dontShow: '.dont-show',
		animation: 'slide',
		animationSpeed: 500,
		dontShowExpire: 0,
		removeCookie: '.remove-cookie',
		meerkatPosition: 'bottom',
		background: '#2e2a22 url(../../img/meerkat-bg.png) repeat-x 0 0',
		height: '110px'
	});

});
</script>
	<meta charset="utf-8" />
    <title>მანქანები</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="Admin/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="Admin/assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="Admin/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />


	<link rel="stylesheet" type="text/css" href="Admin/css/style.css" />
	<link rel="stylesheet" type="text/css" href="Admin/css/style_responsive.css" />
	<link rel="stylesheet" type="text/css" href="Admin/css/style_gray.css" />

	<link href="Admin/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link href="Admin/assets/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
	<link href="Admin/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="Admin/assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="Admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
    <link href="Admin/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" rel="stylesheet" type="text/css" />


	<script type="text/javascript" src="Admin/js/jquery-1.8.3.min.js"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="administration.php">
                    <img src="http://rentcartbilisi.com/logo.png" width="100px" height="100px" alt="">

                </a>
				<!-- END LOGO -->

				<div id="top_menu" class="nav notify-row">
                    <!-- BEGIN NOTIFICATION -->
                                    </div>
                <!-- END  NOTIFICATION -->

                <div class="top-nav ">
                    <ul class="nav pull-right top-menu">
                        						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="https://demo.betscheme.com/theme/Admin/img/user-avatar.png" alt="" />                                <span class="username">
                                    <? echo $_SESSION['a_login'] ?>                                </span>
							</a>
						</li>
							<li class="">



                                    <a href="?exit"><img src="https://demo.betscheme.com/theme/Admin/img/door-exit.png" alt="" />

                                    </a>


						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->

    <div id="container" class="row-fluid">

        <!-- BEGIN SIDEBAR -->

<div id="sidebar" class="nav-collapse collapse">
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->

    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
    <div class="navbar-inverse">
        <form class="navbar-search visible-phone">
            <input type="text" class="search-query" placeholder="Search" />
        </form>
    </div>
    <!-- END RESPONSIVE QUICK SEARCH FORM -->
    <!-- BEGIN SIDEBAR MENU -->

    <ul class="sidebar-menu loading">
                                    <li class="has-sub hidden" >
                    <a href="javascript:;" class="">
                        <span class="icon-box"><i class="icon-book"></i></span>
                        საიტის მართვა                        <span class="arrow"></span>
                    </a>
                                            <ul class="sub">
                                                                                                                                        <li><a href="jewelry.php">მანქანების დამატება</a></li><li><a href="jewelry2.php">სასტუმროს დამატება</a></li><li><a href="n.php">სიახლეების დამატება</a></li><li><a href="jewelry3.php">შეკვეთები</a></li><li><a href="jewelry5.php">სერვისები</a></li><li><a href="jewelry4.php">კონტაქტი</a></li>

                                                                                                                        </ul>
                                    </li>



									<li class="has-sub hidden" >
                    <a href="javascript:;" class="">
                        <span class="icon-box"><i class="icon-book"></i></span>
                        სისტემის მართვა                       <span class="arrow"></span>
                    </a>
                                            <ul class="sub">
                                                                                                                                        <li><a href="header.php">სლაიდერი</a></li><li><a href="other.php">სხვა პარამეტრები</a></li><li><a href="title.php">Title-ების შეცვლა</a></li>

                                                                                                                        </ul>
                                    </li>


	</ul>
    <script type="text/javascript">
        $(function() {
            var container = $('.sidebar-menu');
            container.find('a[href="/users.php"]').parent().parent().toggle('open');
            $('.sidebar-menu ul').each(function() {
                if($(this).find('ul li').context.childElementCount == 0) {
                    $(this).parent().css({'display' : 'none'});
                }else{

                }
            });
            container.removeClass('loading');
            container.find('li').removeClass('hidden');
        }());
    </script>
    <!-- END SIDEBAR MENU -->
</div>		<!-- END SIDEBAR -->

		<!-- BEGIN PAGE -->
		<div id="main-content">
			<!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <ul class="breadcrumb">
                                    <li>
                    <a href="/eng"><i class="icon-home"></i></a>
                    <span class="divider">&nbsp;</span>
                </li>
                                                <li>
                    ტურები                   <span class="divider">&nbsp;</span>
                </li>
                                                <li>
                    ტურების სია                  <span class="divider-last">&nbsp;</span>
                </li>
                        </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div id="page" class="dashboard">
<?
if(isset($_GET['add']))
{
	//$id = mysqli_real_escape_string($_GET['add']);
	//$Collection4 = mysqli_query($GLOBALS['db'],"SELECT * FROM collections WHERE id='".$id."'");
	//$CollectionRow4 = mysqli_fetch_array($Collection4);
	echo '<div class="row-fluid ">
            <div class="span12">
                <!-- BEGIN INLINE TABS PORTLET-->
                <div class="widget">
				<div class="widget-body">
                        <div class="row-fluid">
                            <div class="span12">
                                <!--BEGIN TABS-->
                                <div class="table table-custom">
								 <h3>მანქანების დამატება</h3>
								 <br />';
								 echo '<form action="jewelry.php?add" id="UserAdminEditForm" enctype="multipart/form-data" method="POST" accept-charset="utf-8"><div style="display:none;"><input type="hidden" name="_method" value="PUT"/></div><fieldset>
								 <div class="input text"><label for="UserEmail">title_eng</label><input name="title_eng" type="text" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">title_rus</label><input name="title_rus" type="text" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">title_geo</label><input name="title_geo" type="text" id="UserEmail"/></div>
								 <br>
								 <div class="input text"><label for="UserFirstName">description_eng</label><textarea name="d_eng" id="editor44" rows="10" cols="80" type="text">'.$ProductRow3['desc_eng'].'</textarea>
													<script>
									// Replace the <textarea id="editor1"> with a CKEditor
									// instance, using default configuration.
									CKEDITOR.replace( "editor44" );
								</script>
									</div>
									<div class="input text"><label for="UserFirstName">description_rus</label><textarea name="d_rus" id="editor45" rows="10" cols="80" type="text">'.$ProductRow3['desc_rus'].'</textarea>
													<script>
									// Replace the <textarea id="editor1"> with a CKEditor
									// instance, using default configuration.
									CKEDITOR.replace( "editor45" );
								</script>
									</div>
									<div class="input text"><label for="UserFirstName">description_geo</label><textarea name="d_geo" id="editor46" rows="10" cols="80" type="text">'.$ProductRow3['desc_geo'].'</textarea>
													<script>
									// Replace the <textarea id="editor1"> with a CKEditor
									// instance, using default configuration.
									CKEDITOR.replace( "editor46" );
								</script>
									</div>
								 <br><br><br>
<div class="input text"><label for="UserUsername">მთავარი სურათი(1920X1080)</label><input name="m_pic" type="file" accept="image/*" id="UserUsername"/></div>
<div class="input text"><label for="UserEmail">სურათის აღწერა</label><input name="alt_main" type="text" value="" id="UserEmail"/></div>
								 <div class="input text"><label for="UserFirstName">მანქანის აღწერა(GEO)</label><textarea name="descgeo" id="editor2" rows="10" cols="80" type="text">'.$ProductRow3['description_geo'].'</textarea>
													<script>
									// Replace the <textarea id="editor1"> with a CKEditor
									// instance, using default configuration.
									CKEDITOR.replace( "editor2" );
								</script>
									</div>
									<div class="input text"><label for="UserFirstName">მანქანის აღწერა(RUS)</label><textarea name="descrus" id="editor3" rows="10" cols="80" type="text">'.$ProductRow3['description_rus'].'</textarea>
													<script>
									// Replace the <textarea id="editor1"> with a CKEditor
									// instance, using default configuration.
									CKEDITOR.replace( "editor3" );
								</script>
									</div>
									<div class="input text"><label for="UserFirstName">მანქანის აღწერა(ENG)</label><textarea name="desceng" id="editor4" rows="10" cols="80" type="text">'.$ProductRow3['description_eng'].'</textarea>
													<script>
									// Replace the <textarea id="editor1"> with a CKEditor
									// instance, using default configuration.
									CKEDITOR.replace( "editor4" );
								</script>
									</div>
								 <div class="input text"><label for="UserEmail">სახელი</label><input name="name" type="text" value="" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">მდებარეობა</label><input name="loc" type="text" value="" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">მწარმოებელი</label><input name="mark" type="text" value="" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">მოდელი</label><input name="model" type="text" value="" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">გამოშვების წელი</label><input name="year" type="text" value="" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ძრავის მოცულობა</label><input name="engine" type="text" value="" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფერი</label><input name="colours" type="text" value="" id="UserEmail"/></div>


								 <div class="input text"><label for="UserEmail">საწვავი 100კმ</label><input name="sto" type="text" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ბაკის ტევადობა</label><input name="capacity" type="text" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">კარების რაოდენობა</label><input name="doors" type="text" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ადგილების რაოდენობა</label><input name="seats" type="text" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">კონდინციონერი</label><input name="air" type="text" id="UserEmail"/></div>


								 <div class="input text"><label for="UserEmail">საჭე</label><input name="wheel" type="text" id="UserEmail"/></div>

								 <div class="input text"><label for="UserEmail">საჭე(RUS)</label><input name="wheel_rus" type="text" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">საჭე(ENG)</label><input name="wheel_eng" type="text" id="UserEmail"/></div>



								 <div class="input text"><label for="UserEmail">წამყვანი თვლები(GEO)</label><input name="wd_geo" type="text" id="UserEmail"/></div>

								 <div class="input text"><label for="UserEmail">წამყვანი თვლები(RUS)</label><input name="wd_rus" type="text" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">წამყვანი თვლები(ENG)</label><input name="wd_eng" type="text" id="UserEmail"/></div>



								 <div class="input text"><label for="UserEmail">კლასი</label><select name="class">';
									 echo '
									 <option value="pickup">PICKUP</option>
									 <option value="SUV">ჯიპი</option>
									 <option value="coupe">კუპე</option>
									 <option value="conv">კაბრიოლეტი</option>
									 <option value="sedan">სედანი</option>
									 <option value="mini">MINICAR</option>
									 <option value="hybrid">Hybris</option>
									 <option value="electric">electric</option>
									 ';
								 echo '</select></div>

								 <div class="input text"><label for="UserEmail">ტრანსმისია</label><select name="transm">';
									 echo '
									 <option value="1">ავტომატიკა</option>
									 <option value="2">მექანიკა</option>
									 <option value="3">ტიპტრონიკი</option>
									 ';

								 echo '</select></div>

								 <div class="input text"><label for="UserEmail">საწვავი</label><select name="fuel">';
									 echo '
									 <option value="1">ბენზინი</option>
									 <option value="2">დიზელი</option>
									 <option value="3">ჰიბრიდი</option>
									 <option value="4">ელექტრო</option>
									 ';

								 echo '</select></div>

								  <div class="input text"><label for="UserEmail">მხოლოდ მძღოლით?</label><select name="pop">';

									 echo '
									 <option value="0">არა</option>
									 <option value="1">კი</option>
									 ';
								 echo '</select></div>


								 <div class="input text"><label for="UserEmail">ფასი 1</label><input name="erti" type="text" value="'.$ProductRow3[price].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 3-4</label><input name="sami" type="text" value="'.$ProductRow3['3-4'].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 5-7</label><input name="xuti" type="text" value="'.$ProductRow3['5-7'].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 8-10</label><input name="rva" type="text" value="'.$ProductRow3['8-10'].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 11-15</label><input name="tertmeti" type="text" value="'.$ProductRow3['11-15'].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 16-21</label><input name="teqvsmeti" type="text" value="'.$ProductRow3['16-21'].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 22-25</label><input name="ocdaori" type="text" value="'.$ProductRow3['22-25'].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 26-27</label><input name="ocdaeqvsi" type="text" value="'.$ProductRow3['26-27'].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 28-29</label><input name="ocdarva" type="text" value="'.$ProductRow3['28-29'].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 30-31</label><input name="ocdaati" type="text" value="'.$ProductRow3['30-31'].'" id="UserEmail"/></div>


								 <div class="input text"><img src="'.$ProductRow3[pic1].'" height="240" width="120"></div>
								 <div class="input text"><label for="UserDateOfBirth">სურ 1(1920X1080)</label><input name="f1" type="file" accept="image/*" id="UserUsername"/></div>
								 <div class="input text"><label for="UserEmail">სურათის აღწერა 1</label><input name="alt1" type="text" value="" id="UserEmail"/></div>
								 <div class="input text"><img src="'.$ProductRow3[pic2].'" height="240" width="120"></div>
								 <div class="input text"><label for="UserDateOfBirth">სურ 2(1920X1080)</label><input name="f2" type="file" accept="image/*" id="UserUsername"/></div>
								 <div class="input text"><label for="UserEmail">სურათის აღწერა 2</label><input name="alt2" type="text" value="" id="UserEmail"/></div>
								 <div class="input text"><img src="'.$ProductRow3[pic3].'" height="240" width="120"></div>
								 <div class="input text"><label for="UserDateOfBirth">სურ 3(1920X1080)</label><input name="f3" type="file" accept="image/*" id="UserUsername"/></div>
								 <div class="input text"><label for="UserEmail">სურათის აღწერა 3</label><input name="alt3" type="text" value="" id="UserEmail"/></div>
								 <div class="input text"><img src="'.$ProductRow3[pic4].'" height="240" width="120"></div>
								 <div class="input text"><label for="UserDateOfBirth">სურ 4(1920X1080)</label><input name="f4" type="file" accept="image/*" id="UserUsername"/></div>
								 <div class="input text"><label for="UserEmail">სურათის აღწერა 4</label><input name="alt4" type="text" value="" id="UserEmail"/></div>
								 <div class="input text"><img src="'.$ProductRow3[pic5].'" height="240" width="120"></div>
								 <div class="input text"><label for="UserDateOfBirth">სურ 5(1920X1080)</label><input name="f5" type="file" accept="image/*" id="UserUsername"/></div>
								 <div class="input text"><label for="UserEmail">სურათის აღწერა 5</label><input name="alt5" type="text" value="" id="UserEmail"/></div>
<div class="submit"><input  class="btn" style="margin-top: 15px;" type="submit" value="Submit"/></div></form>';


									$name = mysqli_real_escape_string($GLOBALS['db'],$_POST['name']);
									$loc = $_POST[loc];
									$mark = $_POST['mark'];
									$model = $_POST['model'];
									$year = $_POST['year'];
									$engine = $_POST['engine'];
									$colours = $_POST['colours'];
									$trans = $_POST['transm'];
									$fuel = $_POST['fuel'];
									$class = $_POST['class'];
									$pop = $_POST['pop'];

									$erti = $_POST['erti'];
									$sami = $_POST['sami'];
									$xuti = $_POST['xuti'];
									$rva = $_POST['rva'];
									$tertmeti = $_POST['tertmeti'];
									$teqvsmeti = $_POST['teqvsmeti'];
									$ocdaori = $_POST['ocdaori'];
									$ocdaeqvsi = $_POST['ocdaeqvsi'];
									$ocdarva = $_POST['ocdarva'];
									$ocdaati = $_POST['ocdaati'];

									$ldescr = mysqli_real_escape_string($GLOBALS['db'],$_POST['ldescr']);
									$ldescrrus = mysqli_real_escape_string($GLOBALS['db'],$_POST['ldescrrus']);
									$ldescrgeo = mysqli_real_escape_string($GLOBALS['db'],$_POST['ldescrgeo']);

									$filep = $_FILES['m_pic']['tmp_name'];
									$filep1 = $_FILES['f1']['tmp_name'];
									$filep2 = $_FILES['f2']['tmp_name'];
									$filep3 = $_FILES['f3']['tmp_name'];
									$filep4 = $_FILES['f4']['tmp_name'];
									$filep5 = $_FILES['f5']['tmp_name'];

									if(empty($name))
									{
										echo '<h2 style="color:red;"><b>თქვენ გამოტოვეთ საჭირო ველები!!!</b></h2>';
									}
									else
									{


										$ftp_server = '107.180.41.168';
															$ftp_user_name = 'auto4rental@rentcartbilisi.com';
															$ftp_user_pass = 'C4d3e64c';
										$name23 = uniqid();

										$name1 = uniqid();
										$name2 = uniqid();
										$name3 = uniqid();
										$name4 = uniqid();
										$name5 = uniqid();

										$name23 = $name23.'.jpg';

										$name1 = $name1.'.jpg';
										$name2 = $name2.'.jpg';
										$name3 = $name3.'.jpg';
										$name4 = $name4.'.jpg';
										$name5 = $name5.'.jpg';
										$conn_id = ftp_connect($ftp_server);
										$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
										// загружаем файл
										$upload = ftp_put($conn_id, 'www/img/'.$name23, $filep, FTP_BINARY);
										echo $upload;
										$upload1 = ftp_put($conn_id, 'www/img/'.$name1, $filep1, FTP_BINARY);
										$upload2 = ftp_put($conn_id, 'www/img/'.$name2, $filep2, FTP_BINARY);
										$upload3 = ftp_put($conn_id, 'www/img/'.$name3, $filep3, FTP_BINARY);
										$upload4 = ftp_put($conn_id, 'www/img/'.$name4, $filep4, FTP_BINARY);
										$upload5 = ftp_put($conn_id, 'www/img/'.$name5, $filep5, FTP_BINARY);

										// проверяем статус загрузки
										
										$AddCatalog = mysqli_query($GLOBALS['db'],"INSERT INTO catalog(`driver`,`name`,`price`,`main_pic`,`mark`,`model`,`location`,`class`,`year`,`engine`,`transmision`,`fuel`,`colours`,`3-4`,`5-7`,`8-10`,`11-15`,`16-21`,`22-25`,`26-27`,`28-29`,`30-31`,`pic1`,`pic2`,`pic3`,`pic4`,`pic5`,`wheel`,`wheel_rus`,`wheel_eng`,`sto`,`capacity`,`doors`,`seats`,`air`,`alt_main`,`alt1`,`alt2`,`alt3`,`alt4`,`alt5`,`description_geo`,`description_rus`,`description_eng`,`wd_geo`,`wd_eng`,`wd_rus`,`title_eng`,`title_rus`,`title_geo`,`desc_eng`,`desc_rus`,`desc_geo`) VALUES('$pop','$name','$erti','http://rentcartbilisi.com/img/$name23','$mark','$model','$loc','$class','$year','$engine','$trans','$fuel','$colours','$sami','$xuti','$rva','$tertmeti','$teqvsmeti','$ocdaori','$ocdaeqvsi','$ocdarva','$ocdaati','http://rentcartbilisi.com/img/$name1','http://rentcartbilisi.com/img//$name2','http://rentcartbilisi.com/img/$name3','http://rentcartbilisi.com/img/$name4','http://rentcartbilisi.com/img/$name5','$_POST[wheel]','$_POST[wheel_rus]','$_POST[wheel_eng]','$_POST[sto]','$_POST[capacity]','$_POST[doors]','$_POST[seats]','$_POST[air]','$_POST[alt_main]','$_POST[alt1]','$_POST[alt2]','$_POST[alt3]','$_POST[alt4]','$_POST[alt5]','$_POST[descgeo]','$_POST[descrus]','$_POST[desceng]','$_POST[wd_geo]','$_POST[wd_eng]','$_POST[wd_rus]','$_POST[title_eng]','$_POST[title_rus]','$_POST[title_geo]','$_POST[d_eng]','$_POST[d_rus]','$_POST[d_geo]')") or die(mysqli_error($GLOBALS['db']));
										
										if($AddCatalog == true)
										{
											echo '<h2 style="color:green;"><b>მანქანა '.$name.' დამატებულია!!!</b></h2>';
										}
										else
										{
											echo 'შეცდომაა';
										}

									}






                                 echo '                                 </div>
								</div></div></div></div></div></div>';
}
if(isset($_GET['edit']) and !empty($_GET['edit']))
{
	$d = $_GET[d];
		if($d == 'b')
		{
			$Up = mysqli_query($GLOBALS['db'],"UPDATE catalog SET main_pic='' WHERE id='$_GET[edit]'");
		}
		if($d == '1')
		{
			$Up = mysqli_query($GLOBALS['db'],"UPDATE catalog SET pic1='' WHERE id='$_GET[edit]'");
		}
		if($d == '2')
		{
			$Up = mysqli_query($GLOBALS['db'],"UPDATE catalog SET pic2='' WHERE id='$_GET[edit]'");
		}
		if($d == '3')
		{
			$Up = mysqli_query($GLOBALS['db'],"UPDATE catalog SET pic3='' WHERE id='$_GET[edit]'");
		}
		if($d == '4')
		{
			$Up = mysqli_query($GLOBALS['db'],"UPDATE catalog SET pic4='' WHERE id='$_GET[edit]'");
		}
		if($d == '5')
		{
			$Up = mysqli_query($GLOBALS['db'],"UPDATE catalog SET pic5='' WHERE id='$_GET[edit]'");
		}
$id = mysqli_real_escape_string($GLOBALS['db'],$_GET['edit']);
$Product3 = mysqli_query($GLOBALS['db'],"SELECT * FROM catalog WHERE id='".$id."'");
$ProductRow3 = mysqli_fetch_array($Product3);
	echo '<div class="row-fluid ">
            <div class="span12">
                <!-- BEGIN INLINE TABS PORTLET-->
                <div class="widget">
				<div class="widget-body">
                        <div class="row-fluid">
                            <div class="span12">
                                <!--BEGIN TABS-->
                                <div class="table table-custom">
								 <h3>მანქანის რედაქტირება <b>'.$ProductRow3[name].'</b></h3>
								 <br />';
								 echo '<form action="jewelry.php?edit='.$ProductRow3[id].'" id="UserAdminEditForm" enctype="multipart/form-data" method="POST" accept-charset="utf-8"><div style="display:none;"><input type="hidden" name="_method" value="PUT"/></div><fieldset>
								 <div class="input text"><label for="UserEmail">title_eng</label><input name="title_eng" type="text" id="UserEmail" value="'.$ProductRow3['title_eng'].'"/></div>
								 <div class="input text"><label for="UserEmail">title_rus</label><input name="title_rus" type="text" id="UserEmail" value="'.$ProductRow3['title_rus'].'"/></div>
								 <div class="input text"><label for="UserEmail">title_geo</label><input name="title_geo" type="text" id="UserEmail" value="'.$ProductRow3['title_geo'].'"/></div>
								 <br>
								 <div class="input text"><label for="UserFirstName">description_eng</label><textarea name="d_eng" id="editor44" rows="10" cols="80" type="text">'.$ProductRow3['desc_eng'].'</textarea>
													<script>
									// Replace the <textarea id="editor1"> with a CKEditor
									// instance, using default configuration.
									CKEDITOR.replace( "editor44" );
								</script>
									</div>
									<div class="input text"><label for="UserFirstName">description_rus</label><textarea name="d_rus" id="editor45" rows="10" cols="80" type="text">'.$ProductRow3['desc_rus'].'</textarea>
													<script>
									// Replace the <textarea id="editor1"> with a CKEditor
									// instance, using default configuration.
									CKEDITOR.replace( "editor45" );
								</script>
									</div>
									<div class="input text"><label for="UserFirstName">description_geo</label><textarea name="d_geo" id="editor46" rows="10" cols="80" type="text">'.$ProductRow3['desc_geo'].'</textarea>
													<script>
									// Replace the <textarea id="editor1"> with a CKEditor
									// instance, using default configuration.
									CKEDITOR.replace( "editor46" );
								</script>
									</div>
								 <br><br><br>
								 <div class="input text"><img src="'.$ProductRow3[main_pic].'" height="240" width="120"></div>
<div class="input text"><label for="UserUsername">მთავარი სურათი(1920X1080)(<a href="?edit='.$id.'&d=b" style="color:red;">წაშლა</a>)</label><input name="m_pic" type="file" accept="image/*" id="UserUsername"/></div>
								 <div class="input text"><label for="UserEmail">სურათის აღწერა</label><input name="alt_main" type="text" value="'.$ProductRow3[alt_main].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ID(დალაგება)</label><input name="id1" type="text" value="'.$ProductRow3[id].'" id="UserEmail"/></div>

								 <div class="input text"><label for="UserFirstName">მანქანის აღწერა(GEO)</label><textarea name="descgeo" id="editor2" rows="10" cols="80" type="text">'.$ProductRow3['description_geo'].'</textarea>
													<script>
									// Replace the <textarea id="editor1"> with a CKEditor
									// instance, using default configuration.
									CKEDITOR.replace( "editor2" );
								</script>
									</div>
									<div class="input text"><label for="UserFirstName">მანქანის აღწერა(RUS)</label><textarea name="descrus" id="editor3" rows="10" cols="80" type="text">'.$ProductRow3['description_rus'].'</textarea>
													<script>
									// Replace the <textarea id="editor1"> with a CKEditor
									// instance, using default configuration.
									CKEDITOR.replace( "editor3" );
								</script>
									</div>
									<div class="input text"><label for="UserFirstName">მანქანის აღწერა(ENG)</label><textarea name="desceng" id="editor4" rows="10" cols="80" type="text">'.$ProductRow3['description_eng'].'</textarea>
													<script>
									// Replace the <textarea id="editor1"> with a CKEditor
									// instance, using default configuration.
									CKEDITOR.replace( "editor4" );
								</script>
									</div>
								 <div class="input text"><label for="UserEmail">სახელი</label><input name="name" type="text" value="'.$ProductRow3[name].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">მდებარეობა</label><input name="loc" type="text" value="'.$ProductRow3[location].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">მწარმოებელი</label><input name="mark" type="text" value="'.$ProductRow3[mark].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">მოდელი</label><input name="model" type="text" value="'.$ProductRow3[model].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">გამოშვების წელი</label><input name="year" type="text" value="'.$ProductRow3[year].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ძრავის მოცულობა</label><input name="engine" type="text" value="'.$ProductRow3[engine].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფერი</label><input name="colours" type="text" value="'.$ProductRow3[colours].'" id="UserEmail"/></div>


								 <div class="input text"><label for="UserEmail">საწვავი 100კმ</label><input name="sto" type="text" value="'.$ProductRow3[sto].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ბაკის ტევადობა</label><input name="capacity" type="text" value="'.$ProductRow3[capacity].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">კარების რაოდენობა</label><input name="doors" type="text" value="'.$ProductRow3[doors].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ადგილების რაოდენობა</label><input name="seats" type="text" value="'.$ProductRow3[seats].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">კონდინციონერი</label><input name="air" type="text" value="'.$ProductRow3[air].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">საჭე</label><input name="wheel" type="text" value="'.$ProductRow3[wheel].'" id="UserEmail"/></div>

								 <div class="input text"><label for="UserEmail">საჭე(RUS)</label><input name="wheel_rus" type="text" value="'.$ProductRow3[wheel_rus].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">საჭე(ENG)</label><input name="wheel_eng" type="text" value="'.$ProductRow3[wheel_eng].'" id="UserEmail"/></div>



								 <div class="input text"><label for="UserEmail">წამყვანი თვლები(GEO)</label><input name="wd_geo" type="text" value="'.$ProductRow3[wd_geo].'" id="UserEmail"/></div>

								 <div class="input text"><label for="UserEmail">წამყვანი თვლები(RUS)</label><input name="wd_rus" type="text" value="'.$ProductRow3[wd_rus].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">წამყვანი თვლები(ENG)</label><input name="wd_eng" type="text" value="'.$ProductRow3[wd_eng].'" id="UserEmail"/></div>




								 <div class="input text"><label for="UserEmail">მხოლოდ მძღოლით?</label><select name="pop">';
								 if($ProductRow3['driver'] == 1)
								 {
									 echo '
									 <option value="1">კი</option>
									 <option value="0">არა</option>
									 ';
								 }
								 if($ProductRow3['driver'] == 0)
								 {
									 echo '
									 <option value="0">არა</option>
									 <option value="1">კი</option>
									 ';
								 }
								 echo '</select></div>
								 <div class="input text"><label for="UserEmail">კლასი</label><select name="class">';
								 if($ProductRow3['class'] == 'service')
								 {
									 echo '
									 <option value="service">Service</option>
									 <option value="SUV">SUV</option>
									 <option value="sedan">სედანი</option>
									 ';
								 }
								 if($ProductRow3['class'] == 'SUV')
								 {
									 echo '
									 <option value="SUV">SUV</option>
									 <option value="sedan">სედანი</option>
									 <option value="service">Service</option>
									 ';
								 }
								 if($ProductRow3['class'] == 'sedan')
								 {
									 echo '
									 <option value="sedan">სედანი</option>
									 <option value="SUV">SUV</option>
									 <option value="service">Service</option>
									 ';
								 }

								 echo '</select></div>

								 <div class="input text"><label for="UserEmail">ტრანსმისია</label><select name="transm">';
								 if($ProductRow3['transmision'] == 1)
								 {
									 echo '
									 <option value="1">ავტომატიკა</option>
									 <option value="2">მექანიკა</option>
									 <option value="3">ტიპტრონიკი</option>
									 ';
								 }
								 if($ProductRow3['transmision'] == 2)
								 {
									 echo '
									 <option value="2">მექანიკა</option>
									 <option value="1">ავტომატიკა</option>
									 <option value="3">ტიპტრონიკი</option>
									 ';
								 }
								 if($ProductRow3['transmision'] == 3)
								 {
									 echo '
									 <option value="3">ტიპტრონიკი</option>
									 <option value="2">მექანიკა</option>
									 <option value="1">ავტომატიკა</option>
									 ';
								 }
								 echo '</select></div>

								 <div class="input text"><label for="UserEmail">საწვავი</label><select name="fuel">';
								 if($ProductRow3['fuel'] == 1)
								 {
									 echo '
									 <option value="1">ბენზინი</option>
									 <option value="2">დიზელი</option>
									 <option value="3">ჰიბრიდი</option>
									 <option value="4">ელექტრო</option>
									 ';
								 }
								 if($ProductRow3['fuel'] == 2)
								 {
									 echo '
									 <option value="2">დიზელი</option>
									 <option value="1">ბენზინი</option>
									 <option value="3">ჰიბრიდი</option>
									 <option value="4">ელექტრო</option>
									 ';
								 }
								 if($ProductRow3['fuel'] == 3)
								 {
									 echo '
									 <option value="3">ჰიბრიდი</option>
									 <option value="4">ელექტრო</option>
									 <option value="2">დიზელი</option>
									 <option value="1">ბენზინი</option>
									 ';
								 }
								 if($ProductRow3['fuel'] == 4)
								 {
									 echo '
									 <option value="4">ელექტრო</option>
									 <option value="3">ჰიბრიდი</option>

									 <option value="2">დიზელი</option>
									 <option value="1">ბენზინი</option>
									 ';
								 }
								 echo '</select></div>

								 <div class="input text"><label for="UserEmail">მიღება თბილისი</label><input name="pick_tbs" type="text" value="'.$ProductRow3[pick_tbs].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">მიღება თბილისის AIRPORT</label><input name="pick_tbs_air" type="text" value="'.$ProductRow3[pick_tbs_air].'" id="UserEmail"/></div>

								 <div class="input text"><label for="UserEmail">მიღება ბათუმი</label><input name="pick_btm" type="text" value="'.$ProductRow3[pick_btm].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">მიღება ბათუმის AIRPORT</label><input name="pick_btm_air" type="text" value="'.$ProductRow3[pick_btm_air].'" id="UserEmail"/></div>

								 <div class="input text"><label for="UserEmail">მიღება ქუთაისი</label><input name="pick_kut" type="text" value="'.$ProductRow3[pick_kut].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">მიღება ქუთაისის AIRPORT</label><input name="pick_kut_air" type="text" value="'.$ProductRow3[pick_kut_air].'" id="UserEmail"/></div>


								 <div class="input text"><label for="UserEmail">ჩაბარება თბილისი</label><input name="drop_tbs" type="text" value="'.$ProductRow3[drop_tbs].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ჩაბარება თბილისის AIRPORT</label><input name="drop_tbs_air" type="text" value="'.$ProductRow3[drop_tbs_air].'" id="UserEmail"/></div>

								 <div class="input text"><label for="UserEmail">ჩაბარება ბათუმი</label><input name="drop_btm" type="text" value="'.$ProductRow3[drop_btm].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ჩაბარება ბათუმის AIRPORT</label><input name="drop_btm_air" type="text" value="'.$ProductRow3[drop_btm_air].'" id="UserEmail"/></div>

								 <div class="input text"><label for="UserEmail">ჩაბარება ქუთაისი</label><input name="drop_kut" type="text" value="'.$ProductRow3[drop_kut].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ჩაბარება ქუთაისის AIRPORT</label><input name="drop_kut_air" type="text" value="'.$ProductRow3[drop_kut_air].'" id="UserEmail"/></div>



								 <div class="input text"><label for="UserEmail">ფასი 1</label><input name="erti" type="text" value="'.$ProductRow3[price].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 3-4</label><input name="sami" type="text" value="'.$ProductRow3['3-4'].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 5-7</label><input name="xuti" type="text" value="'.$ProductRow3['5-7'].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 8-10</label><input name="rva" type="text" value="'.$ProductRow3['8-10'].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 11-15</label><input name="tertmeti" type="text" value="'.$ProductRow3['11-15'].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 16-21</label><input name="teqvsmeti" type="text" value="'.$ProductRow3['16-21'].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 22-25</label><input name="ocdaori" type="text" value="'.$ProductRow3['22-25'].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 26-27</label><input name="ocdaeqvsi" type="text" value="'.$ProductRow3['26-27'].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 28-29</label><input name="ocdarva" type="text" value="'.$ProductRow3['28-29'].'" id="UserEmail"/></div>
								 <div class="input text"><label for="UserEmail">ფასი 30-31</label><input name="ocdaati" type="text" value="'.$ProductRow3['30-31'].'" id="UserEmail"/></div>
								 
								 <div class="input text"><label for="UserEmail">სერვისების მაქს ფასი</label><input name="smp" type="text" value="'.$ProductRow3['serv_max_price'].'" id="UserEmail"/></div>


								 <div class="input text"><img src="'.$ProductRow3[pic1].'" height="240" width="120"></div>
								 <div class="input text"><label for="UserDateOfBirth">სურ 1(1920X1080)(<a href="?edit='.$id.'&d=1" style="color:red;">წაშლა</a>)</label><input name="f1" type="file" accept="image/*" id="UserUsername"/></div>

								 <div class="input text"><label for="UserEmail">სურათის აღწერა 1</label><input name="alt1" type="text" value="'.$ProductRow3[alt1].'" id="UserEmail"/></div>
								 <div class="input text"><img src="'.$ProductRow3[pic2].'" height="240" width="120"></div>
								 <div class="input text"><label for="UserDateOfBirth">სურ 2(1920X1080)(<a href="?edit='.$id.'&d=2" style="color:red;">წაშლა</a>)</label><input name="f2" type="file" accept="image/*" id="UserUsername"/></div>

								 <div class="input text"><label for="UserEmail">სურათის აღწერა 2</label><input name="alt2" type="text" value="'.$ProductRow3[alt2].'" id="UserEmail"/></div>
								 <div class="input text"><img src="'.$ProductRow3[pic3].'" height="240" width="120"></div>
								 <div class="input text"><label for="UserDateOfBirth">სურ 3(1920X1080)(<a href="?edit='.$id.'&d=3" style="color:red;">წაშლა</a>)</label><input name="f3" type="file" accept="image/*" id="UserUsername"/></div>

								 <div class="input text"><label for="UserEmail">სურათის აღწერა 3</label><input name="alt3" type="text" value="'.$ProductRow3[alt3].'" id="UserEmail"/></div>
								 <div class="input text"><img src="'.$ProductRow3[pic4].'" height="240" width="120"></div>
								 <div class="input text"><label for="UserDateOfBirth">სურ 4(1920X1080)(<a href="?edit='.$id.'&d=4" style="color:red;">წაშლა</a>)</label><input name="f4" type="file" accept="image/*" id="UserUsername"/></div>

								 <div class="input text"><label for="UserEmail">სურათის აღწერა 4</label><input name="alt4" type="text" value="'.$ProductRow3[alt4].'" id="UserEmail"/></div>
								 <div class="input text"><img src="'.$ProductRow3[pic5].'" height="240" width="120"></div>
								 <div class="input text"><label for="UserDateOfBirth">სურ 5(1920X1080(<a href="?edit='.$id.'&d=5" style="color:red;">წაშლა</a>))</label><input name="f5" type="file" accept="image/*" id="UserUsername"/></div>

								 <div class="input text"><label for="UserEmail">სურათის აღწერა 5</label><input name="alt5" type="text" value="'.$ProductRow3[alt5].'" id="UserEmail"/></div>
<div class="submit"><input  class="btn" style="margin-top: 15px;" type="submit" value="Submit"/></div></form>';
                                 echo '                                 </div>
								</div></div></div></div></div></div>';



									$name = mysqli_real_escape_string($GLOBALS['db'],$_POST['name']);
									$loc = $_POST[loc];
									$mark = $_POST['mark'];
									$model = $_POST['model'];
									$year = $_POST['year'];
									$engine = $_POST['engine'];
									$colours = $_POST['colours'];
									$trans = $_POST['transm'];
									$fuel = $_POST['fuel'];
									$class = $_POST['class'];
									$pop = $_POST['pop'];
									$erti = $_POST['erti'];
									$sami = $_POST['sami'];
									$xuti = $_POST['xuti'];
									$rva = $_POST['rva'];
									$tertmeti = $_POST['tertmeti'];
									$teqvsmeti = $_POST['teqvsmeti'];
									$ocdaori = $_POST['ocdaori'];
									$ocdaeqvsi = $_POST['ocdaeqvsi'];
									$ocdarva = $_POST['ocdarva'];
									$ocdaati = $_POST['ocdaati'];

									$ldescr = mysqli_real_escape_string($GLOBALS['db'],$_POST['ldescr']);
									$ldescrrus = mysqli_real_escape_string($GLOBALS['db'],$_POST['ldescrrus']);
									$ldescrgeo = mysqli_real_escape_string($GLOBALS['db'],$_POST['ldescrgeo']);

									$filep = $_FILES['m_pic']['tmp_name'];
									$filep1 = $_FILES['f1']['tmp_name'];
									$filep2 = $_FILES['f2']['tmp_name'];
									$filep3 = $_FILES['f3']['tmp_name'];
									$filep4 = $_FILES['f4']['tmp_name'];
									$filep5 = $_FILES['f5']['tmp_name'];
									if(empty($name) or empty($loc) or empty($mark) or empty($model) or empty($year) or empty($engine) or empty($colours) or empty($erti) or empty($sami) or empty($xuti) or empty($rva) or empty($tertmeti) or empty($teqvsmeti) or empty($ocdaori) or empty($ocdaeqvsi) or empty($ocdarva) or empty($ocdaati))
									{
										echo '<h2 style="color:red;"><b>თქვენ გამოტოვეთ საჭირო ველები!!!</b></h2>';
									}
									else
									{
											$UpdateProduct = mysqli_query($GLOBALS['db'],"UPDATE catalog SET name='$name', location='$loc', transmision='$trans', fuel='$fuel', class='$class', driver='$pop', mark='$mark', model='$model', year='$year', engine='$engine', colours='$colours', price='$erti', `3-4`='$sami', `5-7`='$xuti', `8-10`='$rva', `11-15`='$tertmeti', `16-21`='$teqvsmeti', `22-25`='$ocdaori', `26-27`='$ocdaeqvsi', `28-29`='$ocdarva', `30-31`='$ocdaati', sto='$_POST[sto]', capacity='$_POST[capacity]', doors='$_POST[doors]', seats='$_POST[seats]', air='$_POST[air]', wheel='$_POST[wheel]', wheel_rus='$_POST[wheel_rus]', wheel_eng='$_POST[wheel_eng]', id=$_POST[id1], alt_main='$_POST[alt_main]', alt1='$_POST[alt1]', alt2='$_POST[alt2]', alt3='$_POST[alt3]', alt4='$_POST[alt4]', alt5='$_POST[alt5]', description_eng='$_POST[desceng]', description_geo='$_POST[descgeo]', description_rus='$_POST[descrus]',wd_rus='$_POST[wd_rus]',wd_geo='$_POST[wd_geo]',wd_eng='$_POST[wd_eng]',title_geo='$_POST[title_geo]',title_eng='$_POST[title_eng]',title_rus='$_POST[title_rus]',desc_geo='$_POST[d_geo]',desc_rus='$_POST[d_rus]',desc_eng='$_POST[d_eng]', pick_tbs='$_POST[pick_tbs]', pick_tbs_air='$_POST[pick_tbs_air]', pick_btm='$_POST[pick_btm]', pick_btm_air='$_POST[pick_btm_air]', pick_kut='$_POST[pick_kut]', pick_kut_air='$_POST[pick_kut_air]', drop_tbs='$_POST[drop_tbs]', drop_tbs_air='$_POST[drop_tbs_air]', drop_btm='$_POST[drop_btm]', drop_btm_air='$_POST[drop_btm_air]', drop_kut='$_POST[drop_kut]', drop_kut_air='$_POST[drop_kut_air]', serv_max_price='$_POST[smp]' WHERE id='".$id."'") or die(mysqli_error($GLOBALS['db']));
											if($UpdateProduct == true)
											{
												echo '<h2 style="color:green;"><b>მანქანა '.$ProductRow3[name].' შესწორებულია!!!</b></h2>';
											}
											else
											{
												echo 'შეცდომაა';
											}
											$ftp_server = '107.180.41.168';
															$ftp_user_name = 'auto4rental@rentcartbilisi.com';
															$ftp_user_pass = 'C4d3e64c';
											$name23 = uniqid();

											$name1 = uniqid();
											$name2 = uniqid();
											$name3 = uniqid();
											$name4 = uniqid();
											$name5 = uniqid();

											$name23 = $name23.'.jpg';

											$name1 = $name1.'.jpg';
											$name2 = $name2.'.jpg';
											$name3 = $name3.'.jpg';
											$name4 = $name4.'.jpg';
											$name5 = $name5.'.jpg';
											$conn_id = ftp_connect($ftp_server);
											$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
											// загружаем файл



											if(!empty($filep))
											{
												$upload = ftp_put($conn_id, 'www/img/'.$name23, $filep, FTP_BINARY);
												$UpdateProduct = mysqli_query($GLOBALS['db'],"UPDATE catalog SET main_pic='http://rentcartbilisi.com/img/$name23' WHERE id='".$id."'") or die(mysqli_error($GLOBALS['db'])());
											}
											if(!empty($filep1))
											{
												$upload1 = ftp_put($conn_id, 'www/img/'.$name1, $filep1, FTP_BINARY);
												$UpdateProduct = mysqli_query($GLOBALS['db'],"UPDATE catalog SET pic1='http://rentcartbilisi.com/img/$name1' WHERE id='".$id."'") or die(mysqli_error($GLOBALS['db'])());
											}
											if(!empty($filep2))
											{
												$upload1 = ftp_put($conn_id, 'www/img/'.$name2, $filep2, FTP_BINARY);
												$UpdateProduct = mysqli_query($GLOBALS['db'],"UPDATE catalog SET pic2='http://rentcartbilisi.com/img/$name2' WHERE id='".$id."'") or die(mysqli_error($GLOBALS['db'])());
											}
											if(!empty($filep3))
											{
												$upload1 = ftp_put($conn_id, 'www/img/'.$name3, $filep3, FTP_BINARY);
												$UpdateProduct = mysqli_query($GLOBALS['db'],"UPDATE catalog SET pic3='http://rentcartbilisi.com/img/$name3' WHERE id='".$id."'") or die(mysqli_error($GLOBALS['db'])());
											}
											if(!empty($filep4))
											{
												$upload1 = ftp_put($conn_id, 'www/img/'.$name4, $filep4, FTP_BINARY);
												$UpdateProduct = mysqli_query($GLOBALS['db'],"UPDATE catalog SET pic4='http://rentcartbilisi.com/img/$name4' WHERE id='".$id."'") or die(mysqli_error($GLOBALS['db'])());
											}
											if(!empty($filep5))
											{
												$upload1 = ftp_put($conn_id, 'www/img/'.$name5, $filep5, FTP_BINARY);
												$UpdateProduct = mysqli_query($GLOBALS['db'],"UPDATE catalog SET pic5='http://rentcartbilisi.com/img/$name5' WHERE id='".$id."'") or die(mysqli_error($GLOBALS['db'])());
											}

									}

}
if(isset($_GET['del']) and !empty($_GET['del']) and !isset($_GET[comment_id]))
{
$id = mysqli_real_escape_string($GLOBALS['db'],$_GET['del']);
$Product2 = mysqli_query($GLOBALS['db'],"SELECT * FROM catalog WHERE id='".$id."'");
$ProductRow2 = mysqli_fetch_array($Product2);
echo '<div class="row-fluid ">
            <div class="span12">
                <!-- BEGIN INLINE TABS PORTLET-->
                <div class="widget">
				<div class="widget-body">
                        <div class="row-fluid">
                            <div class="span12">
                                <!--BEGIN TABS-->
                                <div class="table table-custom">
								 <h3>მანქანის <b>'.$ProductRow2[name].' წაშლა</b></h3>
								 <br />
                                    <div class="tab-content">
                                        Are you sure?<br> <a href="jewelry.php?del='.$_GET[del].'&yes">Yes, of course</a><br><a href="jewelry.php" style="color:red;">No</a>                                    </div>
                                </div>
								</div></div></div></div></div></div>';
								if(isset($_GET[yes]))
								{
									$DelProduct = mysqli_query($GLOBALS['db'],"DELETE FROM catalog WHERE id='".$id."'");
									if($DelProduct == true)
									{
										echo '<h2 style="color:green;"><b>მანქანა '.$ProductRow2[name].' წაშლილია</b></h2>';
									}
								}

}

if(!isset($_GET['edit']) and !isset($_GET['del']) and !isset($_GET['add']))
{
echo '<div class="row-fluid ">
            <div class="span12">
                <!-- BEGIN INLINE TABS PORTLET-->
                <div class="widget">
				<div class="widget-body">
                        <div class="row-fluid">
                            <div class="span12">
                                <!--BEGIN TABS-->
                                <div class="table table-custom">
								 <ul class="nav nav-tabs">
                                    <li class="active">
                    <a href="#">სია</a>                </li>
                            <li >
                    <a href="jewelry.php?add">დამატება</a>                </li>
                        </ul>
								 <br />
								 <div class="tab-content">

									<div class="row-fluid">
        <div class="span12">
            <!-- BEGIN TABLE widget-->
            <div class="widget">
                <div class="widget-title">
                    <h4>
                        <i class="icon-reorder"></i>
                                                    Data list                                            </h4>

                </div>
                <div class="widget-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                                                                <th>
                                        <a href="/users.php/index/sort:id/direction:asc" class="desc">Id</a>                                    </th>
                                                                    <th>
                                        <a href="/users.php/index/sort:username/direction:asc">Image</a>                                    </th>
										<th>
                                        <a href="/users.php/index/sort:username/direction:asc">მწარმოებელი</a>                                    </th>
										<th>
                                        <a href="/users.php/index/sort:username/direction:asc">მოდელი</a>                                    </th>
                                                                    <th>
                                        <a href="/users.php/index/sort:email/direction:asc">Name</a>                                    </th>
                                                                    <th>
                                        <a href="/users.php/index/sort:registration_date/direction:asc">ფასი(1 დღის) ლარი</a>                                    </th>

										<th>
                                        <a href="/users.php/index/sort:last_visit/direction:asc">კლასი</a>                                    </th>
										<th>
                                        <a href="/users.php/index/sort:last_visit/direction:asc">პოპ. მანქანებში?</a>                                    </th>





                                                            <th>
                                    Actions                                </th>

                                                    </tr>
                        </thead>
                        <tbody>';


					   $Product = mysqli_query($GLOBALS['db'],"SELECT * FROM catalog ORDER BY id DESC") or die(mysqli_error($GLOBALS['db'])());
					   $ProductRow = mysqli_fetch_array($Product);
					   do
					   {
						   echo "<tr><td class=''>".$ProductRow[id]."</td><td class=''><img src='".$ProductRow[main_pic]."' width='90' height='90'></td><td class=''>".$ProductRow[mark]."</td><td class=''>".$ProductRow[model]."</td><td class=''>".$ProductRow[name]."</td>";


						  echo "<td class=''>".$ProductRow[price]."</td><td class=''>";
						  if($ProductRow['class']=="SUV")
						  {
							  echo 'ჯიპი';
						  }
						  else{
							  echo $ProductRow['class'];
						  }
						  echo "</td><td class=''>";

						  if($ProductRow[num] == 1)
						  {
							  echo 'კი';
						  }
						  if($ProductRow[num] == 0)
						  {
							  echo 'არა';
						  }

						  echo"</td>   <td style='min-width: 190px;' class='actions'>
	<a href='?edit=$ProductRow[id]' class='btn btn-mini btn-primary'>Edit</a> <a href='?del=$ProductRow[id]' class='btn btn-mini btn-success'>Delete</a> <a href='?comment_id=$ProductRow[id]' class='btn btn-mini btn-success'>Comments</a>  </td>                       </tr>";
					   }
					   while($ProductRow = mysqli_fetch_array($Product));




                                               echo ' <tr>
                                                            <td>
                                                           </td>
                                                            <td>
                                                           </td>
                                                            <td>
                                                           </td>
                                                            <td>
                                                           </td>
                                                            <td>
                                                           </td>
                                                                                    <td></td>
                                                    </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END TABLE widget-->
        </div>
    </div>


                                                                  </div>
								</div></div></div></div></div></div>';
}
		?>
    </div>
    <!-- END PAGE CONTENT-->
</div>			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="Admin/assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
	<script src="Admin/assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="Admin/assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
	<script src="Admin/assets/bootstrap/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="Admin/js/jquery.blockui.js"></script>

	<script type="text/javascript" src="Admin/js/jquery.cookie.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>

	<script type="text/javascript" src="https://demo.betscheme.com/theme/Admin/js/excanvas.js"></script>

	<script type="text/javascript" src="https://demo.betscheme.com/theme/Admin/js/respond.js"></script>
	<![endif]-->


    <!-- ie8 fixes -->
    <!--[if lt IE 9]>




    <![endif]-->
    <script src="Admin/assets/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
    <script type="text/javascript" src="Admin/assets/jquery-knob/js/jquery.knob.js"></script>
	<script type="text/javascript" src="Admin/js/jquery.peity.min.js"></script>
    <script type="text/javascript" src="Admin/assets/uniform/jquery.uniform.min.js" type="text/javascript"></script>

	<script type="text/javascript" src="Admin/js/scripts.js"></script>

    <script type="text/javascript" src="Admin/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="Admin/assets/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="Admin/assets/clockface/js/clockface.js"></script>
    <script type="text/javascript" src="Admin/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="Admin/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>

    <script type="text/javascript" src="Admin/assets/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="Admin/assets/fancybox/source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="Admin/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <script type="text/javascript" src="Admin/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

    <!--[if lte IE 8]>
    <script language="javascript" type="text/javascript" src="https://demo.betscheme.com/theme/Admin/assets/flot/excanvas.min.js"></script>
    <![endif]-->

    <script type="text/javascript" src="Admin/assets/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="Admin/assets/flot/jquery.flot.pie.js"></script>
<!--Begin Comm100 Live Chat Code-->
<script>
		jQuery(document).ready(function() {
			// initiate layout and plugins
			App.setMainPage(true);
			App.init();
		});
	</script>
<?
if(isset($_GET['exit']))
{
	session_destroy();
	echo '<meta http-equiv="refresh" content="0; url=http://geosky.ge/admin" />';
}
?>
</body>
<!-- END BODY -->
</html>
