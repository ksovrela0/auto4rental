
            <!-- end .b-title-page-->
            <div class="bg-grey">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="index.php"><i class="icon fa fa-home"></i></a>
                                </li>
                                <li class="active">Hotels&Tours</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end breadcrumb-->
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-md-push-3">
                        <main class="l-main-content" style="padding-top:120px;">
                            <div class="filter-goods">
                                <div class="filter-goods__select"><span class="hidden-xs">SORT</span>
                                    
                                    <div class="btns-switch"><i class="btns-switch__item js-view-th active icon fa fa-th-large"></i><i class="btns-switch__item js-view-list icon fa fa-th-list"></i>
                                    </div>
                                </div>
                            </div>
                            <!-- end .filter-goods-->
                            <div id="result" class="goods-group-2 list-goods list-goods_th">
							<input type="hidden" id="page" value="car">
								
                                <!-- end .b-goods-1-->
                                
                                <!-- end .b-goods-1-->
                            </div>
                            <!-- end .goods-group-2-->
							
                        </main>
                        <!-- end .l-main-content-->
                    </div>
					<?
					if(!isset($_GET[search]))
					{
						echo '<div class="col-md-3 col-md-pull-9">
							<aside class="l-sidebar">
								<form class="b-filter-2 bg-grey" action="car.php" method="GET">
									<h3 class="b-filter-2__title">Search</h3>
									<div class="b-filter-2__inner">
									<div class="b-filter-2__group">
											<div class="b-filter-2__group-title">Class</div>
											<div class="b-filter-type-2">
												<div class="b-filter-type-2__item">
													<input class="b-filter-type-2__input" name="a1" id="typeHotel" onclick=\'sendQuery()\' type="checkbox" />
													<label class="b-filter-type-2__label" for="typeHotel"><i class="b-filter-type-2__icon flaticon-bed"></i><span class="b-filter-type-2__title">Hotel</span>
													</label>
												</div>
												<div class="b-filter-type-2__item">
													<input class="b-filter-type-2__input" name="a2" id="typeTour" onclick=\'sendQuery()\' type="checkbox" />
													<label class="b-filter-type-2__label" for="typeTour"><i class="b-filter-type-2__icon flaticon-trajectory"></i><span class="b-filter-type-2__title">Tour</span>
													</label>
												</div>
											</div>
										</div>
										
										<div class="b-filter-2__group">
											<div class="b-filter-2__group-title">Direction</div>
											<div class="styled-select slate">
											<select id="compas" class="" data-width="100%" onchange=\'GetCity(), sendQuery()\' >
											<option>Choose</option>
											<option value="0">Tbilisi</option>
											<option value="1">West</option>
											<option value="2">East</option>
											</select>
											</div>
										</div>
										<div class="b-filter-2__group">
											<div class="b-filter-2__group-title">Place</div>
											<div class="styled-select slate">
											<select id="mark" name="mark" class="" data-width="100%" onchange=\'sendQuery()\'>
											<option>Choose</option>
											
											';

											echo '</select></div>
										</div>
										
										
										
									</div>
								</form>
								<!-- end .b-filter-->
							</aside>
							<!-- end .l-sidebar-->
						</div>';
					}
					?>
                </div>
            </div>