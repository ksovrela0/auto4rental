
            <!-- end .b-title-page-->
            <div class="bg-grey">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="icon fa fa-home"></i></a>
                                </li>
                                <li class="active">Список</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end breadcrumb-->
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-md-push-3">
                        <main class="l-main-content" style="padding-top:120px;">
                            <div class="filter-goods">

                              <div class="styled-select slate">
        											<select id="orders" onchange='filt()' data-width="100%">
														<option value="in">Цена Min</option>
        												<option value="1">Сортировка</option>
        												<option value="ax">Цена Max</option>
        												
        											</select>
        											</div>

                            </div>
                            <!-- end .filter-goods-->
                            <div id="result" class="goods-group-2 list-goods">
							<input type="hidden" id="page" value="car">

                                <!-- end .b-goods-1-->

                                <!-- end .b-goods-1-->
                            </div>
                            <!-- end .goods-group-2-->

                        </main>
                        <!-- end .l-main-content-->
                    </div>
					<?
					if(!isset($_GET[search]))
					{
						$getOptions = mysqli_query($GLOBALS['db'],"SELECT DISTINCT mark FROM catalog ORDER BY mark ASC");
						$getOptionsRow = mysqli_fetch_array($getOptions);
            echo '<div class="col-md-3 col-md-pull-9">
							<aside class="l-sidebar">
								<form class="b-filter-2 bg-grey" action="/ru/car/" method="POST">
									<h3 class="b-filter-2__title">Фильтер</h3>
									<div class="b-filter-2__inner">
									<div class="b-filter-2__group">
									<div class="b-filter-2__group-title">Дата Начала</div>
									<div class="row">
										<div class="col-md-7 col-xs-7">
											<input class="form-control" autocomplete="off" type="text" id="datetimepicker2" name="date1" placeholder="Pick day" value="';
												if(isset($_SESSION[pick]))
												{
													echo $_SESSION[pick];
												}
												else
												{
													echo date('Y/m/d');
												}
												echo '" required="required" />
										</div>
										<div class="col-md-5 col-xs-5">
											<select id="time1" name="time1" style="font-size:20px;padding-left:18px;width:100%;height:48px; margin-top:10px;" data-width="100%">';
															
																if(isset($_SESSION['time1']))
    															{
    																echo '<option value="'.$_SESSION['time1'].'">'.$_SESSION['time1'].'</option>';
    																for($k=0;$k<24;$k++)
    																{
    																	if($k < 9)
    																	{
    																		echo '<option value="0'.$k.':00">0'.$k.':00</option>';
    																	}
    																	if($k == 9)
    																	{
    																		echo '<option value="0'.$k.':00">0'.$k.':00</option>';
    																	}
    																	if($k > 9)
    																	{
    																		echo '<option value="'.$k.':00">'.$k.':00</option>';
    																	}
    																}
    															}
    															else
    															{
    																for($k=0;$k<24;$k++)
    																{
    																	if($k < 9)
    																	{
    																		echo '<option value="0'.$k.':00">0'.$k.':00</option>';
    																	}
    																	if($k == 9)
    																	{
    																		echo '<option value="0'.$k.':00">0'.$k.':00</option>';
    																	}
    																	if($k > 9)
    																	{
    																		echo '<option value="'.$k.':00">'.$k.':00</option>';
    																	}
    																}
    															}
															
														echo '</select>
										</div>
										</div>
										</div>
										<div class="b-filter-2__group">
										<div class="b-filter-2__group-title">Дата Конца</div>
										<div class="row">
										<div class="col-md-7 col-xs-7">
											<input class="form-control" autocomplete="off" type="text" id="datetimepicker3" name="date2" value="';
											if(isset($_SESSION['end']))
											{
												echo $_SESSION['end'];
											}
											else
											{
												
												echo date('Y/m/d');
												
											}
											echo '" placeholder="End day" required="required" />
										</div>
										<div class="col-md-5 col-xs-5">
											<select id="time2" name="time2" style="font-size:20px;padding-left:18px;width:100%;height:48px; margin-top:10px;" data-width="100%">';
															
															
															if(isset($_SESSION['time2']))
    															{
    																echo '<option value="'.$_SESSION['time2'].'">'.$_SESSION['time2'].'</option>';
    																for($k=0;$k<24;$k++)
    																{
    																	if($k < 9)
    																	{
    																		echo '<option value="0'.$k.':00">0'.$k.':00</option>';
    																	}
    																	if($k == 9)
    																	{
    																		echo '<option value="0'.$k.':00">0'.$k.':00</option>';
    																	}
    																	if($k > 9)
    																	{
    																		echo '<option value="'.$k.':00">'.$k.':00</option>';
    																	}
    																}
    															}
    															else
    															{
    																for($k=0;$k<24;$k++)
    																{
    																	if($k < 9)
    																	{
    																		echo '<option value="0'.$k.':00">0'.$k.':00</option>';
    																	}
    																	if($k == 9)
    																	{
    																		echo '<option value="0'.$k.':00">0'.$k.':00</option>';
    																	}
    																	if($k > 9)
    																	{
    																		echo '<option value="'.$k.':00">'.$k.':00</option>';
    																	}
    																}
    															}
														echo '</select>
										</div>
										</div>	
											
										</div>
										<div class="b-filter-2__group">
											<div class="b-filter-2__group-title">Получение</div>
											<div class="styled-select slate">
											<select id="pick" name="place" class="" data-width="100%" >
											';
												if(isset($_SESSION['place']))
												{
													$GetPlaces = mysqli_query($GLOBALS['db'],"SELECT * FROM places WHERE id='$_SESSION[place]' LIMIT 1");
													$GetPlacesR = mysqli_fetch_array($GetPlaces);
													echo '<option value="'.$GetPlacesR[id].'">'.$GetPlacesR[name_rus].' ('.$GetPlacesR[price].')</option>';
													$GetPlaces = mysqli_query($GLOBALS['db'],"SELECT * FROM places WHERE id!='$_SESSION[place]' ORDER BY id ASC");
													$GetPlacesR = mysqli_fetch_array($GetPlaces);
													do
													{
														
														echo '<option value="'.$GetPlacesR[id].'">'.$GetPlacesR[name_rus].' ('.$GetPlacesR[price].')</option>';
													}
													while($GetPlacesR = mysqli_fetch_array($GetPlaces));
												}
												else
												{
													$GetPlaces = mysqli_query($GLOBALS['db'],"SELECT * FROM places ORDER BY id ASC");
													$GetPlacesR = mysqli_fetch_array($GetPlaces);
													do
													{
														
														echo '<option value="'.$GetPlacesR[id].'">'.$GetPlacesR[name_rus].' ('.$GetPlacesR[price].')</option>';
													}
													while($GetPlacesR = mysqli_fetch_array($GetPlaces));
												}
												
											
											echo '
											</select>
											</div>
										</div>
										
										<div class="b-filter-2__group">
											<div class="b-filter-2__group-title">Возврат</div>
											<div class="styled-select slate">
											<select id="drop" name="drop" class="" data-width="100%" >
											';
											
												if(isset($_SESSION['drop']))
												{
													$GetPlaces = mysqli_query($GLOBALS['db'],"SELECT * FROM dropoff WHERE id='$_SESSION[drop]' LIMIT 1");
													$GetPlacesR = mysqli_fetch_array($GetPlaces);
													echo '<option value="'.$GetPlacesR[id].'">'.$GetPlacesR[name_rus].' ('.$GetPlacesR[price].')</option>';
													$GetPlaces = mysqli_query($GLOBALS['db'],"SELECT * FROM dropoff WHERE id!='$_SESSION[drop]' ORDER BY id ASC");
													$GetPlacesR = mysqli_fetch_array($GetPlaces);
													do
													{
														
														echo '<option value="'.$GetPlacesR[id].'">'.$GetPlacesR[name_rus].' ('.$GetPlacesR[price].')</option>';
													}
													while($GetPlacesR = mysqli_fetch_array($GetPlaces));
												}
												else
												{
													$GetPlaces = mysqli_query($GLOBALS['db'],"SELECT * FROM dropoff ORDER BY id ASC");
													$GetPlacesR = mysqli_fetch_array($GetPlaces);
													do
													{
														echo '<option value="'.$GetPlacesR[id].'">'.$GetPlacesR[name_rus].'('.$GetPlacesR[price].')</option>';
													}
													while($GetPlacesR = mysqli_fetch_array($GetPlaces));
												}
												
											
											
											echo '
											</select>
											</div><br>
											<input type="submit" class="btn btn-dark" value="Поиск">
										</div>
										</form>
									
									<div class="b-filter-2__group">
											<div class="b-filter-2__group-title">Class</div>
											<div class="b-filter-type-2">
												<div class="b-filter-type-2__item" style="width:100%;">
													<input class="b-filter-type-2__input" name="a2" id="typeSuv" onclick=\'Getadd()\' type="checkbox" />
													<label class="b-filter-type-2__label" for="typeSuv"><i class="b-filter-type-2__icon flaticon-car-of-hatchback-model"></i><span class="b-filter-type-2__title">SUV</span>
													</label>
												</div>
												<div class="b-filter-type-2__item" style="width:100%;">
													<input class="b-filter-type-2__input" name="a5" id="typeSedan" onclick=\'Getadd()\' type="checkbox" />
													<label class="b-filter-type-2__label" for="typeSedan"><i class="b-filter-type-2__icon flaticon-sedan-car-model"></i><span class="b-filter-type-2__title">Sedan</span>
													</label>
												</div>


											</div>
										</div>
										<div class="b-filter-2__group">
                                        <div class="b-filter-2__group-title">Services</div>';
                                        $getServices = mysqli_query($GLOBALS['db'],"SELECT * FROM services");
										$getServicesR = mysqli_fetch_array($getServices);
										$c=1;
										do
										{
											echo '
											
											<div class="col-md-12 col-xs-12">
												<input onchange=\'Getadd();\' class="forms__check hidden" id="chec'.$getServicesR[id].'" type="checkbox"" />
												<label style="font-size:11px; margin-bottom:15px;" class="forms__label forms__label-check forms__label-check-1" for="chec'.$getServicesR[id].'">'.$getServicesR[name_rus].'</label>
											</div>
											
											';
											$c++;
										}
										while($getServicesR = mysqli_fetch_array($getServices));
                                    echo '</div>
										<div class="b-filter-2__group">
											<div class="b-filter-2__group-title">Производитель</div>
											<div class="styled-select slate">
											<select id="mark" name="mark" class="" data-width="100%" onchange=\'GetModels(), Getadd()\'>
											<option value="1">Choose</option>

											';

                      do
                      {
                        if($getOptionsRow[mark] != 'service')
                        {
                          echo '<option value="'.$getOptionsRow[mark].'">'.$getOptionsRow[mark].'</option>';
                        }

                      }
                      while($getOptionsRow = mysqli_fetch_array($getOptions));



											echo '</select></div>
										</div>

										<div class="b-filter-2__group">
											<div class="b-filter-2__group-title">Модель</div>
											<div class="styled-select slate">
											<select id="model" class="" data-width="100%" onchange=\'Getadd()\' >
											<option>Choose</option>
											</select>
											</div>
										</div>

										
										<div class="b-filter-2__group">
											<div class="b-filter-2__group-title">Тип топлива</div>
											<div class="styled-select slate">
											<select id="fuel" onchange=\'Getadd()\' data-width="100%">
												<option>Choose</option>
												<option value="1">бензин</option>
												<option value="2">дизель</option>
												<option value="3">гибрид</option>
												<option value="4">электро</option>
											</select>
											</div>
										</div>
										<div class="b-filter-2__group">
											<div class="b-filter-2__group-title">Трансмиссия</div>
											<div class="styled-select slate">
											<select id="transm" onchange=\'Getadd()\' data-width="100%">
												<option>Choose</option>
												<option value="1">Автоматика</option>
												<option value="2">Механика</option>
												<option value="3">Типтроник</option>
											</select>
											</div>
										</div>


									</div>
								
								<!-- end .b-filter-->
							</aside>
							<!-- end .l-sidebar-->
						</div>';
					}
					?>
                </div>
            </div>
