<?
include("db.php");
if(isset($_GET[id]) and $_GET[id] != 0)
{
	$id = mysqli_real_escape_string($GLOBALS['db'],trim($_GET[id]));
	$getNews = mysqli_query($GLOBALS['db'],"SELECT * FROM news WHERE id='$id'");
	$getNewsC = mysqli_num_rows($getNews);
	if($getNewsC == 0)
	{
		echo '
		
		<div class="section-title-page area-bg area-bg_dark area-bg_op_70">
                <div class="area-bg__inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <h1 class="b-title-page bg-primary_a">NONE</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end .b-title-page-->
            <div class="bg-grey">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="icon fa fa-home"></i></a>
                                </li>
                                <li><a href="">NONE</a>
                                </li>
                                <li class="active">NONE</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end breadcrumb-->
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <main class="l-main-content">
                            <article class="b-post b-post-full clearfix">
                                <div class="entry-media">
                                    <a class="js-zoom-images" href="NONE">
                                        <img class="img-responsive" src="NONE" alt="Foto" />
                                    </a>
                                </div>
                                <div class="entry-main">
                                    <div class="entry-meta">
                                        <div class="entry-meta__group-left"><span class="entry-meta__item">Post by<a class="entry-meta__link" href="">NONE</a></span><span class="entry-meta__item">On<a class="entry-meta__link" href=""> NONE</a></span>
                                            <span
                                            class="entry-meta__categorie bg-primary">Ford News</span>
                                        </div>
                                        <div class="entry-meta__group-right"><span class="entry-meta__item"><i class="icon fa fa-heart"></i><a class="entry-meta__link" href=""> </a></span><span class="entry-meta__item"><i class="icon fa fa-comment-o"></i>Comments<a class="entry-meta__link" href=""> </a></span>
                                        </div>
                                    </div>
                                    <div class="entry-header">
                                        <h2 class="entry-title"><a href="">NONE</a></h2>
                                    </div>
                                    <div class="entry-content">
                                        NONE
                                        <div class="b-blockquote b-blockquote-2">
                                            <blockquote>
                                                NONE
                                                <cite class="b-blockquote__cite" title="Blockquote Title"><span class="b-blockquote__author">James Liam</span></cite>
                                            </blockquote>
                                        </div>
                                        <!-- end .b-blockquote-->
                                        <h3 class="entry-title-inner">NONE</h3>
                                        <p>NONE</p>
                                        <div class="b-post__section">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <img class="img-post img-responsive" src="NONE" alt="foto" />
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="b-post__group-1"><strong>NONE</strong>
                                                        <ul class="list list-mark-5 list_mark-prim">
                                                            <li>NONE</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <p>NONE</p>
                                        <p>NONE</p>
                                    </div>
                                </div>
                                <div class="entry-footer">
                                    <div class="entry-footer__group"><i class="icon fa fa-tags text-primary"></i><span class="entry-footer__title"></span><a class="entry-footer__tags" href=""></a><a class="entry-footer__tags" href=""></a>
                                        <a
                                        class="entry-footer__tags" href="">autos</a><a class="entry-footer__tags" href="">NONE</a>
                                    </div>
                                    <div class="entry-footer__group"><i class="icon fa fa-share-alt text-primary"></i><a class="entry-footer__link" href="">NONE</a>
                                    </div>
                                </div>
                            </article>
                            <!-- end .post-->
                           
                            <!-- end .about-author-->
                            <div class="post-carousel owl-carousel owl-theme enable-owl-carousel" data-min768="2" data-min992="2" data-min1200="2" data-margin="30" data-pagination="false" data-navigation="true" data-auto-play="4000" data-stop-on-hover="true">
                                <div class="b-post-nav__item"><span class="b-post-nav__img"><img class="img-responsive" src="assets/media/content/posts/360x180/1.jpg" alt="foto"/></span><span class="b-post-nav__inner"><span class="b-post-nav__title">2017 Mazda 6 quick take:<br>The enthusiast\'s midsize sedan</span>
                                    <a
                                    class="b-post-nav__btn btn btn-primary btn-xs" href="">Mazda Car</a>
                                        </span>
                                </div>
                                <div class="b-post-nav__item"><span class="b-post-nav__img"><img class="img-responsive" src="assets/media/content/posts/360x180/2.jpg" alt="foto"/></span><span class="b-post-nav__inner"><span class="b-post-nav__title">Meet the new Rolls-Royce Phantom: Built-in art gallery</span>
                                    <a
                                    class="b-post-nav__btn btn btn-primary btn-xs" href="">Rolls Royce</a>
                                        </span>
                                </div>
                                <div class="b-post-nav__item"><span class="b-post-nav__img"><img class="img-responsive" src="assets/media/content/posts/360x180/1.jpg" alt="foto"/></span><span class="b-post-nav__inner"><span class="b-post-nav__title">2017 Mazda 6 quick take:<br>The enthusiast\'s midsize sedan</span>
                                    <a
                                    class="b-post-nav__btn btn btn-primary btn-xs" href="">Mazda Car</a>
                                        </span>
                                </div>
                                <div class="b-post-nav__item"><span class="b-post-nav__img"><img class="img-responsive" src="assets/media/content/posts/360x180/2.jpg" alt="foto"/></span><span class="b-post-nav__inner"><span class="b-post-nav__title">Meet the new Rolls-Royce Phantom: Built-in art gallery</span>
                                    <a
                                    class="b-post-nav__btn btn btn-primary btn-xs" href="">Rolls Royce</a>
                                        </span>
                                </div>
                            </div>
                            
                            <!-- end .section-comment-->
                            
                            <!-- end .section-reply-form-->
                        </main>
                        <!-- end .l-main-content-->
                    </div>
		
		';
	}
	else
	{
		$getNewsR = mysqli_fetch_array($getNews);
		echo '
		
		
            <!-- end .b-title-page-->
            <div class="bg-grey">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="icon fa fa-home"></i></a>
                                </li>
                                <li><a href="/news">Новости</a>
                                </li>
                                <li class="active">'.$getNewsR[name_rus].'</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end breadcrumb-->
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <main class="l-main-content" style="padding-top:120px;">
                            <article class="b-post b-post-full clearfix">
                                <div class="entry-media">
                                    ';
									if(empty($getNewsR[video]))
												{
													echo '<a class="js-zoom-images" href="'.$getNewsR[main_pic].'">
                                        <img class="img-responsive" src="'.$getNewsR[main_pic].'" alt="Foto" />
                                    </a>';
												}
												else
												{
													echo '<iframe id="ytplayer" type="text/html" width="100%" height="640" src="'.$getNewsR[video].'" frameborder="0"/></iframe>';
												}
                                echo '</div>
                                <div class="entry-main">
                                    <div class="entry-meta">
                                        <div class="entry-meta__group-left"><span class="entry-meta__item"><a class="entry-meta__link" href=""></a></span><span class="entry-meta__item">On<a class="entry-meta__link" href=""> '.$getNewsR['date'].'</a></span>
                                            <span
                                            class="entry-meta__categorie bg-primary">Новости</span>
                                        </div>
                                        <div class="entry-meta__group-right"><span class="entry-meta__item"><i class=""></i><a class="entry-meta__link" href=""></a></span><span class="entry-meta__item"><i class=""></i><a class="entry-meta__link" href=""></a></span>
                                        </div>
                                    </div>
                                    <div class="entry-header">
                                        <h2 class="entry-title"><a href="">'.$getNewsR[name_rus].'</a></h2>
                                    </div>
                                    <div class="entry-content">
                                        '.$getNewsR[desc_rus].'
                                    </div>
                                </div>
                            </article>
                            <!-- end .post-->
                            
                            <!-- end .about-author-->
                            
                           
                            <!-- end .section-comment-->
                            
                            <!-- end .section-reply-form-->
                        </main>
                        <!-- end .l-main-content-->
                    </div>
		
		';
	}
}
?>