<?
session_start();
$id = mysqli_real_escape_string($GLOBALS['db'],trim($_GET['id']));
$url = explode('-',$id);
$id_counter = count($url);
$car_id = $url[$id_counter-1];
$getCar = mysqli_query($GLOBALS['db'],"SELECT * FROM catalog WHERE id='$car_id' LIMIT 1") or die(mysqli_error($GLOBALS['db']));
$getCarCount = mysqli_num_rows($getCar);
$getSystem = mysqli_query($GLOBALS['db'],"SELECT * FROM system WHERE id=2");
$getSystemRow = mysqli_fetch_array($getSystem);

if($getCarCount == 0)
{
	
}
else
{
	$getCarRow = mysqli_fetch_array($getCar);
	echo '
            <!-- end .b-title-page-->
            <div class="bg-grey">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="icon fa fa-home"></i></a>
                                </li>
                                <li><a href="#">Каталог</a>
                                </li>
                                <li class="active">Car</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end breadcrumb-->
            <main class="l-main-content" style="padding-top:120px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <section class="b-car-details">
                                <div class="b-car-details__header">
                                    <h2 class="b-car-details__title">'.$getCarRow[name].'</h2>
                                    <div class="b-car-details__subtitle"></div>
                                    
                                    
                                </div>
                                <div class="slider-car-details slider-pro" id="slider-car-details">
                                    <div class="sp-slides">';
                                        if(!empty($getCarRow[main_pic]))
										{
											echo '
											<div class="sp-slide">
												<img class="sp-image" src="'.$getCarRow[main_pic].'" alt="'.$getCarRow[alt_main].'" />
											</div>
											';
										}
										if(!empty($getCarRow[pic1]))
										{
											echo '
											<div class="sp-slide">
												<img class="sp-image" src="'.$getCarRow[pic1].'" alt="'.$getCarRow[alt1].'" />
											</div>
											';
										}
										if(!empty($getCarRow[pic2]))
										{
											echo '
											<div class="sp-slide">
												<img class="sp-image" src="'.$getCarRow[pic2].'" alt="'.$getCarRow[alt2].'" />
											</div>
											';
										}
										if(!empty($getCarRow[pic3]))
										{
											echo '
											<div class="sp-slide">
												<img class="sp-image" src="'.$getCarRow[pic3].'" alt="'.$getCarRow[alt3].'" />
											</div>
											';
										}
										if(!empty($getCarRow[pic4]))
										{
											echo '
											<div class="sp-slide">
												<img class="sp-image" src="'.$getCarRow[pic4].'" alt="'.$getCarRow[alt4].'" />
											</div>
											';
										}
										if(!empty($getCarRow[pic5]))
										{
											echo '
											<div class="sp-slide">
												<img class="sp-image" src="'.$getCarRow[pic5].'" alt="'.$getCarRow[alt5].'" />
											</div>
											';
										}
                                        
                                    echo '</div>
                                    <div class="sp-thumbnails">';
                                        if(!empty($getCarRow[main_pic]))
										{
											echo '
											<div class="sp-thumbnail">
                                            <img class="img-responsive" src="'.$getCarRow[main_pic].'" alt="'.$getCarRow[alt_main].'" />
                                        </div>
											';
										}
										if(!empty($getCarRow[pic1]))
										{
											echo '
											<div class="sp-thumbnail">
                                            <img class="img-responsive" src="'.$getCarRow[pic1].'" alt="'.$getCarRow[alt1].'" />
                                        </div>
											';
										}
										if(!empty($getCarRow[pic2]))
										{
											echo '
											<div class="sp-thumbnail">
                                            <img class="img-responsive" src="'.$getCarRow[pic2].'" alt="'.$getCarRow[alt2].'" />
                                        </div>
											';
										}
										if(!empty($getCarRow[pic3]))
										{
											echo '
											<div class="sp-thumbnail">
                                            <img class="img-responsive" src="'.$getCarRow[pic3].'" alt="'.$getCarRow[alt3].'" />
                                        </div>
											';
										}
										if(!empty($getCarRow[pic4]))
										{
											echo '
											<div class="sp-thumbnail">
                                            <img class="img-responsive" src="'.$getCarRow[pic4].'" alt="'.$getCarRow[alt4].'" />
                                        </div>
											';
										}
										if(!empty($getCarRow[pic5]))
										{
											echo '
											<div class="sp-thumbnail">
                                            <img class="img-responsive" src="'.$getCarRow[pic5].'" alt="'.$getCarRow[alt5].'" />
                                        </div>
											';
										}
                                       
                                    echo '</div>
                                </div>
                                <!-- end .b-thumb-slider-->
                                <div class="b-car-details__section">
                                    <h3 class="b-car-details__section-title ui-title-inner">Информация</h3>';
									if($getCarRow['class'] == 'service')
									{
									}
									else
									{
										
																			echo '<br><b>'.$getCarRow[description_rus].'</b>';
									}
									
                                    
                                echo '</div>
                                
                                <div class="b-car-details__tabs tab-content">
                                    
                                    
                                </div>
                            </section>
                        </div>
                        <div class="col-md-4">
                            <aside class="l-sidebar-2">
                                <div class="b-car-info">
                                    <div class="b-car-info__price">От '.$getCarRow["30-31"].' Лари
                                    </div>
                                    <dl class="b-car-info__desc dl-horizontal bg-grey">
										<dt class="b-car-info__desc-dt">1-2 Дня</dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow["price"].' лари / за 1 День</dd>
										<dt class="b-car-info__desc-dt">3-4 Дней</dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow["3-4"].' лари / за 1 День</dd>
										<dt class="b-car-info__desc-dt">5-7 Дней</dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow["5-7"].' лари / за 1 День</dd>
										<dt class="b-car-info__desc-dt">8-10 Дней</dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow["8-10"].' лари / за 1 День</dd>
										<dt class="b-car-info__desc-dt">11-14 Дней</dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow["11-15"].' лари / за 1 День</dd>
										<dt class="b-car-info__desc-dt">15-17 Дней</dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow["16-21"].' лари / за 1 День</dd>
										<dt class="b-car-info__desc-dt">18-20 Дней</dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow["22-25"].' лари / за 1 День</dd>
										<dt class="b-car-info__desc-dt">21-23 Дня</dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow["26-27"].' лари / за 1 День</dd>
										<dt class="b-car-info__desc-dt">24-26 Дней</dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow["28-29"].' лари / за 1 День</dd>
										<dt class="b-car-info__desc-dt">27-31 Дней</dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow["30-31"].' лари / за 1 День</dd>
                                        
                                        
                                         
                                    </dl>';
									
									
									
                                    
                                    echo '
                                    <!-- end .b-calculator-->
                                </div>
								<div class="b-car-info">
                                    <div class="b-car-info__price">ХАРАКТЕРИСТИКИ
                                    </div>
                                    <dl class="b-car-info__desc dl-horizontal bg-grey">
                                        <dt class="b-car-info__desc-dt">Класс</dt>
                                        <dd class="b-car-info__desc-dd">';
										if($getCarRow['class'] == 'SUV')
										{
											echo 'JEEP';
										}
										if($getCarRow['class'] == 'sedan')
										{
											echo 'SEDAN';
										}
										echo '</dd>
                                        <dt class="b-car-info__desc-dt">Топливо</dt>
                                        <dd class="b-car-info__desc-dd">';
										if($getCarRow[fuel] == 1)
										{
											echo 'Бензин';
										}
										if($getCarRow[fuel] == 2)
										{
											echo 'Дизель';
										}
										if($getCarRow[fuel] == 3)
										{
											echo 'Гибрид';
										}
										if($getCarRow[fuel] == 4)
										{
											echo 'Электро';
										}
										echo '</dd>
                                        <dt class="b-car-info__desc-dt">Год</dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow[year].'</dd>
                                        <dt class="b-car-info__desc-dt">Топливный Бак</dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow[capacity].'</dd>
                                        <dt class="b-car-info__desc-dt">Двигатель</dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow[engine].'</dd>
                                        <dt class="b-car-info__desc-dt">FUEL 100KM</dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow[sto].'</dd>
										<dt class="b-car-info__desc-dt">Ведущие колеса</dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow[wd_eng].'</dd>
                                        <dt class="b-car-info__desc-dt">Трансмиссия</dt>
                                        <dd class="b-car-info__desc-dd">';
										if($getCarRow[transmision] == 1)
										{
											echo 'Автоматика';
										}
										if($getCarRow[transmision] == 2)
										{
											echo 'Механика';
										}
										if($getCarRow[transmision] == 3)
										{
											echo 'Типтроник';
										}
										echo '</dd>
                                        <dt class="b-car-info__desc-dt"><img src="http://rentcartbilisi.com/assets/img/img_536840.png" width="15" height="15"> <b>Руль</b></dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow[wheel_rus].'</dd>
                                        <dt class="b-car-info__desc-dt"><img src="http://rentcartbilisi.com/assets/img/17460-200.png" width="15" height="15"> <b>Двери</b></dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow[doors].'</dd>
										<dt class="b-car-info__desc-dt"><i class="fa fa-snowflake-o" aria-hidden="true"></i> <b>Конд.</b></dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow[air].'</dd>
										<dt class="b-car-info__desc-dt"><i class="fa fa-user" aria-hidden="true"></i> <b>Места</b></dt>
                                        <dd class="b-car-info__desc-dd">'.$getCarRow[seats].'</dd>
                                    </dl>
                                   
                                    
                                    <!-- end .b-banner-->
                                    
                                </div>
                            </aside>
						</div>
						<div class="col-md-12">
						<ul class="b-car-details__nav nav nav-tabs bg-primary">
                                    <li class="active"><a href="#specifications" data-toggle="tab">ОФОРМЛЕНИЕ ЗАКАЗА</a>
                                    </li>
                                    
                                </ul>
							<div class="b-car-details__tabs tab-content">
                                    <div class="tab-pane active fade in" id="specifications">
                                        <section class="section-reply-form" id="section-reply-form">
                                <h2 class="ui-title-inner">ОТПРАВТЬЕ ЗАПРОС НА АРЕНДУ ЭТОГО АВТО</h2>
								
								
                                <form class="form-reply ui-form-2" action="#" method="post">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="ui-label">Имя</div>
                                                <input style="margin-bottom:0;"  id="firstname" class="form-control" type="text" required="required" />
                                            </div>
											<div class="form-group">
                                                <div class="ui-label">Фамилия</div>
                                                <input style="margin-bottom:0;" id="lastname" class="form-control" type="text" required="required" />
                                            </div>
                                            
											<div class="form-group">
                                                <div class="ui-label">Возраст и Вод.права</div>
                                                <input class="forms__check hidden" id="chec" type="checkbox"" />
												<label style="font-size:17px;" class="forms__label forms__label-check forms__label-check-1" for="chec">Вам 21 или больше? У вас ксть водительские права?</label><br>
                                            </div>
											<div class="form-group">
                                                <div class="ui-label">ВЫБЕРИТЕ СЕРВИС: </div>
													<div class="row">';
														$getServicesc = mysqli_query($GLOBALS['db'],"SELECT * FROM services");
														$getServicescNum = mysqli_num_rows($getServicesc);
														
														$showHalf = ceil($getServicescNum/2);
														$showLeft = $getServicescNum - $showHalf;
														
														$startFrom = 0 + $showHalf;
														
														$getServices = mysqli_query($GLOBALS['db'],"SELECT * FROM services LIMIT 0,$showHalf");
														$getServicesR = mysqli_fetch_array($getServices);
														$c=1;
														do
														{
															if(isset($_SESSION["ser$getServicesR[id]"]))
															{
																$checked = 'checked';
															}
															else
															{
																$checked = '';
															}
															echo '
															
															<div class="col-md-12 col-xs-12">
																<input onchange=\'calculate();\' class="forms__check hidden" id="chec'.$getServicesR[id].'" type="checkbox"" '.$checked.'/>
																<label style="font-size:15px; margin-bottom:25px; font-weight:bold;" class="forms__label forms__label-check forms__label-check-1" for="chec'.$getServicesR[id].'">'.$getServicesR[name_rus].'</label>
															</div>
															
															';
															$c++;
														}
														while($getServicesR = mysqli_fetch_array($getServices));
														echo '
													</div>
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="ui-label">E-mail</div>
                                                <input style="margin-bottom:0;" id="Email" class="form-control" type="email" required="required" />
                                            </div>
                                            <div class="form-group">
                                                <div class="ui-label">Мобильный</div>
                                                <input style="margin-bottom:0;" id="mob" class="form-control" type="tel" required="required" />
                                            </div>
											<div class="form-group" style="visibility:hidden;">
                                                <div class="ui-label">AGE</div>
                                                <input class="forms__check hidden" id="checs" type="checkbox"" />
												<label class="forms__label forms__label-check forms__label-check-1" for="chec">Driver age is 21 year or more with minimum 1 year driving experience</label><br>
                                            </div><br>
											<div class="form-group" style="margin-top:3px;">
                                                
													<div class="row">';
														$getServices = mysqli_query($GLOBALS['db'],"SELECT * FROM services LIMIT $startFrom,$showLeft");
														$getServicesR = mysqli_fetch_array($getServices);
														$c=1;
														do
														{
															if(isset($_SESSION["ser$getServicesR[id]"]))
															{
																$checked = 'checked';
															}
															else
															{
																$checked = '';
															}
															echo '
															
															<div class="col-md-12 col-xs-12">
																<input onchange=\'calculate();\' class="forms__check hidden" id="chec'.$getServicesR[id].'" type="checkbox"" '.$checked.'/>
																<label style="font-size:15px; margin-bottom:25px; font-weight:bold;" class="forms__label forms__label-check forms__label-check-1" for="chec'.$getServicesR[id].'">'.$getServicesR[name_rus].'</label>
															</div>
															
															';
															$c++;
														}
														while($getServicesR = mysqli_fetch_array($getServices));
														echo '
													</div>
                                                
                                            </div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
                                                <div class="ui-label">МЕСТО ПОЛУЧЕНИЕ</div>
                                                <select id="pickup" name="place" data-width="100%" style="width:100%; height:48px;" onchange=\'calculate();\'>';
												
												if(isset($_SESSION['place']))
												{
													if($_SESSION['place'] == 1)
													{
														$price = 'Any Place Free';
													}
													if($_SESSION['place'] == 2)
													{
														$price = $getCarRow[pick_tbs_air];
													}
													if($_SESSION['place'] == 3)
													{
														$price = $getCarRow[pick_btm];
													}
													if($_SESSION['place'] == 4)
													{
														$price = $getCarRow[pick_btm_air];
													}
													if($_SESSION['place'] == 5)
													{
														$price = $getCarRow[pick_kut];
													}
													if($_SESSION['place'] == 6)
													{
														$price = $getCarRow[pick_kut_air];
													}
													if($_SESSION['place'] == 7)
													{
														$price = $getCarRow[pick_step];
													}
													
													
													$getPlaceId = mysqli_query($GLOBALS['db'],"SELECT * FROM places WHERE id='$_SESSION[place]' LIMIT 1");
													$getPlaceIdR = mysqli_fetch_array($getPlaceId);
													if($price == 'Any Place Free')
													{
														echo '<option value="'.$_SESSION['place'].'">'.$getPlaceIdR['name_rus'].' ('.$price.')</option>';
													}
													else
													{
														echo '<option value="'.$_SESSION['place'].'">'.$getPlaceIdR['name_rus'].' (+'.$price.'GEL)</option>';
													}
													
													$getOtherPlaces = mysqli_query($GLOBALS['db'],"SELECT * FROM places WHERE id!='$_SESSION[place]'");
													$getOtherPlacesR = mysqli_fetch_array($getOtherPlaces);
													
													do
													{
														echo '<option value="'.$getOtherPlacesR[id].'">'.$getOtherPlacesR[name_eng].' ('.$getOtherPlacesR[price].')</option>';
													}
													while($getOtherPlacesR = mysqli_fetch_array($getOtherPlaces));
												}
												else
												{
													$getOtherPlaces = mysqli_query($GLOBALS['db'],"SELECT * FROM places ORDER BY id ASC");
													$getOtherPlacesR = mysqli_fetch_array($getOtherPlaces);
													
													do
													{
														if($getOtherPlacesR[id] == 1)
														{
															$price2 = $getCarRow[pick_tbs];
														}
														if($getOtherPlacesR[id] == 2)
														{
															$price2 = $getCarRow[pick_tbs_air];
														}
														if($getOtherPlacesR[id] == 3)
														{
															$price2 = $getCarRow[pick_btm];
														}
														if($getOtherPlacesR[id] == 4)
														{
															$price2 = $getCarRow[pick_btm_air];
														}
														if($getOtherPlacesR[id] == 5)
														{
															$price2 = $getCarRow[pick_kut];
														}
														if($getOtherPlacesR[id] == 6)
														{
															$price2 = $getCarRow[pick_kut_air];
														}
														if($getOtherPlacesR[id] == 7)
														{
															$price2 = $getCarRow[pick_step];
														}
														echo '<option value="'.$getOtherPlacesR[id].'">'.$getOtherPlacesR[name_rus];
														if($price2 == 0)
														{
															echo '( <b>FREE</b> )';
														}
														else
														{
															echo '(+<b>'.$price2.'</b>GEL)';
														}
														
														
														echo '</option>';
													}
													while($getOtherPlacesR = mysqli_fetch_array($getOtherPlaces));
												}
												
											
											
											echo '</select>
                                            </div>
											<div class="form-group">
												<div class="ui-label">ДАТА ПОЛУЧЕНИЕ</div>
												<div class="row">
													<div class="col-md-7 col-xs-7">
														<input style="margin-bottom:0;border: 1px solid #6d6d6d;" class="form-control" type="text" autocomplete="off" id="datetimepicker3" placeholder="Start" required="required" value="';
														if(isset($_SESSION[pick]))
														{
															echo $_SESSION[pick];
														}
														else
														{
															echo date('Y/m/d');
														}
														echo '" onchange=\'calculate();\'/>
													</div>
													<div class="col-md-5 col-xs-5">
														<select id="time1" name="time1" style="font-size:20px;padding-left:18px;width:100%;height:48px;" data-width="100%" onchange=\'calculate();\'>';
																
																	if(isset($_SESSION['time1']))
        															{
        																echo '<option value="'.$_SESSION['time1'].'">'.$_SESSION['time1'].'</option>';
        																for($k=0;$k<24;$k++)
        																{
        																	if($k < 9)
        																	{
        																		echo '<option value="0'.$k.':00">0'.$k.':00</option>';
        																	}
        																	if($k == 9)
        																	{
        																		echo '<option value="0'.$k.':00">0'.$k.':00</option>';
        																	}
        																	if($k > 9)
        																	{
        																		echo '<option value="'.$k.':00">'.$k.':00</option>';
        																	}
        																}
        															}
        															else
        															{
        																for($k=0;$k<24;$k++)
        																{
        																	if($k < 9)
        																	{
        																		echo '<option value="0'.$k.':00">0'.$k.':00</option>';
        																	}
        																	if($k == 9)
        																	{
        																		echo '<option value="0'.$k.':00">0'.$k.':00</option>';
        																	}
        																	if($k > 9)
        																	{
        																		echo '<option value="'.$k.':00">'.$k.':00</option>';
        																	}
        																}
        															}
																
															echo '</select>
													</div>
												</div>
                                            </div>
											<div class="form-group">
                                                <div class="ui-label">МЕСТО ВОЗВРАТА</div>
                                                <select id="dropoff" name="place" data-width="100%" style="width:100%; height:48px;" onchange=\'calculate();\'>';
													if(isset($_SESSION['drop']))
													{
														if($_SESSION['drop'] == 1)
														{
															$price = 'Any Place Free';
														}
														if($_SESSION['drop'] == 2)
														{
															$price = $getCarRow[drop_tbs_air];
														}
														if($_SESSION['drop'] == 3)
														{
															$price = $getCarRow[drop_btm];
														}
														if($_SESSION['drop'] == 4)
														{
															$price = $getCarRow[drop_btm_air];
														}
														if($_SESSION['drop'] == 5)
														{
															$price = $getCarRow[drop_kut];
														}
														if($_SESSION['drop'] == 6)
														{
															$price = $getCarRow[drop_kut_air];
														}
														if($_SESSION['drop'] == 7)
														{
															$price = $getCarRow[drop_step];
														}
														$getPlaceId = mysqli_query($GLOBALS['db'],"SELECT * FROM dropoff WHERE id='$_SESSION[drop]' LIMIT 1");
														$getPlaceIdR = mysqli_fetch_array($getPlaceId);
														if($price == 'Any Place Free')
														{
															echo '<option value="'.$_SESSION['drop'].'">'.$getPlaceIdR['name_rus'].' ('.$price.')</option>';
														}
														else
														{
															echo '<option value="'.$_SESSION['drop'].'">'.$getPlaceIdR['name_rus'].' (+'.$price.'GEL)</option>';
														}
														
														
														$getOtherPlaces = mysqli_query($GLOBALS['db'],"SELECT * FROM dropoff WHERE id!='$_SESSION[drop]'");
														$getOtherPlacesR = mysqli_fetch_array($getOtherPlaces);
														
														do
														{
															echo '<option value="'.$getOtherPlacesR[id].'">'.$getOtherPlacesR[name_rus].' ('.$getOtherPlacesR[price].')</option>';
														}
														while($getOtherPlacesR = mysqli_fetch_array($getOtherPlaces));
													}
													else
													{
														$getOtherPlaces = mysqli_query($GLOBALS['db'],"SELECT * FROM dropoff ORDER BY id ASC");
														$getOtherPlacesR = mysqli_fetch_array($getOtherPlaces);
														
														do
														{
															if($getOtherPlacesR[id] == 1)
															{
																$price3 = $getCarRow[drop_tbs];
															}
															if($getOtherPlacesR[id] == 2)
															{
																$price3 = $getCarRow[drop_tbs_air];
															}
															if($getOtherPlacesR[id] == 3)
															{
																$price3 = $getCarRow[drop_btm];
															}
															if($getOtherPlacesR[id] == 4)
															{
																$price3 = $getCarRow[drop_btm_air];
															}
															if($getOtherPlacesR[id] == 5)
															{
																$price3 = $getCarRow[drop_kut];
															}
															if($getOtherPlacesR[id] == 6)
															{
																$price3 = $getCarRow[drop_kut_air];
															}
															if($getOtherPlacesR[id] == 7)
															{
																$price3 = $getCarRow[drop_step];
															}
															echo '<option value="'.$getOtherPlacesR[id].'">'.$getOtherPlacesR[name_rus];

															if($price3 == 0)
															{
																echo '( <b>FREE</b> )';
															}
															else
															{
																echo '(+<b>'.$price3.'</b>GEL)';
															}
															
															echo '</option>';
														}
														while($getOtherPlacesR = mysqli_fetch_array($getOtherPlaces));
													}
											
											echo '</select>
                                            </div>
											<div class="form-group">
												<div class="ui-label">ДАТА ВОЗВРАТА</div>
                                                <input type="hidden" id="carID" value="'.$id.'">
												<div class="row">
													<div class="col-md-7 col-xs-7">
														<input class="form-control" style="margin-bottom:0;" type="text" autocomplete="off" id="datetimepicker2" placeholder="End" required="required" value="';
														if(isset($_SESSION['end']))
														{
															echo $_SESSION['end'];
														}
														else
														{
															
															$day = date('d') + 1;
															if($day < 10)
															{
																echo date('Y/m/').'0'.$day;
															}
															else
															{
																echo date('Y/m/').$day;
															}
															
														}
														echo '" onchange=\'calculate();\' />
													</div>
													<div class="col-md-5 col-xs-5">
														<select id="time2" name="time1" style="font-size:20px;padding-left:18px;width:100%;height:48px;" data-width="100%" onchange=\'calculate();\'>';
															
																if(isset($_SESSION['time2']))
    															{
    																echo '<option value="'.$_SESSION['time2'].'">'.$_SESSION['time2'].'</option>';
    																for($k=0;$k<24;$k++)
    																{
    																	if($k < 9)
    																	{
    																		echo '<option value="0'.$k.':00">0'.$k.':00</option>';
    																	}
    																	if($k == 9)
    																	{
    																		echo '<option value="0'.$k.':00">0'.$k.':00</option>';
    																	}
    																	if($k > 9)
    																	{
    																		echo '<option value="'.$k.':00">'.$k.':00</option>';
    																	}
    																}
    															}
    															else
    															{
    																for($k=0;$k<24;$k++)
    																{
    																	if($k < 9)
    																	{
    																		echo '<option value="0'.$k.':00">0'.$k.':00</option>';
    																	}
    																	if($k == 9)
    																	{
    																		echo '<option value="0'.$k.':00">0'.$k.':00</option>';
    																	}
    																	if($k > 9)
    																	{
    																		echo '<option value="'.$k.':00">'.$k.':00</option>';
    																	}
    																}
    															}
															
														echo '</select>
													</div>
												</div>
												
                                            </div>
											
											
											
											<div class="form-group">
												<div class="ui-label">PROMO CODE (if exist)</div>
                                                <input type="hidden" id="carID" value="'.$id.'">
												<div class="row">
													<div class="col-md-12 col-xs-12">
														<input class="form-control"  maxlength="5" style="margin-bottom:0;" type="text" autocomplete="off" name="promo" id="promo" placeholder="Ведите промокод"  value="';
														if(isset($_SESSION['promo']))
														{
															echo $_SESSION['promo'];
														}
														
														echo '" onkeyup=\'calculate();\' />
													</div>
													<div class="col-md-12" id="promo_answer">
														<p style="text-align:center; color:red; font-size:17px; font-weight:900; display:none" >Promo not real!!!</p>
													</div>
													
												</div>
												
                                            </div>
											
											
											<div class="form-group" >
												
                                                <span id="amount"> <b id="zero">';
												$DaysCount = $_SESSION[days];
												if($DaysCount >= 1 and $DaysCount <=3)
												{
													$price = ($getCarRow['price']*$DaysCount).'GEL';
												}
												if($DaysCount > 4 and $DaysCount <= 6)
												{
													$price = ($getCarRow['3-4']*$DaysCount).'GEL';
												}
												if($DaysCount >= 7 and $DaysCount <= 9)
												{
													$price = ($getCarRow['5-7']*$DaysCount).'GEL';
												}
												if($DaysCount >= 10 and $DaysCount <= 12)
												{
													$price = ($getCarRow['8-10']*$DaysCount).'GEL';
												}
												if($DaysCount >= 13 and $DaysCount <= 15)
												{
													$price = ($getCarRow['11-15']*$DaysCount).'GEL';
												}
												if($DaysCount >= 16 and $DaysCount <= 18)
												{
													$price = ($getCarRow['16-21']*$DaysCount).'GEL';
												}
												if($DaysCount >= 19 and $DaysCount <= 21)
												{
													$price = ($getCarRow['22-25']*$DaysCount).'GEL';
												}
												if($DaysCount >= 22 and $DaysCount <= 24)
												{
													$price = ($getCarRow['26-27']*$DaysCount).'GEL';
												}
												if($DaysCount >= 25 and $DaysCount <= 27)
												{
													$price = ($getCarRow['28-29']*$DaysCount).'GEL';
												}
												if($DaysCount >= 28 and $DaysCount <= 31)
												{
													$price = ($getCarRow['30-31']*$DaysCount).'GEL';
												}
												if($DaysCount > 31)
												{
													$price = 'price with deal';
												}
												echo $price;
												
												if(!isset($_SESSION[days]))
												{
													echo '0';
												}
												echo '</b></span>
                                            </div>
											<div class="form-group">
                                                <input id="b" type="hidden" value="book">
												<div class="form-group" style="font-size:36px;"><span id="answer"><button id="book" onclick=\'zak()\' class="btn btn-dark">Забронировать</button></span>
                                            </div>
										</div>
										</form>
                            </section>
                                    </div>
                    </div>
                </div>
                <!-- end .b-car-details-->
                
                <!-- end .section-default_top-->
            </main>';
}
?>