<?
include("db.php");
?>

            <!-- end .b-title-page-->
            <div class="bg-grey">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="/ru"><i class="icon fa fa-home"></i></a>
                                </li>
                                <li class="active">Последнии Новости</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end breadcrumb-->
            <div class="container">
                <div class="row">
<div class="col-md-8">
                        <main class="l-main-content" style="padding-top:120px;">
                            <div class="posts-group-2">
							
							<?
								$getNews = mysqli_query($GLOBALS['db'],"SELECT * FROM news ORDER BY id DESC");
								$getNewsCount = mysqli_num_rows($getNews);
								if($getNewsCount == 0)
								{
									echo '<div class="alert alert-warning"><i class="icon icon_error-circle_alt"></i>News not found!!!</div>';
								}
								else
								{
									$getNewsR = mysqli_fetch_array($getNews);
									do
									{
										echo '<section class="b-post b-post-full clearfix">
											 <div class="entry-media">
                                    ';
									if(empty($getNewsR[video]))
												{
													echo '<a class="js-zoom-images" href="'.$getNewsR[main_pic].'">
                                        <img class="img-responsive" src="'.$getNewsR[main_pic].'" alt="Foto" />
                                    </a>';
												}
												else
												{
													echo '<iframe id="ytplayer" type="text/html" width="100%" height="640" src="'.$getNewsR[video].'" frameborder="0"/></iframe>';
												}
                                echo '</div>
											<div class="entry-main">
												<div class="entry-meta">
													<div class="entry-meta__group-left"><span class="entry-meta__item">On<a class="entry-meta__link" href="'.$getNewsR[id].'"> '.$getNewsR['date'].'</a></span>
														<span
														class="entry-meta__categorie bg-primary">Новости</span>
													</div>
													<div class="entry-meta__group-right"><span class="entry-meta__item"><i class=""></i><a class="entry-meta__link" href=""></a></span><span class="entry-meta__item"><i class=""></i><a class="entry-meta__link" href=""></a></span>
													</div>
												</div>
												<div class="entry-header">
													<h2 class="entry-title"><a href="'.$getNewsR[id].'">'.$getNewsR[name_rus].'</a></h2>
												</div>
												<div class="entry-content">
													<p>';
													$len = strlen(strip_tags($getNewsR[desc_rus]));
													if($len <= 1500)
													{
														echo strip_tags($getNewsR[desc_rus]);
													}
													if($len > 1501)
													{
														
														$short_text = mb_substr(strip_tags($getNewsR[desc_rus]),0,1500,'UTF-8');
														$short_text = $short_text . '...';
														echo $short_text;
													}
													echo '</p>
												</div>
												<div class="entry-footer"><a class="btn btn-default" href="'.$getNewsR[id].'">Детально</a>
												</div>
											</div>
										</section>';
									}
									while($getNewsR = mysqli_fetch_array($getNews));
								}
								
							?>	
								
                                <!-- end .post-->
                                
                                <!-- end .post-->
                            </div>
                           
                        </main>
                        <!-- end .l-main-content-->
                    </div>