<?
ob_start("ob_gzhandler");
include("db.php");
?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta name="yandex-verification" content="44786e581c8d566f" />
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111206860-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111206860-1');
</script>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-2489017558407552",
    enable_page_level_ads: true
  });
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter49005989 = new Ya.Metrika({
                    id:49005989,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/49005989" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge" />
        <title>Прокат авто в Грузии от Auto4rental | мы заботимся о вашем комфорте</title>
        <meta content="Узнайте как взять в аренде подходящий машины в Грузии. Вы можете арендовать автомобиль Тбилиси онлайн. Мы предлагаем бесплатное бронирование авто в Тбилиси." name="description" />
        <meta content="<? echo $getSystemRow[keywords_rus]; ?>" name="keywords" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta property="og:title" content="Auto4Rental - car rental in tbilisi, rent a car from 19$ per day" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="http://rentcartbilisi.com" />
		<meta property="og:image" content="http://rentcartbilisi.com/black.png" />
        <meta content="telephone=no" name="format-detection" />
        <meta name="HandheldFriendly" content="true" />
        <link rel="icon" type="image/x-icon" href="http://rentcartbilisi.com/ru/favicon.ico" />
		<link rel="stylesheet" type="text/css" href="http://rentcartbilisi.com/assets/fonts/flaticon/font/flaticon.css">
		
		<link rel="stylesheet" href="http://rentcartbilisi.com/assets/plugins/slider-pro/slider-pro.css">
		<link rel="stylesheet" href="http://rentcartbilisi.com/mega2.css">
        <!--[if lt IE 9 ]>
<script src="http://rentcartbilisi.com/ru/assets/js/separate-js/html5shiv-3.7.2.min.js" type="text/javascript"></script><meta content="no" http-equiv="imagetoolbar">
<![endif]-->
    </head>

    <body>
        <!-- Loader-->
        <div id="page-preloader"><span class="spinner border-t_second_b border-t_prim_a"></span>
        </div>
        <!-- Loader end-->
        <!-- ==========================-->
        <!-- MOBILE MENU-->
        <!-- ==========================-->
        <? include("assets/blocks/header_m.php"); ?>
        <div class="l-theme animated-css" data-header="sticky" data-header-top="200" data-canvas="container">
            
            <!-- ==========================-->
            <!-- SEARCH MODAL-->
            <!-- ==========================-->
            <div class="header-search open-search">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                            <div class="navbar-search">
                                <form class="search-global">
                                    <input class="search-global__input" type="text" placeholder="Type to search" autocomplete="off" name="s" value="" />
                                    <button class="search-global__btn"><i class="icon stroke icon-Search"></i>
                                    </button>
                                    <div class="search-global__note">Begin typing your search above and press return to search.</div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="search-close close" type="button"><i class="fa fa-times"></i>
                </button>
            </div>
            <? include("assets/blocks/header.php"); ?>
            <!-- end .header-->
					
                    
            <!-- end .b-title-page-->
            <div class="bg-grey">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="/ru"><i class="icon fa fa-home"></i></a>
                                </li>
                                
                                <li class="active">О НАС</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end breadcrumb-->
            <div class="container2">
                <div class="row">
                    <div class="col-md-8" style="width:100%;">
                        <main class="l-main-content" style="padding-top:120px;">
                            <article class="b-post b-post-full clearfix">
                                <div class="entry-main">
 
                                    <div class="entry-header">
                                        <h2 class="entry-title" align="center"><a href="#"></a></h2>
                                    </div>
                                    <div class="entry-content">
                                        <?
										$getContent = mysqli_query($GLOBALS['db'],"SELECT * FROM system WHERE id=2");
										$getContentR = mysqli_fetch_array($getContent);
										echo $getContentR['about-us-rus'];
										?>
                                    </div>
                                </div>
                                
                            </article>
                            <!-- end .post-->
                            
                            <!-- end .section-reply-form-->
                        </main>
                        <!-- end .l-main-content-->
                    </div>
                    
                </div>
            </div>
            <? include("assets/blocks/footer.php"); ?>
            <!-- .footer-->
        </div>
        <!-- end layout-theme-->


        <!-- ++++++++++++-->
        <!-- MAIN SCRIPTS-->
        <!-- ++++++++++++-->
        <script src="http://rentcartbilisi.com/ru/bro2.js"></script>
		
    </body>



</html>