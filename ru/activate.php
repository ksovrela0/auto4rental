<?
ob_start("ob_gzhandler");
include("db.php");
?>
<!DOCTYPE html>
<html lang="zxx">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111206860-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111206860-1');
</script>

        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge" />
        <title>Подтверждение</title>
        <meta content="Auto4Rental - Georgian one of the best car rental service!!! - <? echo $getSystemRow[keywords]; ?>" name="description" />
        <meta content="<? echo $getSystemRow[keywords]; ?>" name="keywords" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta content="telephone=no" name="format-detection" />
        <meta name="HandheldFriendly" content="true" />
        <link rel="stylesheet" href="assets/css/master.css" />
		<meta property="og:title" content="Auto4Rental - car rental in tbilisi, rent a car from 19$ per day" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="http://rentcartbilisi.com" />
		<meta property="og:image" content="http://rentcartbilisi.com/black.png" />
        <!-- SWITCHER-->
        <link href="assets/plugins/switcher/css/switcher.css" rel="stylesheet" id="switcher-css" />
        <link href="assets/plugins/switcher/css/color1.css" rel="alternate stylesheet" title="color1" />
        <link href="assets/plugins/switcher/css/color2.css" rel="alternate stylesheet" title="color2" />
        <link href="assets/plugins/switcher/css/color3.css" rel="alternate stylesheet" title="color3" />
        <link href="assets/plugins/switcher/css/color4.css" rel="alternate stylesheet" title="color4" />
        <link rel="icon" type="image/x-icon" href="favicon.ico" />
        <!--[if lt IE 9 ]>
<script src="assets/js/separate-js/html5shiv-3.7.2.min.js" type="text/javascript"></script><meta content="no" http-equiv="imagetoolbar">
<![endif]-->
    </head>

    <body>
        <!-- Loader-->
        <div id="page-preloader"><span class="spinner border-t_second_b border-t_prim_a"></span>
        </div>
        <!-- Loader end-->
        <!-- ==========================-->
        <!-- MOBILE MENU-->
        <!-- ==========================-->
        <? include("assets/blocks/header_m.php"); ?>
        <div class="l-theme animated-css" data-header="sticky" data-header-top="200" data-canvas="container">
            
            <!-- ==========================-->
            <!-- SEARCH MODAL-->
            <!-- ==========================-->
            <div class="header-search open-search">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                            <div class="navbar-search">
                                <form class="search-global">
                                    <input class="search-global__input" type="text" placeholder="Type to search" autocomplete="off" name="s" value="" />
                                    <button class="search-global__btn"><i class="icon stroke icon-Search"></i>
                                    </button>
                                    <div class="search-global__note">Begin typing your search above and press return to search.</div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="search-close close" type="button"><i class="fa fa-times"></i>
                </button>
            </div>
            <? include("assets/blocks/header.php"); ?>
            <!-- end .header-->
					
                    
            <!-- end .b-title-page-->
            <div class="bg-grey">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="index.php"><i class="icon fa fa-home"></i></a>
                                </li>
                                
                                <li class="active">Подтверждение</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end breadcrumb-->
            <div class="container2">
                <div class="row">
                    <div class="col-md-8" style="width:100%;">
                        <main class="l-main-content" style="padding-top:120px;">
                            
                            <!-- end .post-->

                            
                            
                            <!-- end .section-comment-->
                            <?
							if(isset($_GET[code]))
							{
								$code = mysql_real_escape_string(trim($_GET[code]));
								$checkCode = mysql_query("SELECT * FROM booking WHERE activated='$code'");
								$countCode = mysql_num_rows($checkCode);
								if($countCode == 1)
								{
									$acceptBook = mysql_query("UPDATE booking SET activated='yes' WHERE activated='$code'");
									echo '
									<div class="alert alert-block alert-5">
                                            
                                            
                                            <div class="alert-icon"><i class="icon fa fa-check"></i>
                                            </div>
                                            <div class="alert__inner">
                                                <h3 class="alert-title">Ваша почта подтверждена!!!</h3>
                                                <div class="alert-text">Мы свяжемя с вами!!!</div>
                                            </div>
                                        </div>
									';
								}
								
								else{
									echo '
									<div class="alert alert-block alert-4">
                                            <div class="alert-icon"><i class="icon fa fa-close"></i>
                                            </div>
                                            <div class="alert__inner">
                                                <h3 class="alert-title">Ошибка!!! Код неверный</h3>
                                                <div class="alert-text"></div>
                                            </div>
                                        </div>
									';
								}
							}
							?>
                            <!-- end .section-reply-form-->
                        </main>
                        <!-- end .l-main-content-->
                    </div>
                    
                </div>
            </div>
            <? include("assets/blocks/footer.php"); ?>
            <!-- .footer-->
        </div>
        <!-- end layout-theme-->


        <!-- ++++++++++++-->
        <!-- MAIN SCRIPTS-->
        <!-- ++++++++++++-->
        <script src="assets/libs/jquery-1.12.4.min.js"></script>
        <script src="assets/libs/jquery-migrate-1.2.1.js"></script>
        <!-- Bootstrap-->
        <script src="assets/libs/bootstrap/bootstrap.min.js"></script>
        <!-- User customization-->
        <script src="assets/js/custom.js"></script>
        <!-- Headers scripts-->
        <script src="assets/plugins/headers/slidebar.js"></script>
        <script src="assets/plugins/headers/header.js"></script>
        <!-- Color scheme-->
        <script src="assets/plugins/switcher/js/dmss.js"></script>
        <!-- Select customization & Color scheme-->
        <script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
        <!-- Slider-->
        <script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
        <!-- Pop-up window-->
        <script src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
        <!-- Mail scripts-->
        <script src="assets/plugins/jqBootstrapValidation.js"></script>
        <script src="assets/plugins/contact_me.js"></script>
        <!-- Filter and sorting images-->
        <script src="assets/plugins/isotope/isotope.pkgd.min.js"></script>
        <script src="assets/plugins/isotope/imagesLoaded.js"></script>
        <!-- Progress numbers-->
        <script src="assets/plugins/rendro-easy-pie-chart/jquery.easypiechart.min.js"></script>
        <script src="assets/plugins/rendro-easy-pie-chart/waypoints.min.js"></script>
        <!-- NoUiSlider-->
        <script src="assets/plugins/noUiSlider/nouislider.min.js"></script>
        <script src="assets/plugins/noUiSlider/wNumb.js"></script>
        <!-- Animations-->
        <script src="assets/plugins/scrollreveal/scrollreveal.min.js"></script>
		<script src="chat.js"></script>
    </body>



</html>