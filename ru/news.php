<?
ob_start("ob_gzhandler");
include("db.php");
?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta name="yandex-verification" content="44786e581c8d566f" />
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111206860-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111206860-1');
</script>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-2489017558407552",
    enable_page_level_ads: true
  });
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter49005989 = new Ya.Metrika({
                    id:49005989,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/49005989" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge" />
        <title>
		<?
			if(isset($_GET[id]) and !empty($_GET[id]))
			{
				$id = mysqli_real_escape_string($GLOBALS['db'],trim($_GET[id]));
				$getTitle = mysqli_query($GLOBALS['db'],"SELECT name_rus,desc_rus FROM news WHERE id='$id'") or die(mysql_error());
				$getTitleR = mysqli_fetch_array($getTitle);
				$title = $getTitleR[name_rus].' | '.$getSystemRow[title_rus];
				echo $title;
			}
			else
			{
				echo 'Новости как арендовать машину в Тбилиси | Прокат авто Тбилиси без залога';
			}
		?>
		</title>
        <meta content="Здесь вы можете прочитать, как взять напрокат автомобиль в Тбилиси в Грузии, без скрытых затрат, без ограничений километрам и без залога." name="description" />
        <meta content="<? echo $getSystemRow[keywords_rus]; ?>" name="keywords" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta property="og:title" content="Auto4Rental - car rental in tbilisi, rent a car from 19$ per day" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="http://rentcartbilisi.com" />
		<meta property="og:image" content="http://rentcartbilisi.com/black.png" />
        <meta content="telephone=no" name="format-detection" />
        <meta name="HandheldFriendly" content="true" />
        <link rel="icon" type="image/x-icon" href="http://rentcartbilisi.com/ru/favicon.ico" />
		<link rel="stylesheet" type="text/css" href="http://rentcartbilisi.com/assets/fonts/flaticon/font/flaticon.css">
		
		<link rel="stylesheet" href="http://rentcartbilisi.com/assets/plugins/slider-pro/slider-pro.css">
		<link rel="stylesheet" href="http://rentcartbilisi.com/mega2.css">
        <!--[if lt IE 9 ]>
<script src="http://rentcartbilisi.com/ru/assets/js/separate-js/html5shiv-3.7.2.min.js" type="text/javascript"></script><meta content="no" http-equiv="imagetoolbar">
<![endif]-->
    </head>

    <body>
        <!-- Loader-->
        <div id="page-preloader"><span class="spinner border-t_second_b border-t_prim_a"></span>
        </div>
        <!-- Loader end-->
        <!-- ==========================-->
        <!-- MOBILE MENU-->
        <!-- ==========================-->
        <? include("assets/blocks/header_m.php"); ?>
        <div class="l-theme animated-css" data-header="sticky" data-header-top="200" data-canvas="container">
            
            <!-- ==========================-->
            <!-- SEARCH MODAL-->
            <!-- ==========================-->
            <div class="header-search open-search">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                            <div class="navbar-search">
                                <form class="search-global">
                                    <input class="search-global__input" type="text" placeholder="Type to search" autocomplete="off" name="s" value="" />
                                    <button class="search-global__btn"><i class="icon stroke icon-Search"></i>
                                    </button>
                                    <div class="search-global__note">Begin typing your search above and press return to search.</div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="search-close close" type="button"><i class="fa fa-times"></i>
                </button>
            </div>
            <? include("assets/blocks/header.php"); ?>
            <!-- end .header-->
					
                    <?
					if(isset($_GET[id]) and !empty($_GET[id]))
					{
						include("assets/blocks/news_s.php");
					}
					else
					{
						include("assets/blocks/news.php"); 
					}
					?>
                    <div class="col-md-4">
                        <aside class="l-sidebar-3">
                            
                            <!-- end .widget-->
                            <!-- end .widget-->
                            <section class="widget section-sidebar">
                                <h3 class="widget-title ui-title-inner">Новости</h3>
                                <div class="widget-content">
								<?
								$getRecent = mysqli_query($GLOBALS['db'],"SELECT * FROM news ORDER BY id DESC LIMIT 4");
								$getRecentC = mysqli_num_rows($getRecent);
								if($getRecent == 0)
								{
									echo '<div class="alert alert-warning"><i class="icon icon_error-circle_alt"></i>News not found!!!</div>';
								}
								else
								{
									$getRecentR = mysqli_fetch_array($getRecent);
									do
									{
                                    echo '<div class="post-widget clearfix">
                                        <div class="post-widget__media">
                                            <a href="'.$getRecentR[id].'">
                                                <img class="img-responsive" src="'.$getRecentR[main_pic].'" alt="foto" />
                                            </a>
                                        </div>
                                        <div class="post-widget__inner"><a class="post-widget__title" href="'.$getRecentR[id].'">'.$getRecentR[name_rus].'</a>
                                            <div class="post-widget__date">On
                                                <time class="post-widget__time" datetime="">'.$getRecentR['date'].'</time>
                                            </div>
                                        </div>
                                        <!-- end .widget-post-->
                                    </div>';
									}
									while($getRecentR = mysqli_fetch_array($getRecent));
								}
                                  ?>
                                </div>
                            </section>
                            <!-- end .widget-->
                            
                            
                            
                            <!-- end .widget-->
                        </aside>
                        <!-- end .sidebar-->
                    </div>
                </div>
            </div>
            <? include("assets/blocks/footer.php"); ?>
            <!-- .footer-->
        </div>
        <!-- end layout-theme-->


        <!-- ++++++++++++-->
        <!-- MAIN SCRIPTS-->
        <!-- ++++++++++++-->
       <script src="http://rentcartbilisi.com/ru/bro2.js"></script>
    </body>



</html>