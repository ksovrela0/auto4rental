<?
session_start();

ob_start("ob_gzhandler");
include("db.php");

if(isset($_POST['date1']) and isset($_POST['date2']) and ($_POST['place'] !='none' or $_POST['drop'] != 'none'))
{
	$pick = $_POST['date1'];
	$end = $_POST['date2'];
	$_SESSION['pick'] = $pick;
	$_SESSION['end'] = $end;
	$_SESSION['place'] = $_POST[place];
	$_SESSION['drop'] = $_POST[drop];
	$getPlaceId = mysqli_query($GLOBALS['db'],"SELECT * FROM places WHERE name_eng='$_POST[place]' LIMIT 1");
	$getPlaceIdR = mysqli_fetch_array($getPlaceId);
	$_SESSION['place_id'] = $getPlaceIdR[id];
	$short_text = mb_substr($end,0,10,'UTF-8');
	$short_text2 = mb_substr($pick,0,10,'UTF-8');
	$pick = explode("/", $short_text2); //$pick[0] Day, $pick[1] Month, $pick[2] Year
	$end = explode("/", $short_text); //$end[0] Year, $end[1] Month, $end[2] Day
	$dayPick = $pick[2].'-'.$pick[1].'-'.$pick[0];
	$dayEnd = $end[2].'-'.$end[1].'-'.$end[0];
	$difference = intval(abs(strtotime($dayPick) - strtotime($dayEnd)));
	$promo = $_POST['promo'];
	$promoAct = 0;
	if(!empty($promo))
	{
		$promo = mysqli_real_escape_string($GLOBALS['db'],$promo);
		$data = array();
		
		$getPromo = mysqli_query($GLOBALS['db'],"SELECT * FROM promo WHERE promo='$promo' LIMIT 1");
		$promoIS = mysqli_num_rows($getPromo);
		
		if($promoIS >= 1)
		{
			$row = mysqli_fetch_array($getPromo);
			
			$promo_amount = $row['p'];
			$promoAct = 1;
			
			$_SESSION['promo'] = $promo;
			$_SESSION['promo_amount'] = $row['p'];
		}
		else
		{
			$promo_amount = 100;
		}
	}
	
	
	$DaysCount = $difference / (3600 * 24);
	
	
	$_SESSION['time1'] = $_POST['time1'];
	$_SESSION['time2'] = $_POST['time2'];
	
	$timePick = $_SESSION['pick'].' '.$_POST['time1'];
	$timePick = strtotime($timePick);
	
	$timeDrop = $_SESSION['end'].' '.$_POST['time2'];
	$timeDrop = strtotime($timeDrop);
	
	$diff = round(($timeDrop-$timePick)/3600);
	
	
	
	$_SESSION['diff'] = $diff;
	$_SESSION['days'] = $DaysCount;
	$getCars4Price = mysqli_query($GLOBALS['db'],"SELECT * FROM catalog");
	$row = mysqli_fetch_array($GLOBALS['db'],$getCars4Price);
	$prices = array();
	while($row = mysqli_fetch_array($getCars4Price))
	{
		if($_SESSION['place'] == 1)
		{
			$getPickupPrice = $row[pick_tbs];
		}
		if($_SESSION['place'] == 2)
		{
			$getPickupPrice = $row[pick_tbs_air];
		}
		if($_SESSION['place'] == 3)
		{
			$getPickupPrice = $row[pick_btm];
		}
		if($_SESSION['place'] == 4)
		{
			$getPickupPrice = $row[pick_btm_air];
		}
		if($_SESSION['place'] == 5)
		{
			$getPickupPrice = $row[pick_kut];
		}
		if($_SESSION['place'] == 6)
		{
			$getPickupPrice = $row[pick_kut_air];
		}
		if($_SESSION['place'] == 7)
		{
			$getPickupPrice = $row[pick_step];
		}



		if($_SESSION['drop'] == 1)
		{
			$getDropoffPrice = $row[drop_tbs];
		}
		if($_SESSION['drop'] == 2)
		{
			$getDropoffPrice = $row[drop_tbs_air];
		}
		if($_SESSION['drop'] == 3)
		{
			$getDropoffPrice = $row[drop_btm];
		}
		if($_SESSION['drop'] == 4)
		{
			$getDropoffPrice = $row[drop_btm_air];
		}
		if($_SESSION['drop'] == 5)
		{
			$getDropoffPrice = $row[drop_kut];
		}
		if($_SESSION['drop'] == 6)
		{
			$getDropoffPrice = $row[drop_kut_air];
		}
		if($_SESSION['drop'] == 7)
		{
			$getDropoffPrice = $row[drop_step];
		}


		$pickoff = $getDropoffPrice + $getPickupPrice;
		if(!isset($_SESSION[pick]))
		{
			$price = $row['30-31'];
		}
		if(isset($_SESSION[pick]))
		{
			if($diff <= 0)
			{
				$price = $row['30-31'] + $pickoff;
			}
			if($diff <= 24 and $diff > 0 and $DaysCount != 1)
			{
				$price = $row['price'] + $pickoff;
			}
			if($diff <= 24 and $diff > 0 and $DaysCount == 1)
			{
				$price = $row['price'] + $pickoff;
			}
			if(($DaysCount >= 1 and $DaysCount <=2) and ($diff > 24 and $diff <= 48))
			{
				$pricePerHour = ($row['price']*2);
				$price = $pricePerHour+$pickoff;
			}
			if($DaysCount >= 1 and $DaysCount <=2 and $diff > 48)
			{
				$pricePerHour = $row['price']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
			}
			if($DaysCount >= 3 and $DaysCount <= 4)
			{
				$pricePerHour = $row['3-4']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			if($DaysCount >= 5 and $DaysCount <= 7)
			{
				$pricePerHour = $row['5-7']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			if($DaysCount >= 8 and $DaysCount <= 10)
			{
				$pricePerHour = $row['8-10']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			if($DaysCount >= 11 and $DaysCount <= 14)
			{
				$pricePerHour = $row['11-15']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			if($DaysCount >= 15 and $DaysCount <= 17)
			{
				$pricePerHour = $row['16-21']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			if($DaysCount >= 18 and $DaysCount <= 20)
			{
				$pricePerHour = $row['22-25']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			if($DaysCount >= 21 and $DaysCount <= 23)
			{
				$pricePerHour = $row['26-27']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			if($DaysCount >= 24 and $DaysCount <= 26)
			{
				$pricePerHour = $row['28-29']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			if($DaysCount >= 27 and $DaysCount <= 365)
			{
				$pricePerHour = $row['30-31']/24;
				$price = ($diff*$pricePerHour)+$pickoff;
				
			}
			
		}
		$prices[] = $price;
	}
	
	$maxp = max($prices)+1;
	$minp = min($prices)-1;
	//echo $pickoff;
	//echo $timePick .' and '.$timeDrop;
}

?>
<!DOCTYPE html>
<html lang="zxx">



<head>
<meta name="yandex-verification" content="44786e581c8d566f" />
        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge" />
        <title>
		<?
			if(isset($_GET[id]) and !empty($_GET[id]))
			{
				$id = mysqli_real_escape_string($GLOBALS['db'],trim($_GET[id]));
				$getTitle = mysqli_query($GLOBALS['db'],"SELECT * FROM catalog WHERE id='$id'") or die(mysql_error());
				$getTitleR = mysqli_fetch_array($getTitle);
				//$title = $getTitleR[name].' от '.$getTitleR[price].' GEL | '.$getSystemRow[title_rus];
				//echo $title;
				echo $getTitleR[title_rus];
			}
			else
			{
				echo 'Дешевый цены  прокат джип  и седаны в Грузии | Батуми | Кутаисский';
			}
		?>
		</title>
        <meta content="<?
		if(isset($_GET[id]) and !empty($_GET[id]))
		{
			echo strip_tags($getTitleR[desc_rus]).' | Auto4Rental';
		}
		else
		{
			echo 'Получите лучший дешевый прокат машины в Тбилиси от компании Auto4rental. Прокат автомобилей в аэропортах Батуми и Кутаиси по низким ценам';
		}
		?>" name="description" />
        <meta content="<? echo $getSystemRow[keywords_rus]; ?>" name="keywords" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta property="og:title" content="
		<?
		if(isset($_GET[id]) and !empty($_GET[id]))
			{

				echo $getTitleR[title_rus];
			}
			else
			{
				echo 'Прокат авто в Грузии | Прокат автомобилей Грузии | Аренда авто Грузии';
			}
		?>
		" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="http://rentcartbilisi.com" />
		<meta property="og:image" content="http://rentcartbilisi.com/black.png" />
        <meta content="telephone=no" name="format-detection" />
        <meta name="HandheldFriendly" content="true" />
        <link rel="icon" type="image/x-icon" href="http://rentcartbilisi.com/ru/favicon.ico" />
		<link rel="stylesheet" type="text/css" href="http://rentcartbilisi.com/assets/fonts/flaticon/font/flaticon.css">
		
		<link rel="stylesheet" href="http://rentcartbilisi.com/assets/plugins/slider-pro/slider-pro.css">
		<link rel="stylesheet" href="http://rentcartbilisi.com/mega2.css">
        <!--[if lt IE 9 ]>
<script src="http://rentcartbilisi.com/ru/assets/js/separate-js/html5shiv-3.7.2.min.js" type="text/javascript"></script><meta content="no" http-equiv="imagetoolbar">
<![endif]-->

    </head>

    <body onload='GetCars();'>
        <!-- Loader-->
        <div id="page-preloader"><span class="spinner border-t_second_b border-t_prim_a"></span>
        </div>
        <!-- Loader end-->
        <!-- ==========================-->
        <!-- MOBILE MENU-->
        <!-- ==========================-->
        <? include("assets/blocks/header_m.php"); ?>
        <div class="l-theme animated-css" data-header="sticky" data-header-top="200" data-canvas="container">

            <!-- ==========================-->
            <!-- SEARCH MODAL-->
            <!-- ==========================-->
            <div class="header-search open-search">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                            <div class="navbar-search">
                                <form class="search-global">
                                    <input class="search-global__input" type="text" placeholder="Type to search" autocomplete="off" name="s" value="" />
                                    <button class="search-global__btn"><i class="icon stroke icon-Search"></i>
                                    </button>
                                    <div class="search-global__note">Begin typing your search above and press return to search.</div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="search-close close" type="button"><i class="fa fa-times"></i>
                </button>
            </div>

            <? include("assets/blocks/header.php"); ?>
            <!-- end .header-->

			<?

				function isMobile() { 
				return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
				}
				if(isset($_GET['id']) and !empty($_GET['id']))
				{
					include("assets/blocks/car_d.php");
				}
				else
				{	
					if(isMobile()){
						include("assets/blocks/cars_m.php");
					}
					else
					{
						include("assets/blocks/cars.php");
					}
					
				}

			?>
            <!-- end .l-main-content-->
            <? include("assets/blocks/footer.php"); ?>
            <!-- .footer-->
        </div>
        <!-- end layout-theme-->


        <script src="http://rentcartbilisi.com/ru/bro2.js"></script>
		<script>
if ($('#slider-price').length > 0) {
    var slider1 = document.getElementById('slider-price');
	
    noUiSlider.create(slider1, {
		<?
		if(isset($_POST['date1']) and isset($_POST['date2']) and ($_POST[place] !='none' or $_POST[drop] != 'none'))
		{
			echo '
			start: [ '.$minp.', '.$maxp.' ],
			range: {
			\'min\': ['.$minp.'],
			\'max\': ['.$maxp.']
			
			';
		}
		else
		{
			$getPmin = mysqli_query($GLOBALS['db'],"SELECT min(`30-31`) as m,max(`30-31`) as p FROM catalog") or die(mysqli_error());
			$getPminR = mysqli_fetch_array($getPmin);
			echo '
			
			
			start: [ '.$getPminR[m].', 1200 ],
      range: {
        \'min\': ['.$getPminR[m].'],
        \'max\': [1200]
			';
		}
		?>
      
      },
      step: 1,
      connect: true,
      format: wNumb({
        decimals: 0
      }),
    });
    var snapValues = [
      document.getElementById('slider-snap-value-lower'),
      document.getElementById('slider-snap-value-upper')
    ];

    slider1.noUiSlider.on('update', function( values, handle ) {
      snapValues[handle].innerHTML = values[handle];
    });
  }
  
  if ($('#slider-price2').length > 0) {
    var slider2 = document.getElementById('slider-price2');

    noUiSlider.create(slider2, {
      start: [ 2004, 2018 ],
      range: {
        'min': [2004],
        'max': [2018]
      },
      step: 1,
      connect: true,
      format: wNumb({
        decimals: 0
      }),
    });
    var snapValues2 = [
      document.getElementById('slider-snap-value-lower2'),
      document.getElementById('slider-snap-value-upper2')
    ];

    slider2.noUiSlider.on('update', function( values, handle ) {
      snapValues2[handle].innerHTML = values[handle];
    });
  }
</script>
<div id="add">
		</div>
		<script Language="JavaScript">


$("#promo").on('keyup', function(){
	var promo = $("#promo").val();
	
	
		param 				 = new Object();
		param.promo			 = promo;
		$.ajax({
			url: 'http://rentcartbilisi.com/promo.php',
			data: param,
			success: function(datas) {       
				var d = JSON.parse(datas);
				
				if(d.status == 'ok'){

					$("#promo_answer").html('<p style="text-align:center; color:#51fd51; font-size:17px; font-weight:900; display:block" >Correct promotional code: '+d.promo+'  <b>-'+d.p+'% discount</b></p>');
				}
				else{
					$("#promo_answer").html('<p style="text-align:center; color:red; font-size:17px; font-weight:900; display:block" >Promo code not real!!!</p>');
				}
			}
		});
		
		if(promo == ''){
			$("#promo_answer").empty();
			calculate();
		}
	
});
function calculate()
{
var pick = document.getElementById("datetimepicker3").value;
var end = document.getElementById("datetimepicker2").value;
var pickup = document.getElementById("pickup").value;
var dropoff = document.getElementById("dropoff").value;
var id = document.getElementById("carID").value;
var time1 = document.getElementById("time1").value;
var time2 = document.getElementById("time2").value;
<?
$getServices = mysqli_query($GLOBALS['db'],"SELECT * FROM services");
$getServicesR = mysqli_fetch_array($getServices);
do
{
	echo 'var ser'.$getServicesR[id].' = document.getElementById("chec'.$getServicesR[id].'").checked;';
}
while($getServicesR = mysqli_fetch_array($getServices));
?>
var xhr;
 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
}
var data = "pick=" + pick + "&end=" + end + "&id=" + id + "&pickup=" + pickup + "&dropoff=" + dropoff + "&time1=" + time1 + "&time2=" + time2

<?
$getServices2 = mysqli_query($GLOBALS['db'],"SELECT * FROM services");
$getServicesR2 = mysqli_fetch_array($getServices2);
do
{
	echo ' + "&ser'.$getServicesR2[id].'=" + ser'.$getServicesR2[id].'';
}
while($getServicesR2 = mysqli_fetch_array($getServices2));
?>
;
     xhr.open("POST", "http://rentcartbilisi.com/ru/booking.php", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
	 xhr.onreadystatechange = display_data;
	function display_data() {
	 if (xhr.readyState == 4) {
      if (xhr.status == 200) {
       //alert(xhr.responseText);	   
	  document.getElementById("amount").innerHTML = xhr.responseText;
      } else {
        alert('There was a problem with the request.');
      }
     }
	}
}
try {
// код, вызывающий ошибку

function zak()
{
document.getElementById("answer").innerHTML = '<img src="http://rentcartbilisi.com/lll.gif" style="width:36%;">';
var pick = document.getElementById("datetimepicker3").value;
var end = document.getElementById("datetimepicker2").value;
var id = document.getElementById("carID").value;
var b = document.getElementById("b").value;
var first = document.getElementById("firstname").value;
var last = document.getElementById("lastname").value;
var mob = document.getElementById("mob").value;
var email = document.getElementById("Email").value;

var time1 = document.getElementById("time1").value;
var time2 = document.getElementById("time2").value;
<?
$getServices = mysqli_query($GLOBALS['db'],"SELECT * FROM services");
$getServicesR = mysqli_fetch_array($getServices);
do
{
	echo 'var ser'.$getServicesR[id].' = document.getElementById("chec'.$getServicesR[id].'").checked;';
}
while($getServicesR = mysqli_fetch_array($getServices));
?>
var pickup = document.getElementById("pickup").value;
var dropoff = document.getElementById("dropoff").value;

var xhr;
 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
}
var data = "pick=" + pick + "&end=" + end + "&id=" + id + "&book=" + b + "&first=" + first + "&last=" + last + "&mob=" + mob + "&email=" + email + "&pickup=" + pickup + "&dropoff=" + dropoff + "&time1=" + time1 + "&time2=" + time2
<?
$getServices2 = mysqli_query($GLOBALS['db'],"SELECT * FROM services");
$getServicesR2 = mysqli_fetch_array($getServices2);
do
{
	echo ' + "&ser'.$getServicesR2[id].'=" + ser'.$getServicesR2[id].'';
}
while($getServicesR2 = mysqli_fetch_array($getServices2));
?>
;
     xhr.open("POST", "http://rentcartbilisi.com/ru/booking.php", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
	 xhr.onreadystatechange = display_data;
	function display_data() {
	 if (xhr.readyState == 4) {
      if (xhr.status == 200) {
       //alert(xhr.responseText);	   
	  document.getElementById("answer").innerHTML = xhr.responseText;
      } else {
        alert('There was a problem with the request.');
      }
     }
	}
}
}
catch(e) {
    console.log(e)
}
function sss()
{
document.getElementById("resp").innerHTML = '<img src="http://rentcartbilisi.com/lll2.gif" style="width:6%;">';
var name = document.getElementById("name").value;
		var email = document.getElementById("email").value;
		var mobile = document.getElementById("mobile").value;
		var question = document.getElementById("question").value;
var xhr;
 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
}
var data = "name=" + name + "&email=" + email + "&mobile=" + mobile + "&question=" + question;
     xhr.open("POST", "http://rentcartbilisi.com/ru/send.php", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
	 xhr.onreadystatechange = display_data;
	function display_data() {
	 if (xhr.readyState == 4) {
      if (xhr.status == 200) {
       //alert(xhr.responseText);	   
	  document.getElementById("resp").innerHTML = xhr.responseText;
      } else {
        alert('There was a problem with the request.');
      }
     }
	}
}
function filt()
{
var mark = document.getElementById("mark").value;
var model = document.getElementById("model").value;
var priceN = document.getElementById("slider-snap-value-lower").innerHTML;
var priceM = document.getElementById("slider-snap-value-upper").innerHTML;

var minYear = document.getElementById("slider-snap-value-lower2").innerHTML;
var maxYear = document.getElementById("slider-snap-value-upper2").innerHTML;
var fuel = document.getElementById("fuel").value;
var transm = document.getElementById("transm").value;

var suv = document.getElementById("typeSuv").checked;

var sedan = document.getElementById("typeSedan").checked;


var order = document.getElementById("orders").value;
var xhr;
 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
}
var data = "mark=" + mark + "&model=" + model + "&priceN=" + priceN + "&priceM=" + priceM + "&suv=" + suv + "&sedan=" + sedan + "&service=" + serv + "&fuel=" + fuel + "&transm=" + transm + "&minyear=" + minYear + "&maxyear=" + maxYear + "&order=" + order;
     xhr.open("POST", "http://rentcartbilisi.com/ru/searchV2.php", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
	 xhr.onreadystatechange = display_data;
	function display_data() {
	 if (xhr.readyState == 4) {
      if (xhr.status == 200) {
       //alert(xhr.responseText);	   
	  document.getElementById("result").innerHTML = xhr.responseText;
      } else {
        alert('There was a problem with the request.');
      }
     }
	}
}
function age()
{
var birth = document.getElementById("datetimepicker").value;
var xhr;
 if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
}
var data = "age=" + birth;
     xhr.open("POST", "http://rentcartbilisi.com/ru/booking.php", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
	 xhr.onreadystatechange = display_data;
	function display_data() {
	 if (xhr.readyState == 4) {
      if (xhr.status == 200) {
       //alert(xhr.responseText);	   
	  document.getElementById("age").innerHTML = xhr.responseText;
      } else {
        alert('There was a problem with the request.');
      }
     }
	}
}
function Getadd() {
    document.getElementById("result").innerHTML = '<center><img src="http://rentcartbilisi.com/loading.gif"></center>';
    var mark = document.getElementById("mark").value,
        model = document.getElementById("model").value,
        fuel = document.getElementById("fuel").value,
        transm = document.getElementById("transm").value,
        suv = document.getElementById("typeSuv").checked,
        sedan = document.getElementById("typeSedan").checked,
        order = document.getElementById("orders").value,
        xhr;
	<?
		$getServices = mysqli_query($GLOBALS['db'],"SELECT * FROM services");
		$getServicesR = mysqli_fetch_array($getServices);
		do
		{
			echo 'var ser'.$getServicesR[id].' = document.getElementById("chec'.$getServicesR[id].'").checked;';
		}
		while($getServicesR = mysqli_fetch_array($getServices));
	?>
    window.XMLHttpRequest ? xhr = new XMLHttpRequest : window.ActiveXObject && (xhr = new ActiveXObject("Microsoft.XMLHTTP"));
    var data = "mark=" + mark + "&model=" + model + "&suv=" + suv + "&sedan=" + sedan + "&fuel=" + fuel + "&transm=" + transm + "&order=" + order
	<?
		$getServices2 = mysqli_query($GLOBALS['db'],"SELECT * FROM services");
		$getServicesR2 = mysqli_fetch_array($getServices2);
		do
		{
			echo ' + "&ser'.$getServicesR2[id].'=" + ser'.$getServicesR2[id].'';
		}
		while($getServicesR2 = mysqli_fetch_array($getServices2));
	?>
	
	;

    function display_data() {
        if (4 == xhr.readyState)
            if (200 == xhr.status) {
                document.getElementById("result").innerHTML = xhr.responseText;
                try {
                    eval(document.getElementById("run").innerHTML)
                } catch (e) {}
            } else alert("There was a problem with the request.")
    }
    xhr.open("POST", "http://rentcartbilisi.com/searchV2.php", !0), xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"), xhr.send(data), xhr.onreadystatechange = display_data, setTimeout(function() {
        
    }, 1e3)
}
<?
if(isset($_GET[id]))
{
	echo '
	window.onload = function() {
  calculate();
	}
	';
}
?>


</script>
<script src="http://rentcartbilisi.com/assets/plugins/slider-pro/jquery.sliderPro.min.js"></script>
    </body>



</html>
